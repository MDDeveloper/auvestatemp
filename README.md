# Clone a repository:

## 1. Clone https
## 2. Paste to the sourcetree

# Run project:

## 1. Pull latest changes from "Transition" branch
## 2. Run "pod install"
## 3. Have fun

# Some additional info:

## 1. Development scheme for testing / uploading testFlights
## 2. Production only for releases

### Latest changes was made with Xcode 12, iOS 14.1

you can migrate to:
- latest stable pod versions
- bump deployment target  



