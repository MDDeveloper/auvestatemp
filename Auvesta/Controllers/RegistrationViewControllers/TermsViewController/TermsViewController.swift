import UIKit

protocol TermsViewControllerDelegate: AnyObject {
    func didTapAgreeButton(vc: TermsViewController)
    func didTapDisagreeButton(vc: TermsViewController)
}

class TermsViewController: UIViewController {
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var viewForElements: UIView!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    
    @IBOutlet weak var checkboxOne: UIButton!
    @IBOutlet weak var checkboxTwo: UIButton!
    @IBOutlet weak var checkboxThree: UIButton!
    @IBOutlet weak var checkboxFour: UIButton!
    
    @IBOutlet weak var checkboxOneLabel: UILabel!
    @IBOutlet weak var checkboxTwoLabel: UILabel!
    @IBOutlet weak var checkboxThreeLabel: UILabel!
    @IBOutlet weak var checkboxFourDescriptionLabel: UILabel!
    
    @IBOutlet weak var checkboxOneFirstTextView: UITextView!
    @IBOutlet weak var checkboxOneSecondTextView: UITextView!
    @IBOutlet weak var checkboxOneThirdTextView: UITextView!
    @IBOutlet weak var checkboxTwoFirstTextView: UITextView!
    
    
    @IBOutlet weak var disagreeButton: UIButton!
    @IBOutlet weak var agreeButton: ButtonWithActivityIndicator!
    
    var viewController: UIViewController?
    var delegate: TermsViewControllerDelegate?
    private var controller: TermsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setController(viewController: self)
        setupUI()
        downloadTerms()
    }
    
    private func setController(viewController: TermsViewController) {
        controller = TermsController(vc: viewController)
        viewController.delegate = controller
    }
    
    private func downloadTerms() {
        agreeButton.startActivityIndicator()
        RegistrationNetworkManager.getCustomerTerms { (status, terms, error) in
            self.agreeButton.stopActivityIndicator()
            switch status {
            case 200:
                DispatchQueue.main.async {
                    self.updateData(terms: terms!)
                }
            default:
                self.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
    
    private func updateData(terms: CustomerTerms) {
        checkboxOneLabel.text = terms.checkboxOneText
        checkboxTwoLabel.text = terms.checkboxTwoText
        checkboxThreeLabel.text = terms.checkboxThreeText
        checkboxFourDescriptionLabel.text = terms.checkboxFourDescriptionText
        agreeButton.setTitle(terms.buttonText, for: .normal)
        textViewSetup(textView: checkboxOneFirstTextView, text: terms.tacText, linkText: terms.tacLink)
        textViewSetup(textView: checkboxOneSecondTextView, text: terms.distanceSellingText, linkText: terms.distanceSellingLink)
        textViewSetup(textView: checkboxOneThirdTextView, text: terms.revocationText, linkText: terms.revocationLink)
        textViewSetup(textView: checkboxTwoFirstTextView, text: terms.privacyText, linkText: terms.privacyLink)
        viewForElements.subviews.forEach({ $0.isHidden = false })
    }
    
    func textViewSetup(textView: UITextView, text: String, linkText: String) {
        textView.textContainerInset = .zero
        textView.textContainer.lineFragmentPadding = 0
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        
        let attributedString = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle:paragraph])
        attributedString.addAttribute(.link, value: linkText, range: NSRange(location: 0, length: text.count))
        textView.attributedText = attributedString
    }
    
    private func setupUI() {
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.shadowColor = UIColor.black.cgColor
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundView.layer.shadowOpacity = 0.4
        backgroundView.layer.shadowRadius = 4
        
        agreeButton.layer.cornerRadius = 5
        viewForElements.subviews.forEach({ $0.isHidden = true })
        
        disagreeButton.setTitle("disagreeButton".localized(), for: .normal)
        agreeButton.setTitle("", for: .normal)
        headerTitleLabel.text = "customerTermsVcHeader".localized()
    }
    
    @IBAction func didTapCheckBoxOne(_ sender: UIButton) {
        sender.isSelected.toggle()
    }
    
    @IBAction func didTapCheckBoxTwo(_ sender: UIButton) {
        sender.isSelected.toggle()
    }
    
    @IBAction func didTapCheckBoxThree(_ sender: UIButton) {
        sender.isSelected.toggle()
    }
    
    @IBAction func didTapCheckBoxFour(_ sender: UIButton) {
        
        let body: [String: Any] = ["news": true, "newsletterID": 0]
        
        AccountNetworkManager.changeNewsletter(body: body) { (status, error) in
            switch status {
            case 200:
                print(" send terms")
            case 401:
                self.navigationController?.popToRootViewController(animated: true)
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel)
                self.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
        sender.isSelected.toggle()
        
    }
    
    @IBAction func didTapAgreeButton(_ sender: UIButton) {
        guard
            checkboxOne.isSelected,
            checkboxTwo.isSelected,
            checkboxThree.isSelected else {
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                addAlert(title: "emptyCheckboxTitle".localized(),
                         message: "emptyCheckboxMessage".localized(),
                         actions: [okAction])
                return
        }
        delegate?.didTapAgreeButton(vc: self)
    }
    
    @IBAction func didTapDisagreeButton(_ sender: UIButton) {
        delegate?.didTapDisagreeButton(vc: self)
    }
}
