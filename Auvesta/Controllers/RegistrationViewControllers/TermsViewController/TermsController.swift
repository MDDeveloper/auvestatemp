import UIKit

class TermsController: NSObject {
    private var viewController: TermsViewController?
    
    init(vc: TermsViewController) {
        self.viewController = vc
    }
}

extension TermsController: TermsViewControllerDelegate {
    func didTapAgreeButton(vc: TermsViewController) {
        RegistrationNetworkManager.acceptCustomerTerms { (status, error) in
            switch status {
            case 200:
                let storyboard = UIStoryboard(name: "StripeViewController", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "StripeViewController")
                vc.navigationController?.pushViewController(controller, animated: true)
                
            default:
                print("handle error here")
            }
        }
    }
    
    func didTapDisagreeButton(vc: TermsViewController) {
        AppDelegate.shared.rootViewController.showLoginScreen()
    }
}
