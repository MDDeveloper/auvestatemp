import UIKit

protocol PhoneVerificationStepTwoViewControllerDelegate {
    func didTapCancelButton(vc: PhoneVerificationStepTwoViewController)
    func didTapSendButton(vc: PhoneVerificationStepTwoViewController, code: String, number: String, countryCode: String)
    func didTapResendCodeButton(vc: PhoneVerificationStepTwoViewController, number: String, countryCode: String)
}

class PhoneVerificationStepTwoViewController: UIViewController {
    fileprivate enum Direction {
        case left, right
    }
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var verificationCodeDescriptionStringLabel: UILabel!
    @IBOutlet weak var resendCodeStringLabel: UILabel!
    @IBOutlet weak var resendCodeButton: UIButton!
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var oneTimeCodeTextField: OneTimeCodeTextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var delegate: PhoneVerificationStepTwoViewControllerDelegate?
    private var controller: PhoneVerificationStepTwoController?
    var verificationPhoneNumber = ""
    var verificationPhoneNumberCountryCode = ""
    var fullNumber: String {
        return "+" + verificationPhoneNumberCountryCode + verificationPhoneNumber
    }
    var textFieldsIndexes: [UITextField: Int] = [:]
    var sponsorID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        oneTimeCodeTextField.didEnterLastDigit = { [weak self] code in
//
//        }
        
        setController(viewController: self)
        setupUI()
        localize()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setController(viewController: PhoneVerificationStepTwoViewController) {
        controller = PhoneVerificationStepTwoController(vc: viewController)
        viewController.delegate = controller
    }
    
    private func setupUI() {
        nextButton.layer.cornerRadius = 5
        isHiddenResendFeature(bool: true)
        oneTimeCodeTextField.configure(with: 6)
        addInputAccessoryForTextFields(textFields: [oneTimeCodeTextField], dismissable: true, previousNextable: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(30)) {
            self.isHiddenResendFeature(bool: false)
        }
    }
    
    func isHiddenResendFeature(bool: Bool) {
        resendCodeStringLabel.isHidden = bool
        resendCodeButton.isHidden = bool
    }
    
    private func localize() {
        headerTitleLabel.text = "phoneVerificationHeader".localized()
        verificationCodeDescriptionStringLabel.text = "phoneVerificationEnterCodeDesc".localized() + fullNumber
        resendCodeStringLabel.text = "phoneVerificationResendCodeLabel".localized()
        
        let attributeString = NSMutableAttributedString(string: "phoneVerificationResendCodeButtonTitle".localized())
        attributeString.addAttributes([NSAttributedString.Key.underlineStyle: 1,
                                       NSAttributedString.Key.foregroundColor: UIColor.black],
                                      range: NSMakeRange(0, attributeString.length))
        resendCodeButton.setAttributedTitle(attributeString, for: .normal)
        nextButton.setTitle("verifyButton".localized(), for: .normal)
    }
    
    @objc private func keyboardWillHide() {
        self.scrollView.contentOffset = .zero
    }
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        guard
            !oneTimeCodeTextField!.text!.isEmpty,
            let code = oneTimeCodeTextField!.text else {
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                self.addAlert(title: "emptyFieldsTitle".localized(), message: "emptyFieldsMessage".localized(), actions: [okAction])
                return
        }
//        performSegue(withIdentifier: "nextSegue", sender: nil)
        delegate?.didTapSendButton(vc: self, code: code, number: verificationPhoneNumber, countryCode: verificationPhoneNumberCountryCode)
    }
    
    @IBAction func didTapResendCodeButton(_ sender: UIButton) {
        delegate?.didTapResendCodeButton(vc: self, number: verificationPhoneNumber, countryCode: verificationPhoneNumberCountryCode)
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        delegate?.didTapCancelButton(vc: self)
    }
}

extension PhoneVerificationStepTwoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let centerPoint = textField.center
        let convertedPoint = self.view.convert(centerPoint, from: textField.superview)
        let shouldChangeOffset = convertedPoint.y > self.view.frame.height / 2
        if shouldChangeOffset {
            self.scrollView.contentOffset.y += abs(convertedPoint.y - self.view.frame.height / 2)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField == oneTimeCodeTextField else { return false }
        guard let characterCount = textField.text?.count else { return false }
        return characterCount < oneTimeCodeTextField.codeLabelsCount || string == ""
    }
}
