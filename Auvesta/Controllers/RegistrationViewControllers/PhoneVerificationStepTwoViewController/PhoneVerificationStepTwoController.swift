import UIKit

class PhoneVerificationStepTwoController {
    private var viewController: PhoneVerificationStepTwoViewController?
    
    init(vc: PhoneVerificationStepTwoViewController) {
        self.viewController = vc
    }
}

extension PhoneVerificationStepTwoController: PhoneVerificationStepTwoViewControllerDelegate {
    func didTapResendCodeButton(vc: PhoneVerificationStepTwoViewController, number: String, countryCode: String) {
        vc.nextButton.startActivityIndicator()
        let body = ["number": number,
                    "countrycode": countryCode,
                    "culture_code": Locale.current.languageCode!,
                    "deviceID": UIDevice.current.identifierForVendor!.uuidString]
        
        RegistrationNetworkManager.phoneNumberRequest(body: body) { (status, error, _) in
            vc.nextButton.stopActivityIndicator()
            switch status {
            case 200:
                vc.isHiddenResendFeature(bool: true)
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    func didTapSendButton(vc: PhoneVerificationStepTwoViewController, code: String, number: String, countryCode: String) {
        vc.nextButton.startActivityIndicator()
        let body = ["number": number,
                    "code": code,
                    "countrycode": countryCode,
                    "culture_code": Locale.current.languageCode!,
                    "deviceID": UIDevice.current.identifierForVendor!.uuidString]
        
        RegistrationNetworkManager.phoneNumberVerify(body: body) { (status, error) in
            vc.nextButton.stopActivityIndicator()
            switch status {
            case 200:
                let storyboard = UIStoryboard(name: "NewUserInfoViewController", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "NewUserInfoViewController") as! NewUserInfoViewController
                controller.sponsorID = vc.sponsorID
                controller.mobile = vc.fullNumber
                vc.navigationController?.pushViewController(controller, animated: true)
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    func didTapCancelButton(vc: PhoneVerificationStepTwoViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
}
