import UIKit

//TODO: - Refactore code from segues and one storyboard file to separate views

class NewUserInfoController: NSObject {
    enum GenderTypes: Int, CaseIterable {
        case male = 0
        case female = 1
        case company = 2
        
        var stringValue: String {
            switch self {
            case .male: return "male".localized()
            case .female: return "female".localized()
            case .company: return "company".localized()
            }
        }
        
        var backendValue: String {
            switch self {
            case .male: return "M"
            case .female: return "F"
            case .company: return "C"
            }
        }
    }
    
    private var viewController: NewUserInfoViewController?
    private var countriesVC: SearchTableViewController?
    var selectedGender = GenderTypes.male
    var selectedBirthday: String?
    private var dateFormatter: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yyyy"
        return df
    }
    
    init(viewController: NewUserInfoViewController) {
        self.viewController = viewController
    }
}

extension NewUserInfoController: NewUserInfoViewControllerDelegate {
    func didTapSelectArtIdButton(vc: NewUserInfoViewController) {
        let storyboard = UIStoryboard(name: "SearchTableViewController", bundle: nil)
        let artIdVC = storyboard.instantiateViewController(withIdentifier: "SearchTableViewController") as! SearchTableViewController
        
        countriesVC = artIdVC
        artIdVC.currentCase = .artID
        artIdVC.onViewDidLoad = {
            artIdVC.titleLabel.text = "artIdHeaderTitle".localized()
            artIdVC.tableView.delegate = self
        }
        vc.present(artIdVC, animated: true)
    }
    
    func didTapSelectBirthdayButton(vc: NewUserInfoViewController) {
        let message: String
        if #available(iOS 14, *) {
             message = "\n\n\n\n\n\n\n\n\n\n\n"
        } else {
             message = "\n\n\n\n\n\n"
        }
        let alert = UIAlertController(title: "", message: message, preferredStyle: .actionSheet)
        alert.isModalInPopover = true
        
        let pickerFrame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width - 16, height: 140)
        let picker = UIDatePicker(frame: pickerFrame)
        picker.datePickerMode = .date
        
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        
        components.year = -18
        let maxDate = calendar.date(byAdding: components, to: currentDate)
        
        components.year = -100
        let minDate = calendar.date(byAdding: components, to: currentDate)
        
        picker.minimumDate = minDate
        picker.maximumDate = maxDate
        
        if #available(iOS 14, *) {
            picker.preferredDatePickerStyle = .wheels
            picker.sizeToFit()
            }
        
        alert.view.addSubview(picker)
        
        let cancelAction = UIAlertAction(title: "cancelTitle".localized(), style: .destructive, handler: nil)
        let selectAction = UIAlertAction(title: "selectTitle".localized(), style: .default) { (action) in
            self.selectedBirthday = self.dateFormatter.string(from: picker.date)
            vc.birthdayTextField.text = self.selectedBirthday
        }
        
        alert.addAction(selectAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true)
    }
    
    func didTapSelectGenderButton(vc: NewUserInfoViewController) {
        let alert = UIAlertController(title: "", message: "\n\n\n\n\n\n", preferredStyle: .actionSheet)
        alert.isModalInPopover = true
        
        
        let pickerFrame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width - 16, height: 140)
        let picker = UIPickerView(frame: pickerFrame)
        picker.delegate = self
        picker.dataSource = self
        
        alert.view.addSubview(picker)
        
        let cancelAction = UIAlertAction(title: "cancelTitle".localized(), style: .destructive, handler: nil)
        let selectAction = UIAlertAction(title: "selectTitle".localized(), style: .default) { (action) in
            vc.genderTextField.text = self.selectedGender.stringValue
            vc.setupAccessoryForTextFields(gender: self.selectedGender)
            if self.selectedGender == .company {
                DispatchQueue.main.async {
                    vc.companyArtBackgroundView.isHidden = false
                    vc.companyNameBackgroundView.isHidden = false
                }
            } else {
                DispatchQueue.main.async {
                    vc.companyArtBackgroundView.isHidden = true
                    vc.companyNameBackgroundView.isHidden = true
                }
            }
        }
        
        alert.addAction(selectAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true)
    }
    
    func didTapSelectNationalityButton(vc: NewUserInfoViewController) {
        let storyboard = UIStoryboard(name: "SearchTableViewController", bundle: nil)
        let countriesVc = storyboard.instantiateViewController(withIdentifier: "SearchTableViewController") as! SearchTableViewController
        countriesVC = countriesVc
        countriesVc.currentCase = .country
        countriesVc.onViewDidLoad = {
            countriesVc.titleLabel.text = "nationalityHeaderTitle".localized()
            countriesVc.tableView.delegate = self
        }
        vc.present(countriesVc, animated: true)
    }
    
    func didTapNextButton(vc: NewUserInfoViewController) {
        let validationResult = vc.validate()
        let okAction = UIAlertAction(title: "OK".localized(), style: .cancel) { (action) in
        }
        vc.markTextFieldWithError(validationResult: validationResult)
        switch validationResult {
        case .emptyFields:
            vc.addAlert(title: "emptyFieldsTitle".localized(), message: "emptyFieldsMessage".localized(), actions: [okAction])
        case .incorrectEmail:
            vc.addAlert(title: "emptyFieldsMessage".localized(), message: "wrongEmailMessage".localized(), actions: [okAction])
        case .incorrectPassword:
            vc.addAlert(title: "wrongPasswordTitle".localized(), message: "wrongPasswordMessage".localized(), actions: [okAction])
        case .incorrectConfirmationPassword:
            vc.addAlert(title: "wrongPasswordConfirmTitle".localized(), message: "wrongPasswordConfirmMessage".localized(), actions: [okAction])
        case .incorrectFirstName:
            vc.addAlert(title: "wrongFirstNameTitle".localized(), message: "wrongFirstNameMessage".localized(), actions: [okAction])
        case .incorrectLastName:
            vc.addAlert(title: "wrongLastNameTitle".localized(), message: "wrongLastNameMessage".localized(), actions: [okAction])
        case .incorrectTelephone:
            vc.addAlert(title: "wrongTelephoneTitle".localized(), message: "wrongTelephoneMessage".localized(), actions: [okAction])
        case .incorrectMobile:
            vc.addAlert(title: "wrongMobileTitle".localized(), message: "wrongMobileMessage".localized(), actions: [okAction])
        case .incorrectCompanyName:
            vc.addAlert(title: "wrongCompanyNameTitle".localized(), message: "wrongCompanyNameMessage".localized(), actions: [okAction])
        case .incorrectCity:
            vc.addAlert(title: "wrongCityTitle".localized(), message: "wrongCityMessage".localized(), actions: [okAction])
        case .incorrectStreet:
            vc.addAlert(title: "wrongStreetTitle".localized(), message: "wrongStreetMessage".localized(), actions: [okAction])
        case .incorrectZip:
            vc.addAlert(title: "wrongZipCodeTitle".localized(), message: "wrongZipCodeMessage".localized(), actions: [okAction])
        case .valid:
            vc.nextButton.startActivityIndicator()
            
            guard
                let firstName = vc.firstNameTextField.text,
                let lastName = vc.lastNameTextField.text,
                let password = vc.passwordTextField.text,
                let email = vc.emailTextField.text,
                let telephone = vc.telephoneTextField.text,
                let mobile = vc.mobileTextField.text,
                let birthday = vc.birthdayTextField.text,
                let city = vc.cityTextField.text,
                let street = vc.streetTextField.text,
                let zipCode = vc.zipCodeTextField.text,
                let nationality = vc.selectedCountry?.is3,
                let cultureCode = Locale.current.languageCode else { return }
            
            
            //                        AuvestaUser.shared.firstName = vc.firstNameTextField.text!
            //                        AuvestaUser.shared.lastName = vc.lastNameTextField.text!
            //                        AuvestaUser.shared.password = vc.passwordTextField.text!
            //                        AuvestaUser.shared.email = vc.emailTextField.text!
            //                        AuvestaUser.shared.telephone = vc.telephoneTextField.text!
            //                        AuvestaUser.shared.mobilePhone = vc.mobileTextField.text!
            //            AuvestaUser.shared.genderType = selectedGender.backendValue
            //                        AuvestaUser.shared.birthdayDate = vc.birthdayTextField.text!
            //            AuvestaUser.shared.companyName = vc.companyNameTextField.text ?? ""
            //                        AuvestaUser.shared.city = vc.cityTextField.text!
            //                        AuvestaUser.shared.street = vc.streetTextField.text!
            //                        AuvestaUser.shared.zipCode = vc.zipCodeTextField.text!
            //                        AuvestaUser.shared.nationality = vc.selectedCountry!.is3
            //                        AuvestaUser.shared.deviceCultureCode = Locale.current.languageCode!
            //            AuvestaUser.shared.companyArtID = vc.selectedCompanyArtID?.id
            
            let plainPassword = ["password": password, "confirmPassword": password]
            var body: [String: Any] = ["email": email,
                                       "firstName": firstName,
                                       "lastName": lastName,
                                       "plainPassword": plainPassword,
                                       "telephone": telephone,
                                       "mobile": mobile,
                                       "birthday": birthday,
                                       "city": city,
                                       "street": street,
                                       "zip": zipCode,
                                       "nationality": nationality,
                                       "gender": selectedGender.backendValue,
                                       "cultureCode": cultureCode,
                                       "device": "iOS",
                                       "emailcode": "1"
            ]
            
            if let companyName = vc.companyNameTextField.text, let artID = vc.selectedCompanyArtID?.id {
                body["companyName"] = companyName
                body["companyArtID"] = artID
            }
            
            RegistrationNetworkManager.uploadUserInfo(sponsorID: vc.sponsorID, body: body) { (status, error) in
                vc.nextButton.stopActivityIndicator()
                
                switch status {
                case 200:
                    vc.showAlertViewController()
                default:
                    vc.showErrorAlert(title: "somethingWrongTitle".localized(), message: error)
                }
            }
        }
    }
    
    func didTapBackButton(vc: NewUserInfoViewController) {
        AppDelegate.shared.rootViewController.showLoginScreen()
    }
}

extension NewUserInfoController: NewUserInfoViewControllerDataSource {
    
}

extension NewUserInfoController: AlertViewControllerDelegate {
    
    func didTapMainActionButton(vc: AlertViewController, parentViewController: UIViewController) {
        vc.alertMainActionButton.startActivityIndicator()
        
        if let code = vc.oneTimeCodeTextField.text {
            RegistrationNetworkManager.emailVerify(body: ["code": code]) { (status, error) in
                vc.alertMainActionButton.stopActivityIndicator()
                switch status {
                case 200:
                    vc.dismiss(animated: true) {
                        let newUserInfoViewController = parentViewController as! NewUserInfoViewController
                        let storyboard = UIStoryboard(name: "SubscriptionViewController", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "SubscriptionViewController")
                        newUserInfoViewController.navigationController?.pushViewController(controller, animated: true)
                    }
                case 404:
                    vc.alertTextLabel.text = "emailAlertVcWrongPasscode".localized()
                    vc.oneTimeCodeTextField.text = ""
                    vc.oneTimeCodeTextField.textDidChange()
                default:
                    vc.alertTextLabel.text = "emailAlertVcConfirmError".localized()
                }
            }
        }
    }
    
    func didTapSecondaryActionButton(vc: AlertViewController, parentViewController: UIViewController) {
        vc.alertSecondaryActionButton.startActivityIndicator()
        
        RegistrationNetworkManager.resendEmail { (status, error) in
            vc.alertSecondaryActionButton.stopActivityIndicator()
            
            switch status {
            case 200:
                vc.alertSecondaryActionButton.setTitleColor(.white, for: .normal)
                vc.alertSecondaryActionButton.setTitle("emailAlertVcResendComplete".localized(), for: .normal)
                vc.alertSecondaryActionButton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                vc.alertSecondaryActionButton.isEnabled = false
                let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                parentViewController.addAlert(title: "emailAlertVcResendHeader".localized(), message: "emailAlertVcResendMessage".localized(), actions: [okAction])
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 20, execute: {
                    vc.alertSecondaryActionButton.setTitleColor(#colorLiteral(red: 0.431372549, green: 0.3490196078, blue: 0.2196078431, alpha: 1), for: .normal)
                    vc.alertSecondaryActionButton.backgroundColor = #colorLiteral(red: 0.8759256005, green: 0.7072297931, blue: 0.3024555743, alpha: 1)
                    vc.alertSecondaryActionButton.setTitle("emailAlertVcResend".localized(), for: .normal)
                    vc.alertSecondaryActionButton.isEnabled = true
                })
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                parentViewController.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
//                let newUserInfoViewController = parentViewController as! NewUserInfoViewController
//                newUserInfoViewController.showErrorAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
    
    func didTapCloseButton(vc: AlertViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
}

extension NewUserInfoController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch countriesVC!.currentCase {
        case .country:
            let country = countriesVC!.dataSource?.getCurrentCountry(vc: countriesVC!, indexPath: indexPath)
            viewController?.selectedCountry = country
            viewController?.nationalityTextField.text = country?.countryName
        case .artID:
            let artID = countriesVC!.dataSource?.getCurrentArtID(vc: countriesVC!, indexPath: indexPath)
            viewController?.selectedCompanyArtID = artID
            viewController?.companyArtTextField.text = artID?.text
        }
        countriesVC?.dismiss(animated: true, completion: nil)
    }
}

extension NewUserInfoController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return GenderTypes(rawValue: row)?.stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedGender = GenderTypes(rawValue: row)!
    }
}

extension NewUserInfoController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return GenderTypes.allCases.count
    }
    
}
