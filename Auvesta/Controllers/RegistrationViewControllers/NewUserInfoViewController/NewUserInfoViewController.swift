import UIKit

protocol NewUserInfoViewControllerDelegate: AnyObject {
    func didTapNextButton(vc: NewUserInfoViewController)
    func didTapBackButton(vc: NewUserInfoViewController)
    func didTapSelectBirthdayButton(vc: NewUserInfoViewController)
    func didTapSelectGenderButton(vc: NewUserInfoViewController)
    func didTapSelectNationalityButton(vc: NewUserInfoViewController)
    func didTapSelectArtIdButton(vc:NewUserInfoViewController)
}

protocol NewUserInfoViewControllerDataSource: AnyObject {
    
}

// FIXME: - Alert with confirmation after error not work ( look for underbar sign _ )
// FIXME: - When user receive message with confirmation code and then abandon confirmation process after another one try to confirm email we receive message "The email has already been taken" ( it's wrong, we should let user use one email that he provide before ) upd. same with confirmed email.

class NewUserInfoViewController: UIViewController {
    enum ValidationResult: String {
        case valid
        case incorrectEmail
        case incorrectFirstName
        case incorrectLastName
        case emptyFields
        case incorrectPassword
        case incorrectConfirmationPassword
        case incorrectTelephone
        case incorrectMobile
        case incorrectCompanyName
        case incorrectCity
        case incorrectStreet
        case incorrectZip
    }
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet private weak var emailBackgroundView: UIView!
    @IBOutlet private weak var passwordBackgroundView: UIView!
    @IBOutlet private weak var confirmPasswordBackgroundView: UIView!
    @IBOutlet private weak var firstNameBackgroundView: UIView!
    @IBOutlet private weak var lastNameBackgroundView: UIView!
    @IBOutlet private weak var telephoneBackgroundView: UIView!
    @IBOutlet private weak var mobileBackgroundView: UIView!
    @IBOutlet private weak var birthdayBackgroundView: UIView!
    @IBOutlet private weak var genderBackgroundView: UIView!
    @IBOutlet private weak var cityBackgroundView: UIView!
    @IBOutlet private weak var streetBackgroundView: UIView!
    @IBOutlet private weak var zipCodeBackgroundView: UIView!
    @IBOutlet private weak var nationalityBackgroundView: UIView!
    @IBOutlet weak var companyNameBackgroundView: UIView!
    @IBOutlet weak var companyArtBackgroundView: UIView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var nationalityTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var telephoneTextField: UITextField!
    @IBOutlet weak var companyArtTextField: UITextField!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var userInfoScrollView: UIScrollView!
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var delegate: NewUserInfoViewControllerDelegate?
    private var dataSource: NewUserInfoViewControllerDataSource?
    private var controller: NewUserInfoController?
    var mobile = ""
    var sponsorID = ""
    var selectedCountry: Country?
    var selectedCompanyArtID: CompanyArtID?
    var onViewDidLoad: (()->())? = nil
    var isFlow = false
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setController(viewController: self)
        setupUI()
        localize()
        onViewDidLoad?()
        
        if isFlow {
            showAlertViewController()
        }
        #if DEBUG
        debug()
        #endif
    }
    
    // MARK: - Methods
    
    private func debug() {
        passwordTextField.text = "Qwerty1234@"
        confirmTextField.text = "Qwerty1234@"
        firstNameTextField.text = "Test"
        lastNameTextField.text = "iOS"
        cityTextField.text = "City"
        streetTextField.text = "Street"
        zipCodeTextField.text = "Zip"
    }
    
    private func setController(viewController: NewUserInfoViewController) {
        controller = NewUserInfoController(viewController: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    func validate() -> ValidationResult {
        guard
            !emailTextField.text!.isEmpty,
            !passwordTextField.text!.isEmpty,
            !confirmTextField.text!.isEmpty,
            !firstNameTextField.text!.isEmpty,
            !lastNameTextField.text!.isEmpty,
            !mobileTextField.text!.isEmpty,
            !birthdayTextField.text!.isEmpty,
            !genderTextField.text!.isEmpty,
            !cityTextField.text!.isEmpty,
            !streetTextField.text!.isEmpty,
            !zipCodeTextField.text!.isEmpty,
            !nationalityTextField.text!.isEmpty else { return .emptyFields }
        guard firstNameTextField.text!.isValidFirstName() else { return .incorrectFirstName }
        guard lastNameTextField.text!.isValidLastName() else { return .incorrectLastName }
        guard emailTextField.text!.isValidEmail() else { return .incorrectEmail }
        guard passwordTextField.text!.isValidPassword(name: firstNameTextField.text!, surname: lastNameTextField.text!, date: birthdayTextField.text!) else { return .incorrectPassword }
        guard passwordTextField.text == confirmTextField.text else { return .incorrectConfirmationPassword }
        guard mobileTextField.text!.isValidPhoneNumber() else { return .incorrectMobile }
        guard cityTextField.text!.isValidCity() else { return .incorrectCity }
        guard streetTextField.text!.isValidStreet() else { return .incorrectStreet }
        guard zipCodeTextField.text!.isValidZipCode() else { return .incorrectZip }
        
        switch controller!.selectedGender {
        case .female, .male:
            guard !telephoneTextField.text!.isEmpty else { return .valid }
            guard telephoneTextField.text!.isValidPhoneNumber() else { return .incorrectTelephone }
            return .valid
        case .company:
            guard
                !companyNameTextField.text!.isEmpty,
                !companyArtTextField.text!.isEmpty else { return .emptyFields }
            guard companyNameTextField.text!.isValidCompanyName() else { return .incorrectCompanyName }
            guard !telephoneTextField.text!.isEmpty else { return .valid }
            guard telephoneTextField.text!.isValidPhoneNumber() else { return .incorrectTelephone }
            return .valid
        }
        
    }
    
    func markTextFieldWithError(validationResult: ValidationResult) {
        let requiredTextFields: [(textField: UITextField, view: UIView)] = [(emailTextField, emailBackgroundView),
                                                                            (passwordTextField, passwordBackgroundView),
                                                                            (confirmTextField, confirmPasswordBackgroundView),
                                                                            (firstNameTextField, firstNameBackgroundView),
                                                                            (lastNameTextField, lastNameBackgroundView),
                                                                            (mobileTextField, mobileBackgroundView),
                                                                            (birthdayTextField, birthdayBackgroundView),
                                                                            (genderTextField, genderBackgroundView),
                                                                            (cityTextField, cityBackgroundView),
                                                                            (streetTextField, streetBackgroundView),
                                                                            (zipCodeTextField, zipCodeBackgroundView),
                                                                            (nationalityTextField, nationalityBackgroundView)]
        switch validationResult {
        case .emptyFields:
            for tuple in requiredTextFields {
                if tuple.0.text!.isEmpty {
                    tuple.1.layer.borderColor = UIColor.red.cgColor
                }
            }
        case .incorrectEmail:
            emailBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectPassword:
            passwordBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectConfirmationPassword:
            confirmPasswordBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectFirstName:
            firstNameTextField.layer.borderColor = UIColor.red.cgColor
        case .incorrectLastName:
            lastNameBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectTelephone:
            telephoneBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectMobile:
            mobileBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectCompanyName:
            companyNameTextField.layer.borderColor = UIColor.red.cgColor
        case .incorrectCity:
            cityBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectStreet:
            streetBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .incorrectZip:
            zipCodeBackgroundView.layer.borderColor = UIColor.red.cgColor
        case .valid:
            return
        }
    }
    
    private func setupUI() {
        modalTransitionStyle = .crossDissolve
        setupViews()
        companyArtBackgroundView.isHidden = true
        companyNameBackgroundView.isHidden = true
        setupAccessoryForTextFields(gender: .male)
        nextButton.layer.cornerRadius = 5
        mobileTextField.text = mobile
    }
    
    private func localize() {
        headerTitleLabel.text = "newUserVcHeader".localized()
        nextButton.setTitle("nextButton".localized(), for: .normal)
        
        emailTextField.placeholder = "emailPlaceholder".localized()
        passwordTextField.placeholder = "passwordPlaceholder".localized()
        confirmTextField.placeholder = "passwordConfirmPlaceholder".localized()
        firstNameTextField.placeholder = "firstNamePlaceholder".localized()
        lastNameTextField.placeholder = "lastNamePlaceholder".localized()
        zipCodeTextField.placeholder = "zipCodePlaceholder".localized()
        streetTextField.placeholder = "streetPlaceholder".localized()
        cityTextField.placeholder = "cityPlaceholder".localized()
        companyNameTextField.placeholder = "companyNamePlaceholder".localized()
        birthdayTextField.placeholder = "birthdayPlaceholder".localized()
        genderTextField.placeholder = "genderPlaceholder".localized()
        nationalityTextField.placeholder = "nationalityPlaceholder".localized()
        mobileTextField.placeholder = "mobilePlaceholder".localized()
        telephoneTextField.placeholder = "telephonePlaceholder".localized()
        companyArtTextField.placeholder = "companyArtPlaceholder".localized()
    }
    
    func setupAccessoryForTextFields(gender: NewUserInfoController.GenderTypes) {
        switch gender {
        case .company:
            addInputAccessoryForTextFields(textFields: [firstNameTextField,
                                                        lastNameTextField,
                                                        emailTextField,
                                                        passwordTextField,
                                                        confirmTextField,
                                                        streetTextField,
                                                        cityTextField,
                                                        zipCodeTextField,
                                                        telephoneTextField,
                                                        companyNameTextField
                ],
                                           dismissable: true,
                                           previousNextable: true)
        default:
            addInputAccessoryForTextFields(textFields: [firstNameTextField,
                                                        lastNameTextField,
                                                        emailTextField,
                                                        passwordTextField,
                                                        confirmTextField,
                                                        streetTextField,
                                                        cityTextField,
                                                        zipCodeTextField,
                                                        telephoneTextField],
                                           dismissable: true,
                                           previousNextable: true)
        }
    }
    
    func setupViews() {
        setupView(view: emailBackgroundView)
        setupView(view: passwordBackgroundView)
        setupView(view: confirmPasswordBackgroundView)
        setupView(view: firstNameBackgroundView)
        setupView(view: lastNameBackgroundView)
        setupView(view: telephoneBackgroundView)
        setupView(view: mobileBackgroundView)
        setupView(view: birthdayBackgroundView)
        setupView(view: genderBackgroundView)
        setupView(view: companyNameBackgroundView)
        setupView(view: cityBackgroundView)
        setupView(view: streetBackgroundView)
        setupView(view: nationalityBackgroundView)
        setupView(view: zipCodeBackgroundView)
        setupView(view: companyArtBackgroundView)
    }
    
    private func setupView(view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 0.2
        view.layer.borderColor = #colorLiteral(red: 0.745223105, green: 0.754733026, blue: 0.7544236779, alpha: 1)
    }
    
    func showAlertViewController() {
        let storyboard = UIStoryboard(name: "AlertViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        
        controller.viewController = self
        controller.delegate = self.controller
        
        controller.onViewDidLoad = {
            controller.alertCloseButton.isHidden = true
            controller.alertTitleLabel.text = "emailAlertVcTitle".localized()
            controller.alertTextLabel.text = "emailAlertVcText".localized()
            controller.alertEmailLabel.text = self.emailTextField.text!
            if self.emailTextField.text == "" {
                controller.alertEmailLabel.text = AuvestaUser.shared.email
            }
            controller.alertMainActionButton.setTitle("emailAlertVcMainActionButton".localized(), for: .normal)
            controller.alertSecondaryActionButton.setTitle("emailAlertVcSecondActionButton".localized(), for: .normal)
            controller.alertChangeEmailButton.setTitle("emailAlertVcChangeEmailActionButton".localized(), for: .normal)
        }

        controller.completion = { [weak self] text in
            guard let self = self else { return }
            self.emailTextField.text = text
        }
        
        controller.modalPresentationStyle = .overCurrentContext
        present(controller, animated: true)
    }
    
    func showErrorAlert(title: String, message: String, completion: ((Bool) -> ())? = nil ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
        
        let okAction = UIAlertAction(title: "OK".localized(), style: .cancel) { (ok) in
            completion?(true)
        }
        alertController.addAction(okAction)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        keyboardWillHide()
        view.endEditing(true)
    }
    
    @objc private func keyboardWillHide() {
        if self.view.frame.origin.y != 0 {
            self.view.transform = .identity
        }
    }
    
    @objc private func keyboardWillShow(sender: NSNotification) {
        if let keyboardSize = (sender.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -keyboardSize.height + 0.3)
            }
        }
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self)
    }
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        setupViews()
        delegate?.didTapNextButton(vc: self)
    }
    
    @IBAction func didTapSelectGenderButton(_ sender: UIButton) {
        delegate?.didTapSelectGenderButton(vc: self)
    }
    
    @IBAction func didTapSelectBirthday(_ sender: UIButton) {
        delegate?.didTapSelectBirthdayButton(vc: self)
    }
    
    @IBAction func didTapSelectArtIdButon(_ sender: UIButton) {
        delegate?.didTapSelectArtIdButton(vc: self)
    }
    
    @IBAction func didTapSelectNationalityButton(_ sender: UIButton) {
        delegate?.didTapSelectNationalityButton(vc: self)
    }
}

extension NewUserInfoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let centerPoint = textField.center
        let convertedPoint = self.view.convert(centerPoint, from: textField.superview)
        let shouldChangeOffset = convertedPoint.y > self.view.frame.height / 2
        if shouldChangeOffset {
            userInfoScrollView.contentOffset.y += abs(convertedPoint.y - self.view.frame.height / 2)
        }
        return true
    }
}
