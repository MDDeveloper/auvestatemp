import UIKit

class StripeController: NSObject {
    private var viewController: StripeViewController?
    
    init(vc: StripeViewController) {
        self.viewController = vc
    }
}



extension StripeController: StripeViewControllerDelegate {
    func didTapSkipButton(vc: StripeViewController) {
        let storyboard = UIStoryboard(name: "FinalRegistrationViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FinalRegistrationViewController")
        vc.navigationController?.pushViewController(controller, animated: true)
    }
    
}
