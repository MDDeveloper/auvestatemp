import Alamofire
import Stripe
import UIKit

protocol StripeViewControllerDelegate: AnyObject {
    func didTapSkipButton(vc: StripeViewController)
}

class StripeViewController: UIViewController {

    var isStripeInRegistrationFlow = true
    
    // MARK: IBOutlets and Variables:
    
    @IBOutlet weak private var skipStripeButton: UIButton!
    @IBOutlet weak private var stripeScrollView: UIScrollView!
    @IBOutlet weak private var payStripeButton: ButtonWithActivityIndicator!
    
    private var delegate: StripeViewControllerDelegate?
    private var controller: StripeController?
    fileprivate var stripeResponse: StripeIntent?
    
    // MARK: IBActions:
    
    @IBAction private func skipStripeAction(_ sender: UIButton) {
        sendStripeCancel()
        if isStripeInRegistrationFlow {
            delegate?.didTapSkipButton(vc: self)
        } else {
            AppDelegate.shared.rootViewController.showMainScreen()
        }
    }
    
    // MARK: - Layout
    
    var productStackView = UIStackView()
    var paymentStackView = UIStackView()
    var productImageView = UIImageView()
    var productLabel = UILabel()
    var outputTextView = UITextView()
    var paymentTextField = STPPaymentCardTextField()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skipStripeButton.setTitle("stripeSkipButton".localized(), for: .normal)
        getStripeKey()
        getStripeIntent()
        setController(viewController: self)
        registerForKeyboardNotifications()
    }
    
    deinit {
        removeKeyboardNotification()
    }
    
    // MARK: Methods
    
    private func setController(viewController: StripeViewController) {
        controller = StripeController(vc: viewController)
        viewController.delegate = controller
    }
    
    private func getStripeKey() {
        StripeNetworkManager.getStripeKey { (status, info, error) in
            print("\(error)")
            switch status {
            case 200:
                guard let key = info else { return }
                Stripe.setDefaultPublishableKey(key.clientPublic)
                print("\(error)")
            default:
                print("\(status)")

            }
        }
    }
    
    private func sendStripeCancel() {
        guard let paymentIntentID = self.stripeResponse?.paymentIntentID else { return }
        StripeNetworkManager.sendStripeCancel(body: ["stripePaymentIntentID": paymentIntentID]) { (_, _) in
        }
    }
    
    private func  getStripeIntent() {
        StripeNetworkManager.getStripeIntent(body: [:]) { (status, info, error) in
            switch status {
            case 200:
                guard let stripeIntent = info else { return }
                self.stripeResponse = stripeIntent
                self.setupUI(agio: stripeIntent.amount_agio, metal: stripeIntent.amount_metal)
            default:
                self.displayStatus(error)
            }
        }
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    private func removeKeyboardNotification() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardFrameSize = (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        stripeScrollView.contentOffset = CGPoint(x: 0, y: keyboardFrameSize.height)
    }
    
    // MARK: -  Actions
    @objc func pay() {
        self.payStripeButton.startActivityIndicator()
        guard let responseDictionary = stripeResponse else { return }
        let clientSecret = responseDictionary.clientSecret
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
        let paymentMethodParams = STPPaymentMethodParams(card: self.paymentTextField.cardParams,
                                                         billingDetails: nil,
                                                         metadata: nil)
        paymentIntentParams.paymentMethodParams = paymentMethodParams
        STPPaymentHandler.shared().confirmPayment(withParams: paymentIntentParams,
                                                  authenticationContext: self) { (status, paymentIntent, error) in
            switch status {
            case .canceled:
                self.payStripeButton.stopActivityIndicator()
                let alert = UIAlertController(title: "Notification", message: "stripePaymentCancelledText".localized(), preferredStyle: .alert)
                let doneButton = UIAlertAction(title: "stripeAlertDoneButton".localized(), style: .default, handler: nil)
                alert.addAction(doneButton)
                self.present(alert, animated: true)
            case .failed:
                self.payStripeButton.stopActivityIndicator()
                let alert = UIAlertController(title: "Error", message: "somethingWentWrong".localized(), preferredStyle: .alert)
                let doneButton = UIAlertAction(title: "stripeAlertDoneButton".localized(), style: .default, handler: nil)
                alert.addAction(doneButton)
                self.present(alert, animated: true)
            case .succeeded:
                self.payStripeButton.stopActivityIndicator()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    if self.isStripeInRegistrationFlow {
                        let storyboard = UIStoryboard(name: "FinalRegistrationViewController", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "FinalRegistrationViewController")
                        self.navigationController?.pushViewController(controller, animated: true)
                    } else {
                        AppDelegate.shared.rootViewController.showMainScreen()
                    }
                })
                
            @unknown default:
                self.payStripeButton.stopActivityIndicator()
                print("Out of range")
            }
        }
    }
}

private extension StripeViewController {
    func setupUI(agio: Int, metal: Int) {
        setupProductLabel(amountAgio: agio, amountMetal: metal)
        setupPaymentTextField()
        setupPayButton()
        setProductStackView()
        setPaymentStackView()
    }

    func setupProductLabel(amountAgio: Int, amountMetal: Int) {
        productLabel.frame = CGRect(x: 0, y: 270, width: self.view.frame.width, height: 50)
        productLabel.text = String(format: NSLocalizedString("stripeHeaderText".localized(), comment: ""), "\(amountAgio / 100)", "\(amountMetal / 100)")
        productLabel.numberOfLines = 6
        productLabel.font = UIFont.boldSystemFont(ofSize: 20)
        productLabel.textAlignment = .center
        productLabel.textColor = UIColor.init(red: 180/255, green: 154/255, blue: 102/255, alpha: 1.0)
    }
    
    func setupPaymentTextField() {
        paymentTextField.frame = CGRect(x: 0, y: 0, width: 300, height: 60)
        paymentTextField.textColor = UIColor.init(red: 180/255, green: 154/255, blue: 102/255, alpha: 1.0)
        paymentTextField.placeholderColor = .white
    }

    func setupPayButton() {
        payStripeButton.frame = CGRect(x: 0, y: 0, width: 300, height: 60)
        payStripeButton.setTitle("stripePayButton".localized(), for: .normal)
        payStripeButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 16)
        payStripeButton.setTitleColor(UIColor.white, for: .normal)
        payStripeButton.backgroundColor = UIColor.init(red: 178/255, green: 141/255, blue: 87/255, alpha: 1.0)
        payStripeButton.layer.cornerRadius = 5
        payStripeButton.addTarget(self, action: #selector(pay), for: .touchUpInside)
    }
    
    func setProductStackView() {
        productStackView.frame = CGRect(x: 0, y: 150, width: 330, height: 150)
        productStackView.center.x = self.view.center.x
        productStackView.alignment = .center
        productStackView.axis = .vertical
        productStackView.distribution = .equalSpacing
        productStackView.addArrangedSubview(self.productImageView)
        productStackView.setCustomSpacing(10, after: self.productImageView)
        productStackView.addArrangedSubview(self.productLabel)
        view.addSubview(self.productStackView)
    }
    
    func setPaymentStackView() {
        paymentStackView.frame = CGRect(x: 0, y: 400, width: 300, height: 140)
        paymentStackView.center.x = self.view.center.x
        paymentStackView.spacing = 20
        paymentStackView.alignment = .fill
        paymentStackView.axis = .vertical
        paymentStackView.distribution = .fillEqually
        paymentStackView.addArrangedSubview(self.paymentTextField)
        paymentStackView.addArrangedSubview(self.payStripeButton)
        view.addSubview(self.paymentStackView)
    }
    
    func displayStatus(_ message: String) {
        DispatchQueue.main.async {
            self.outputTextView.text! += message + "\n"
            self.outputTextView.scrollRangeToVisible(NSMakeRange(self.outputTextView.text.count - 1, 1))
        }
    }
}

extension StripeViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}
