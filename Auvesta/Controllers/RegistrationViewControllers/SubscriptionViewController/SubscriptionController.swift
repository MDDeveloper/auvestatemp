import UIKit

class SubscriptionController: NSObject {
    private var viewController: SubscriptionViewController?
    private var subscriptions = [AuvestaSubscription]()
    
    init(viewController: SubscriptionViewController) {
        self.viewController = viewController
    }
}

extension SubscriptionController: SubscriptionViewControllerDataSource {
    func downloadSubscriptionsAndTerms(vc: SubscriptionViewController) {
        RegistrationNetworkManager.getSubscriptions { (status, subscriptionsArray, error) in
            switch status {
            case 200:
                self.subscriptions = subscriptionsArray!
                DispatchQueue.main.async {
                    vc.subscriptionTableView.reloadData()
                }
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                vc.addAlert(title: "", message: error, actions: [okAction])
            }
        }
    }
    
    func getSubscriptionsCount(vc: SubscriptionViewController) -> Int {
        return subscriptions.count
    }
    
    func getSubscriptionData(vc: SubscriptionViewController, indexPath: IndexPath) -> AuvestaSubscription {
        return subscriptions[indexPath.row]
    }
}

extension SubscriptionController: SubscriptionViewControllerDelegate {
    func didTapBackButton(vc: SubscriptionViewController) {
        let cancelAction = UIAlertAction(title: "noTitle".localized(), style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "yesTitle".localized(), style: .default) { (action) in
            vc.navigationController?.popToRootViewController(animated: true)
        }
        vc.addAlert(title: "exitTitle".localized(), message: "exitMessage".localized(), actions: [cancelAction, okAction])
    }
    
    func didTapTermsButton(vc: SubscriptionViewController) {
        guard let id = vc.selectedSubscriptionID else { return }
        RegistrationNetworkManager.selectSubscription(id: "\(id)") { (status, error) in
            switch status {
            case 200:
                    let storyboard = UIStoryboard(name: "TermsViewController", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "TermsViewController")
                    vc.navigationController?.pushViewController(controller, animated: true)
                
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
}
