import UIKit

protocol SubscriptionViewControllerDelegate: AnyObject {
    func didTapTermsButton(vc: SubscriptionViewController)
    func didTapBackButton(vc: SubscriptionViewController)
}

protocol SubscriptionViewControllerDataSource: AnyObject {
    func getSubscriptionsCount(vc: SubscriptionViewController) -> Int
    func getSubscriptionData(vc: SubscriptionViewController, indexPath: IndexPath) -> AuvestaSubscription
    func downloadSubscriptionsAndTerms(vc: SubscriptionViewController)
}

class SubscriptionViewController: UIViewController {
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet weak var subscriptionTableView: UITableView!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var tableViewBottomToSuperviewConstraint: NSLayoutConstraint!
    @IBOutlet private weak var tableViewBottomToNextButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate: SubscriptionViewControllerDelegate?
    private var dataSource: SubscriptionViewControllerDataSource?
    private var controller: SubscriptionController?
    var onViewDidLoad: (()->())? = nil
    var selectedSubscriptionID: Int?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setController(viewController: self)
        controller!.downloadSubscriptionsAndTerms(vc: self)
        setupUI()
        onViewDidLoad?()
    }
    
    // MARK: - Methods
    
    private func setController(viewController: SubscriptionViewController) {
        controller = SubscriptionController(viewController: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    private func setupUI() {
        modalTransitionStyle = .crossDissolve
        nextButton.isHidden = true
        nextButton.isEnabled = false
        nextButton.layer.cornerRadius = 5
        headerTitleLabel.text = "subscriptionVcHeader".localized()
        nextButton.setTitle("selectTitle".localized(), for: .normal)
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        delegate?.didTapTermsButton(vc: self)
    }
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self)
    }
}

extension SubscriptionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SubscriptionTableViewCell
        cell.selectionView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 0.5)
        
        tableViewBottomToSuperviewConstraint.priority = UILayoutPriority.defaultLow
        tableViewBottomToNextButtonConstraint.priority = UILayoutPriority.defaultHigh
        
        AuvestaUser.shared.subscriptionType = cell.levelLabel.text ?? ""
        AuvestaUser.shared.subscriptionTypeId = cell.subscription?.id ?? 0
        AuvestaUser.shared.subscriptionAgio = cell.subscription?.agio ?? ""
        AuvestaUser.shared.subscriptionRate = cell.subscription?.investAmountDouble ?? 0
        
        selectedSubscriptionID = cell.subscription?.id
        nextButton.isHidden = false
        nextButton.isEnabled = true
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        
        let cell = tableView.cellForRow(at: indexPath) as? SubscriptionTableViewCell
        cell?.selectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        return indexPath
    }
}

extension SubscriptionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource!.getSubscriptionsCount(vc: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "subscriptionCell") as? SubscriptionTableViewCell else {
            return SubscriptionTableViewCell()
        }
        cell.selectionStyle = .none
        cell.subscription = dataSource?.getSubscriptionData(vc: self, indexPath: indexPath)
        return cell
    }
}
