import UIKit

class FinalRegistrationController: NSObject {
    private var viewController: FinalRegistrationViewController?
    
    init(vc: FinalRegistrationViewController) {
        self.viewController = vc
    }
}

extension FinalRegistrationController: FinalRegistrationViewControllerDelegate {
    func didTapNextButton(vc: FinalRegistrationViewController) {
            AppDelegate.shared.rootViewController.showMainScreen()
    }
    
    func didTapBackButton(vc: FinalRegistrationViewController) {
        let cancelAction = UIAlertAction(title: "noTitle".localized(), style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "yesTitle".localized(), style: .default) { (action) in
            vc.navigationController?.popToRootViewController(animated: true)
        }
        vc.addAlert(title: "exitTitle".localized(), message: "exitMessage".localized(), actions: [cancelAction, okAction])
    }
    
}
