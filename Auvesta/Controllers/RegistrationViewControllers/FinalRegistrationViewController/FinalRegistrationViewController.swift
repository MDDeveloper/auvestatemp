import UIKit

protocol FinalRegistrationViewControllerDelegate: AnyObject {
    func didTapBackButton(vc: FinalRegistrationViewController)
    func didTapNextButton(vc: FinalRegistrationViewController)
}

class FinalRegistrationViewController: UIViewController {

    // MARK: IBOutlets and variables
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var registrationTitleLabel: UILabel!
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    
    private var controller: FinalRegistrationController?
    private var delegate: FinalRegistrationViewControllerDelegate?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setController(viewController: self)
        updateUI()
    }
    
    // MARK: Methods
    
    private func setController(viewController: FinalRegistrationViewController) {
        controller = FinalRegistrationController(vc: viewController)
        viewController.delegate = controller
    }
    
    private func updateUI() {
        modalTransitionStyle = .crossDissolve
        nextButton.layer.cornerRadius = 5
        //nextButton.isEnabled = true
        headerTitleLabel.text = "endRegistrationVcHeader".localized()
        registrationTitleLabel.text = "registrationVcLabel".localized()
        nextButton.setTitle("customerRegistrationFinishButton".localized(), for: .normal)
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapBackButton(_ sender: Any) {
        delegate?.didTapBackButton(vc: self)
    }
    @IBAction func didTapNextButton(_ sender: Any) {
        delegate?.didTapNextButton(vc: self)
    }
    
}

extension FinalRegistrationViewController: UINavigationControllerDelegate {
    
}
