import UIKit

protocol PhoneVerificationStepOneViewControllerDelegate {
    func didTapCancelButton(vc: PhoneVerificationStepOneViewController)
    func didTapSendButton(vc: PhoneVerificationStepOneViewController, mobile: String, countryCode: String)
    func didTapSelectCountryButton(vc: PhoneVerificationStepOneViewController)
}

protocol PhoneVerificationStepOneViewControllerDataSource {
    func getCurrentCountryCode(vc: PhoneVerificationStepOneViewController)
}

class PhoneVerificationStepOneViewController: UIViewController {
    
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var enterMobileDescriptionStringLabel: UILabel!
    @IBOutlet private weak var enterMobileStringLabel: UILabel!
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var mobilePhoneTextField: UITextField!
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var customBackgroundView: UIView!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var delegate: PhoneVerificationStepOneViewControllerDelegate?
    private var dataSource: PhoneVerificationStepOneViewControllerDataSource?
    private var controller: PhoneVerificationStepOneController?
    
    var sponsorID = ""
    var selectedCountry: Country?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        localize()
        setController(viewController: self)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name:UIResponder.keyboardWillHideNotification,
                                               object: nil)
        dataSource?.getCurrentCountryCode(vc: self)
    }
    
    private func setController(viewController: PhoneVerificationStepOneViewController) {
        controller = PhoneVerificationStepOneController(vc: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    private func setupUI() {
        setupView(view: customBackgroundView)
        nextButton.layer.cornerRadius = 5
        mobilePhoneTextField.keyboardType = .phonePad
        addInputAccessoryForTextFields(textFields: [mobilePhoneTextField],
                                       dismissable: true,
                                       previousNextable: false)
    }
    
    private func setupView(view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 0.2
        view.layer.borderColor = #colorLiteral(red: 0.439996779, green: 0.4511739612, blue: 0.4894440174, alpha: 1)
    }
    
    private func localize() {
        headerTitleLabel.text = "phoneVerificationHeader".localized()
        enterMobileDescriptionStringLabel.text = "phoneVerificationEnterMobileDesc".localized()
        enterMobileStringLabel.text = "phoneVerificationEnterMobile".localized()
        nextButton.setTitle("sendButton".localized(), for: .normal)
        mobilePhoneTextField.placeholder = "enterYourMobilePlaceholder".localized()
    }
    
    @objc private func keyboardWillHide() {
        
        self.scrollView.contentOffset = .zero
    }
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        guard
            !mobilePhoneTextField.text!.isEmpty,
            let phoneNumber = mobilePhoneTextField.text,
            let countryCode = selectedCountry?.countryPhoneCode else {
                self.addAlert(title: "emptyFieldsTitle".localized(), message: "emptyFieldsMessage".localized(), actions: [okAction])
                return
        }
        guard phoneNumber.isValidPhoneNumber() else {
            self.addAlert(title: "wrongMobileTitle".localized(), message: "wrongMobileMessage".localized(), actions: [okAction])
            return
        }
        delegate?.didTapSendButton(vc: self, mobile: phoneNumber, countryCode: String(countryCode))
    }
    
    @IBAction func didTapSelectCountryLabel(_ sender: UIButton) {
        delegate?.didTapSelectCountryButton(vc: self)
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        delegate?.didTapCancelButton(vc: self)
    }
}

extension PhoneVerificationStepOneViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let centerPoint = textField.center
        let convertedPoint = self.view.convert(centerPoint, from: textField.superview)
        let shouldChangeOffset = convertedPoint.y > self.view.frame.height / 2
        if shouldChangeOffset {
            self.scrollView.contentOffset.y += abs(convertedPoint.y - self.view.frame.height / 2)
        }
        return true
    }
}
