import UIKit

class PhoneVerificationStepOneController: NSObject {
    private var viewController: PhoneVerificationStepOneViewController?
    private var countriesVC: SearchTableViewController?
    
    init(vc: PhoneVerificationStepOneViewController) {
        self.viewController = vc
    }
}

extension PhoneVerificationStepOneController: PhoneVerificationStepOneViewControllerDelegate {
    func didTapSendButton(vc: PhoneVerificationStepOneViewController, mobile: String, countryCode: String) {
        vc.nextButton.startActivityIndicator()
        let body = ["number": mobile,
                    "countrycode": countryCode,
                    "culture_code": Locale.current.languageCode!,
                    "deviceID": UIDevice.current.identifierForVendor!.uuidString]
        
        RegistrationNetworkManager.phoneNumberRequest(body: body) { (status, error, isSkipped) in
            vc.nextButton.stopActivityIndicator()
            switch status {
            case 200:
                switch isSkipped {
                case true:
                    let storyboard = UIStoryboard(name: "NewUserInfoViewController", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "NewUserInfoViewController") as! NewUserInfoViewController
                    controller.mobile = "+\(countryCode)\(mobile)"
                    controller.sponsorID = self.viewController!.sponsorID
                    controller.onViewDidLoad = {
                        controller.backButton.isHidden = false
                    }
                    vc.navigationController?.pushViewController(controller, animated: true)
                default:
                    let storyboard = UIStoryboard(name: "PhoneVerificationStepTwoViewController", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "PhoneVerificationStepTwoViewController") as! PhoneVerificationStepTwoViewController
                    guard
                        let phoneNumber = vc.mobilePhoneTextField.text,
                        let countryCode = vc.selectedCountry?.countryPhoneCode else { return }
                    controller.verificationPhoneNumber = phoneNumber
                    controller.verificationPhoneNumberCountryCode = String(countryCode)
                    controller.sponsorID = vc.sponsorID
                    vc.navigationController?.pushViewController(controller, animated: true)
                }
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    func didTapCancelButton(vc: PhoneVerificationStepOneViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
    func didTapSelectCountryButton(vc: PhoneVerificationStepOneViewController) {
        let storyboard = UIStoryboard(name: "SearchTableViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SearchTableViewController") as! SearchTableViewController
        
        countriesVC = controller
        controller.currentCase = .country
        controller.onViewDidLoad = {
            controller.titleLabel.text = "nationalityHeaderTitle".localized()
            controller.tableView.delegate = self
        }
        vc.present(controller, animated: true)
    }
    
}

extension PhoneVerificationStepOneController: PhoneVerificationStepOneViewControllerDataSource {
    func getCurrentCountryCode(vc: PhoneVerificationStepOneViewController) {
        RegistrationNetworkManager.getIpInfo { (status, info, _) in
            guard
                status == 200,
                let ipInfo = info else { return }
            RegistrationNetworkManager.getCountriesDetails { (status, countries, _) in
                guard
                    status == 200,
                    let countries = countries,
                    let currentCountry = countries.first(where: { $0.iso == ipInfo.countryCode }) else { return }
                vc.selectedCountry = currentCountry
                vc.countryCodeTextField.text = "+\(currentCountry.countryPhoneCode)"
            }
        }
    }
    
    
}

extension PhoneVerificationStepOneController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView == countriesVC?.tableView else { return }
        switch countriesVC!.currentCase {
        case .country:
            let country = countriesVC!.dataSource?.getCurrentCountry(vc: countriesVC!, indexPath: indexPath)
            viewController?.selectedCountry = country
            viewController?.countryCodeTextField.text = "+\(country!.countryPhoneCode)"
        default: return
        }
        countriesVC?.dismiss(animated: true, completion: nil)
    }
}
