import UIKit

class ScanSponsorIdController {
    
    private var viewController: ScanSponsorIdViewController?
    private var qrViewController: QrScannerViewController?
    
    var scannedSponsorId: String?
    
    private func isValidSponsorId(sponsorID: String, vc: ScanSponsorIdViewController,
                                  completion: @escaping (_ result: ScanSponsorIdViewController.ValidationResult)->()) {
        
        RegistrationNetworkManager.checkSponsorId(sponsorID: sponsorID) { (status, error) in
            switch status {
            case 200:
                vc.isValidSponsorId = true
            default:
                vc.isValidSponsorId = false
            }
            completion(vc.validate())
        }
    }
    
    init(vc: ScanSponsorIdViewController) {
        self.viewController = vc
    }
    
    init(QrVc: QrScannerViewController) {
        self.qrViewController = QrVc
    }
}

extension ScanSponsorIdController: ScanSponsorIdViewControllerDataSource {
    
}

extension ScanSponsorIdController: ScanSponsorIdViewControllerDelegate {
    func didTapNextButton(vc: ScanSponsorIdViewController, sponsorID: String) {
        vc.nextButton.startActivityIndicator()
        
        isValidSponsorId(sponsorID: sponsorID, vc: vc) { (result) in
            vc.nextButton.stopActivityIndicator()
            
            switch result {
            case .emptyFields:
                vc.showErrorAlert(title: "emptyFieldsTitle".localized(),
                                  message: "scanIdVcEnterSponsorID".localized())
            case .incorrectValue:
                vc.showErrorAlert(title: "scanIdVcWrongSponsorIdTitle".localized(),
                                  message: "scanIdVcWrongSponsorIdMessage".localized())
            case .valid:
                let storyboard = UIStoryboard(name: "PhoneVerificationStepOneViewController", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "PhoneVerificationStepOneViewController") as! PhoneVerificationStepOneViewController
                guard let sponsorID = vc.sponsorIdTextField.text else { return }
                controller.sponsorID = sponsorID
                
                vc.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func didTapScanButton(vc: ScanSponsorIdViewController) {
        let storyboard = UIStoryboard(name: "QrScannerViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "QrScannerViewController") as! QrScannerViewController
        controller.scanSponsorIdViewController = vc
        vc.present(controller, animated: true)
    }
    
    func didTapBackButton(vc: ScanSponsorIdViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
}

extension ScanSponsorIdController: QrScannerViewControllerDelegate {
    func didTapBackbutton(qrVc: QrScannerViewController) {
        qrVc.dismiss(animated: true, completion: nil)
    }
    
    func didFoundAndDecodeQrCode(qrVc: QrScannerViewController, sponsorID: String, scanSponsordIdViewController: ScanSponsorIdViewController) {
        scannedSponsorId = sponsorID
        
        viewController = scanSponsordIdViewController
        viewController?.sponsorIdTextField.text = sponsorID
        viewController?.completeImageView.alpha = 1
        
        qrVc.dismiss(animated: true, completion: nil)
    }
}
