import UIKit

protocol ScanSponsorIdViewControllerDelegate: AnyObject {
    func didTapNextButton(vc: ScanSponsorIdViewController, sponsorID: String)
    func didTapScanButton(vc: ScanSponsorIdViewController)
    func didTapBackButton(vc: ScanSponsorIdViewController)
}

protocol ScanSponsorIdViewControllerDataSource: AnyObject {
    
}

class ScanSponsorIdViewController: UIViewController {
    enum ValidationResult: String {
        case valid
        case incorrectValue
        case emptyFields
    }
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var tapToScanLabel: UILabel!
    @IBOutlet weak var insertSponsorIdLabel: UILabel!
    
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var registrationQrImageView: UIImageView!
    @IBOutlet private weak var sponsorCustomView: UIView!
    
    @IBOutlet weak var completeImageView: UIImageView!
    @IBOutlet weak var sponsorIdTextField: UITextField!
    
    private var delegate: ScanSponsorIdViewControllerDelegate?
    private var dataSource: ScanSponsorIdViewControllerDataSource?
    private var controller: ScanSponsorIdController?
    
    var isValidSponsorId = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setController(viewController: self)
        launchObservers()
        localize()
        setupUI()
        
        #if DEBUG
        debug()
        #endif
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        deleteObservers()
    }
    
    // MARK: - Methods
    
    private func debug() {
        sponsorIdTextField.text = "81052-4"
    }
    
    func showErrorAlert(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okAction)
    }
    
    private func launchObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)),
                                               name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func deleteObservers() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: self)
    }
    
    private func setController(viewController: ScanSponsorIdViewController) {
        controller = ScanSponsorIdController(vc: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    private func setupUI() {
        registrationQrImageView.layer.cornerRadius = registrationQrImageView.frame.size.height / 2
        
        sponsorCustomView.layer.cornerRadius = 5
        sponsorCustomView.layer.borderWidth = 0.2
        sponsorCustomView.layer.borderColor = #colorLiteral(red: 0.7598875761, green: 0.7695845366, blue: 0.7692692876, alpha: 1)
        
        nextButton.layer.cornerRadius = 5
    }
    
    private func localize() {
        headerTitleLabel.text = "scanIdVcHeader".localized()
        tapToScanLabel.text = "scanIdVcTapToScan".localized()
        insertSponsorIdLabel.text = "scanIdVcInsertSponsorID".localized()
        nextButton.setTitle("nextButton".localized(), for: .normal)
    }
    
    func validate() -> ValidationResult {
        guard !sponsorIdTextField.text!.isEmpty else { return .emptyFields }
        guard isValidSponsorId else { return .incorrectValue }
        return .valid
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillHide(sender: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.transform = .identity
        }
    }
    
    @objc private func keyboardWillShow(sender: NSNotification) {
        if let keyboardSize = (sender.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -keyboardSize.height / 3)
            }
        }
    }
    
    // MARK: - @IBAction
    
    @IBAction private func didTapNextButton(_ sender: UIButton) {
        delegate?.didTapNextButton(vc: self, sponsorID: sponsorIdTextField.text ?? "nil")
    }
    
    @IBAction private func didTapScanButton(_ sender: UIButton) {
        delegate?.didTapScanButton(vc: self)
    }
    
    @IBAction private func didTapBackButton(_ sender: Any) {
        delegate?.didTapBackButton(vc: self)
    }
}

// MARK: - Extension

extension ScanSponsorIdViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        completeImageView.alpha = 1
        return true
    }
}
