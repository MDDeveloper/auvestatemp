//
//  PoolViewController.swift
//  Auvesta
//
//  Created by meowsawyer on 17/10/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import UIKit
import UICircularProgressRing

class PoolViewController: UIViewController {
    
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet private weak var poolInfoBackgroundView: UIView!
    @IBOutlet private weak var circularBackgroundView: UIView!
    @IBOutlet private weak var poolValuePercentLabel: UILabel!
    @IBOutlet private weak var poolImageView: UIImageView!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var notificationButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var poolsTableView: UITableView!
    @IBOutlet private weak var allParticipantButton: UIButton!
    @IBOutlet private weak var poolGoalLabel: UILabel!
    @IBOutlet private weak var currentPoolValueLabel: UILabel!
    @IBOutlet private weak var poolMessageLabel: UILabel!
    @IBOutlet private weak var poolsTableViewHeaderLabel: UILabel!
    
    private var circularProgressView = UICircularProgressRing()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    let dummyPool = PoolDummyData.getArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        localize()
    }
    // TODO
    private func localize() {
        poolMessageLabel.text = "Your customers need to buy gold"
        poolGoalLabel.text = "OF 10.000€ GOAL"
        currentPoolValueLabel.text = "9023 €"
        poolValuePercentLabel.text = "75%"
        
        headerTitleLabel.text = "poolHeader".localized()
        poolsTableViewHeaderLabel.text = "poolTableViewHeader".localized()
        allParticipantButton.setTitle("poolAllParticipantButton".localized(), for: .normal)
    }
    
    private func setupUI() {
        circularBackgroundView.backgroundColor = .clear
        
        imageBackgroundView.layer.cornerRadius = imageBackgroundView.bounds.size.height / 2
        imageBackgroundView.layer.shadowColor = UIColor.black.cgColor
        imageBackgroundView.layer.shadowRadius = 2
        imageBackgroundView.layer.shadowOpacity = 0.15
        imageBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        poolInfoBackgroundView.layer.cornerRadius = 10
        poolInfoBackgroundView.layer.shadowColor = UIColor.black.cgColor
        poolInfoBackgroundView.layer.shadowRadius = 10
        poolInfoBackgroundView.layer.shadowOpacity = 0.3
        
        setupCircularProgressView()
    }
    
    private func setupCircularProgressView() {
        let width = CGFloat(10)
        let gradient = UICircularRingGradientOptions(startPosition: .topLeft,
                                                     endPosition: .topRight,
                                                     colors: [#colorLiteral(red: 0.8145852685, green: 0.660461843, blue: 0.2676816881, alpha: 1),
                                                              #colorLiteral(red: 0.8985717297, green: 0.7268576026, blue: 0.2927324772, alpha: 1)],
                                                     colorLocations: [CGFloat(0.0), CGFloat(1.0)])
        
        
        circularProgressView.frame = CGRect(x: 0, y: 0, width: circularBackgroundView.frame.width, height: circularBackgroundView.frame.height)
        circularProgressView.minValue = 0
        circularProgressView.maxValue = 100
        circularProgressView.value = 75 // Deprecate
        circularProgressView.fullCircle = false
        circularProgressView.style = .ontop
        circularProgressView.shouldShowValueText = false
        circularProgressView.startAngle = 135
        circularProgressView.endAngle = 45
        
        circularProgressView.innerRingWidth = width
        circularProgressView.gradientOptions = gradient
        
        circularProgressView.outerRingWidth = width
        circularProgressView.outerRingColor = #colorLiteral(red: 0.8521254063, green: 0.8556905389, blue: 0.8593344092, alpha: 1)
        circularProgressView.outerCapStyle = .round
        
        circularBackgroundView.addSubview(circularProgressView)
        
        NSLayoutConstraint.activate([circularProgressView.topAnchor.constraint(equalTo: circularBackgroundView.topAnchor),
                                     circularProgressView.leadingAnchor.constraint(equalTo: circularBackgroundView.leadingAnchor),
                                     circularProgressView.trailingAnchor.constraint(equalTo: circularBackgroundView.trailingAnchor),
                                     circularProgressView.bottomAnchor.constraint(equalTo: circularBackgroundView.bottomAnchor)])
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
       navigationController?.popViewController(animated: true)

    }
}

extension PoolViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dummyPool.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "positionCell", for: indexPath) as! PositionTableViewCell
        cell.poolData = dummyPool[indexPath.row]
        return cell
    }
    
}
