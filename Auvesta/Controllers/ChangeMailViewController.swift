import UIKit

class ChangeMailViewController: UIViewController {
    
    // MARK: IBOutlets and Variables:
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var okButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!
    
    var completion: ((String) -> ())?
    
    override func viewDidLoad() {
        
        setupUI()
        localization()
    }
    
    // MARK: Methods
    
    private func setupUI() {
        headerTitleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        headerTitleLabel.textColor = UIColor.init(red: 227/255, green: 190/255, blue: 75/255, alpha: 1.0)
        emailTextField.tintColor = .black
        okButton.layer.cornerRadius = 5
        okButton.backgroundColor = UIColor.init(red: 227/255, green: 190/255, blue: 75/255, alpha: 1.0)
        okButton.setTitleColor(UIColor.init(red: 135/255, green: 104/255, blue: 48/255, alpha: 1.0), for: .normal)
        cancelButton.layer.cornerRadius = 5
        cancelButton.backgroundColor = UIColor.init(red: 227/255, green: 190/255, blue: 75/255, alpha: 1.0)
        cancelButton.setTitleColor(UIColor.init(red: 135/255, green: 104/255, blue: 48/255, alpha: 1.0), for: .normal)
    }
    
    private func localization() {
        headerTitleLabel.text = "emailVcHeaderTitle".localized()
        emailTextField.placeholder = "emailPlaceholder".localized()
        okButton.setTitle("emailOkButton".localized(), for: .normal)
        cancelButton.setTitle("cancel".localized(), for: .normal)
    }
    
    // MARK: IBActions:
    
    @IBAction private func didTapOkButton(_ sender: Any) {
        guard let emailText = emailTextField.text else { return }
        guard !emailText.isEmpty else {
            let alert = UIAlertController(title: "emailErrorTitle".localized(), message: "emailErrorMessageIsEmpty".localized(), preferredStyle: .alert)
            let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okButton)
            present(alert, animated: true)
            
            return
        }
        guard emailText.isValidEmail() else {
            let alert = UIAlertController(title: "emailErrorTitle".localized(), message: "wrongEmailMessage".localized(), preferredStyle: .alert)
            let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okButton)
            present(alert, animated: true)
            
            return
        }
        NetworkService.request(.changeRegistrationMail, body: ["mail": emailText]) { (_, _, _) in }
        completion?(emailText)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @IBAction private func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
