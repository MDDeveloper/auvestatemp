import UIKit

final class RootViewController: UIViewController {
    //MARK: - Properties
    private var splashViewController: UIViewController = SplashViewController.getInstance
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(splashViewController)
        splashViewController.view.frame = view.bounds
        view.addSubview(splashViewController.view)
        splashViewController.didMove(toParent: self)
    }
    
    //MARK: - Internal Methods
    func showLoginScreen() {
        let storyboard = UIStoryboard(name: "LoginViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        let loginScreen = UINavigationController(rootViewController: controller)
        loginScreen.isNavigationBarHidden = true
        animateFadeTransition(to: loginScreen)
    }

    func showMainScreen() {
        // TODO: - Replace with separate storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TabBarViewController")
        let mainScreen = UINavigationController(rootViewController: controller)
        mainScreen.isNavigationBarHidden = true
        animateFadeTransition(to: mainScreen)
    }
    
    //MARK: - Private Methods
    private func animateFadeTransition(to new: UIViewController, completion: (() -> Void)? = nil) {
        splashViewController.willMove(toParent: nil)
        addChild(new)
        
        transition(from: splashViewController, to: new, duration: 0, options: [], animations: {
        }) { completed in
            self.splashViewController.removeFromParent()
            new.didMove(toParent: self)
            self.splashViewController = new
            completion?()
        }
    }
    
    
}

//MARK: - Navigation extension
extension AppDelegate {
    static var shared: AppDelegate {
        UIApplication.shared.delegate as! AppDelegate
    }
    var rootViewController: RootViewController {
        window!.rootViewController as! RootViewController
    }
}
