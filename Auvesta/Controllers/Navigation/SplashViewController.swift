import UIKit

final class SplashViewController: UIViewController {

    // MARK: - Properties
    static var getInstance: SplashViewController! {
        let storyboard = UIStoryboard(name: "SplashViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier:
            "SplashViewController") as? SplashViewController
        return controller
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createNetworkingRequests()
        setInitialController()
    }

    // MARK: - Private Methods
    
    /// Set up first load networking
    private func createNetworkingRequests() {

    }
    
    /// Set up root navigation
    private func setInitialController() {
        DispatchQueue.main.async {
            AppDelegate.shared.rootViewController.showLoginScreen()

        }
    }
    
    deinit {
        print("Was deinitted")
    }
    
}
