import UIKit

protocol AlertViewControllerDelegate {
    func didTapMainActionButton(vc: AlertViewController, parentViewController: UIViewController)
    func didTapSecondaryActionButton(vc: AlertViewController, parentViewController: UIViewController)
    func didTapCloseButton(vc: AlertViewController)
}

extension AlertViewControllerDelegate {
    func didTapCloseButton(vc: AlertViewController) {}
}

class AlertViewController: UIViewController {
    
    // MARK: IBOutlets and variables:
    
    @IBOutlet private weak var alertBackgroundView: UIView!
    @IBOutlet private weak var alertBackgroundShadowView: UIView!
    
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var alertTextLabel: UILabel!
    @IBOutlet weak var alertEmailLabel: UILabel!
    
    @IBOutlet weak var oneTimeCodeTextField: OneTimeCodeTextField!
    @IBOutlet weak var alertMainActionButton: ButtonWithActivityIndicator!
    @IBOutlet weak var alertSecondaryActionButton: ButtonWithActivityIndicator!
    @IBOutlet weak var alertChangeEmailButton: UIButton!
    
    @IBOutlet weak var alertCloseButton: UIButton!
    
    var viewController: UIViewController?
    
    var delegate: AlertViewControllerDelegate?

    var completion: ((String) -> ())?
    
    var onViewDidLoad: (()->())? = nil
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modalTransitionStyle = .crossDissolve
        
        onViewDidLoad?()
        setupUI()
    }
    
    func createActivityIndicator(view: UIView) -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView()
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
        indicator.color = #colorLiteral(red: 0.5529384017, green: 0.4286764264, blue: 0.1908931732, alpha: 1)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        
        return indicator
    }
    
    func setupUI() {
        
        oneTimeCodeTextField.configure(with: 6)
        addInputAccessoryForTextFields(textFields: [oneTimeCodeTextField], dismissable: true, previousNextable: false)
        
        alertBackgroundView.clipsToBounds = true
        alertBackgroundView.layer.cornerRadius = 10
        
        alertBackgroundShadowView.layer.cornerRadius = 10
        alertBackgroundShadowView.layer.shadowColor = UIColor.black.cgColor
        alertBackgroundShadowView.layer.shadowOffset = CGSize(width: 0, height: 1)
        alertBackgroundShadowView.layer.shadowOpacity = 0.4
        alertBackgroundShadowView.layer.shadowRadius = 4
        
        alertMainActionButton.layer.cornerRadius = 5
        alertSecondaryActionButton.layer.cornerRadius = 5
        alertChangeEmailButton.layer.cornerRadius = 5
    }
    
    @IBAction func didTapMainActionButton(_ sender: UIButton) {
        delegate?.didTapMainActionButton(vc: self, parentViewController: viewController!)
    }
    
    @IBAction func didTapSecondaryActionButton(_ sender: UIButton) {
        delegate?.didTapSecondaryActionButton(vc: self, parentViewController: viewController!)
    }
    
    @IBAction func didTapChangeEmailActionButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ChangeMailViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChangeMailViewController") as! ChangeMailViewController
        controller.completion = { [weak self] text in
            guard let self = self else { return }
            self.alertTextLabel.text = "emailAlertVcText".localized()
            self.completion?(text)
            self.alertEmailLabel.text = text
        }
        controller.modalPresentationStyle = .overCurrentContext
        present(controller, animated: true)
    }
    
    @IBAction func didTapCloseButton(_ sender: UIButton) {
        delegate?.didTapCloseButton(vc: self)
    }
}
