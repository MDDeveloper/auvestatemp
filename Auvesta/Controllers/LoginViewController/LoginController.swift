import UIKit
import LocalAuthentication
import Stripe

class LoginController {
    private enum ForgotPasswordFlow {
        case getOneTimePassword
        case getToken
        case changePassword
    }
    
    private enum ValidationResult: String {
        case valid
        case incorrectEmail
        case emptyFields
        case incorrectPassword
        
        var errorTitle: String {
            switch self {
            case .incorrectEmail: return "wrongEmailTitle".localized()
            case .emptyFields: return "emptyFieldsTitle".localized()
            case .incorrectPassword: return "wrongPasswordTitle".localized()
            default: return ""
            }
        }
        
        var errorMessage: String {
            switch self {
            case .incorrectEmail: return "wrongEmailMessage".localized()
            case .emptyFields: return "emptyFieldsMessage".localized()
            case .incorrectPassword: return "wrongPasswordMessage".localized()
            default: return ""
            }
        }
    }
    
    private var viewController: LoginViewController?
    
    private func startLoginFlow(vc: LoginViewController, stage: UserStatusStage) {
        // TODO: - Fix logics with saving registration states
        switch stage {
        case .unknownState:
            vc.addAlert(message: "unknownUserStage".localized())
        case .notConfirmedEmail:
            let storyboard = UIStoryboard(name: "NewUserInfoViewController", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "NewUserInfoViewController") as! NewUserInfoViewController
            controller.isFlow = true
            vc.navigationController?.pushViewController(controller, animated: true)
//            vc.showAlertViewController()
        case .emailConfirmed:
            let storyboard = UIStoryboard(name: "SubscriptionViewController", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SubscriptionViewController")
            vc.navigationController?.pushViewController(controller, animated: true)
             // MARK: - Latest step according new logics
        case .subscriptionSelected:
            let storyboard = UIStoryboard(name: "SubscriptionViewController", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SubscriptionViewController")
            vc.navigationController?.pushViewController(controller, animated: true)
            
            
            // MARK: - Will be never executed
        case .bankDetailsUploaded:
            let viewController = vc.storyboard?.instantiateViewController(withIdentifier: "BankDetailsViewController") as! BankDetailsViewController
            vc.navigationController?.pushViewController(viewController, animated: true)
        case .customerTermsAccepted:
            let viewController = vc.storyboard?.instantiateViewController(withIdentifier: "UploadPassportViewController") as! UploadPassportViewController
            vc.navigationController?.pushViewController(viewController, animated: true)
        case .customer:
            vc.emailTextField.text = ""
            vc.passwordTextField.text = ""
            self.getStripeCheck()
        }
    }
    
    private func startLoginFlowWithPartnerID(vc: LoginViewController, stage: UserStatusStage) {
        let alert = UIAlertController(title: "partnerIdTitle".localized(),
                                      message: "partnerIdMessage".localized(),
                                      preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textfield) in
            textfield.placeholder = "partnerIdTitle".localized()
            textfield.keyboardType = .default
            textfield.keyboardAppearance = .dark
        })
        
        let okAction = UIAlertAction(title: "continueTitle".localized(), style: .cancel, handler: { (action) in
            let partnerID = alert.textFields?.last?.text ?? ""
            guard !partnerID.isEmpty else {
                vc.addAlert(message: "emptyFieldsMessage".localized())
                return
            }
            
            guard
                let email = vc.emailTextField.text,
                let password = vc.passwordTextField.text else { return }
            let body: [String:Any] = ["email":email,
                                      "password":password,
                                      "partnerID":partnerID]
            
            LoginNetworkManager.loginWithPartnerID(body: body) { (status, error) in
                switch status {
                case 200:
                    self.startLoginFlow(vc: vc, stage: stage)
                default:
                    vc.addAlert(title: "", message: error)
                }
            }
        })
        let cancelAction = UIAlertAction(title: "cancelTitle".localized(), style: .default, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true)
    }
    
    private func validate(email: String, password: String) -> ValidationResult {
        guard
            !email.isEmpty,
            !password.isEmpty else { return .emptyFields }
//        guard email.isValidEmail() else { return .incorrectEmail }
        return .valid
    }
    
    private func loadUpdateInfoVC(vc: UIViewController, configuration: UpdateUserInfoViewController.Configuration, request: ForgotPasswordFlow) {
            let storyboard = UIStoryboard(name: "UpdateUserInfoViewController", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "UpdateUserInfoViewController") as! UpdateUserInfoViewController
            controller.delegate = self
            controller.viewController = vc
            controller.configuration = configuration
            controller.requestType = request
            vc.navigationController?.pushViewController(controller, animated: true)

    }
    
    private func setBiometricksPermission(vc: LoginViewController) {
        let alert = UIAlertController(title: "AllStars", message: AuthManager.shared.biometryAlertText, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "confirm".localized(), style: .default) { action in
            AuthManager.shared.setBiometriksPermission(vc: vc) { success in AuthManager.shared.isBiometricksEnabled = success }
        }
        
        let noAction = UIAlertAction(title: "cancel".localized(), style: .cancel) { action in
            AuthManager.shared.isBiometricksEnabled = false
            self.getStripeCheck()
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        vc.present(alert, animated: true, completion: nil)
    }
    
    private func getStripeCheck() {
        StripeNetworkManager.getStripeCheck(body: [:]) { (status, info, error) in
            switch status {
            case 200:
                guard let stripeCheck = info else { return }
                if stripeCheck.showStripe == "1" {
                    let storyboard = UIStoryboard(name: "StripeViewController", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "StripeViewController") as! StripeViewController
                    controller.isStripeInRegistrationFlow = false
                   
                    self.viewController?.navigationController?.pushViewController(controller, animated: true)
                } else {
                    AppDelegate.shared.rootViewController.showMainScreen()
                }
            default:
                print(" something went wrong")
            }
        }
    }
    
    init(viewController: LoginViewController) {
        self.viewController = viewController
    }
    
    
    
}

// MARK: LoginViewControllerDelegate

extension LoginController: LoginViewControllerDelegate {
    //TODO: - Replace segue with programmatic navigation
    func didTapLoginButton(vc: LoginViewController) {
        let validationResult = validate(email: vc.emailTextField.text!, password: vc.passwordTextField.text!)
        
        switch validationResult {
        case .valid:
            vc.loginButton.startActivityIndicator()
            
            guard
                let email = vc.emailTextField.text,
                let password = vc.passwordTextField.text else { return }
            let body = ["email": email, "password": password]
            
            LoginNetworkManager.login(body: body) {
                (statusCode, error) in
                vc.loginButton.stopActivityIndicator()
                
                switch statusCode {
                    // FIXME: - Refactore navigation logics in case of success login
                case 200:
                    self.startLoginFlow(vc: vc, stage: AuvestaUser.shared.stage)
                    
                    if !AuthManager.shared.isAlreadySaved(password, email: email) {
                        AuthManager.shared.updateAccountCredentials(password, email: email)
                        self.setBiometricksPermission(vc: vc)
                    }
                case 409:
                    self.startLoginFlowWithPartnerID(vc: vc, stage: AuvestaUser.shared.stage)
                default:
                    vc.addAlert(message: error)
                }
            }
        default:
            vc.addAlert(title: validationResult.errorTitle, message: validationResult.errorMessage)
        }
    }
    
    func didTapSignUpButton(vc: LoginViewController) {
        let storyboard = UIStoryboard(name: "ScanSponsorIdViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ScanSponsorIdViewController")
        vc.navigationController?.pushViewController(controller, animated: true)
    }
    
    func didTapForgotPasswordButton(vc: LoginViewController) {
        loadUpdateInfoVC(vc: vc, configuration: .forgotPasswordGetOneTimeCode, request: .getOneTimePassword)
    }
    
    //TODO: - Replace segue with programmatic navigation
    func loginWithBiometricks(vc: LoginViewController) {
        AuthManager.shared.authentificateUser(vc: vc) { [unowned self] password, email in
            DispatchQueue.main.async {
                 vc.passwordTextField.text = password
                 vc.emailTextField.text = email
                 self.didTapLoginButton(vc: vc)
            }
        }
    }

}

extension LoginController: LoginViewControllerDataSource {
    
}

// MARK: UpdateUserInfoViewControllerDelegate

extension LoginController: UpdateUserInfoViewControllerDelegate {
    func didTapApplyButton(vc: UpdateUserInfoViewController, parentViewController: UIViewController?, requestType: Any) {
        vc.applyButton.startActivityIndicator()
        let requestType = requestType as! ForgotPasswordFlow
        
        switch requestType {
        case .getOneTimePassword:
            guard let email = vc.firstTextField.text else { return }
            let body: [String: Any] = ["email": email]
            
            LoginNetworkManager.getOneTimeCode(body: body) { (status, error) in
                vc.applyButton.stopActivityIndicator()
                switch status {
                case 200:
                    self.loadUpdateInfoVC(vc: vc, configuration: .forgotPasswordGetToken, request: .getToken)
                default:
                    vc.addAlert(message: error)
                }
            }
        case .getToken:
            guard let oneTimeCode = vc.firstTextField.text else { return }
            
            LoginNetworkManager.getOneTimeToken(oneTimeCode) { (status, error) in
                vc.applyButton.stopActivityIndicator()
                switch status {
                case 200:
                    self.loadUpdateInfoVC(vc: vc, configuration: .forgotPasswordChange, request: .changePassword)
                default:
                    vc.addAlert(message: error)
                }
            }
        case .changePassword:
            let validationResult = vc.validateNewPassword()
            vc.applyButton.stopActivityIndicator()
            
            switch validationResult {
            case .valid:
                guard
                    let password = vc.firstTextField.text,
                    let confirmPassword = vc.secondTextField.text else { return }
                let plainPassword: [String: Any] = ["password": password, "confirmPassword": confirmPassword]
                let body: [String: Any] = ["plainPassword": plainPassword]
                
                LoginNetworkManager.changeForgottenPassword(body: body) { (status, error) in
                    switch status {
                    case 200:
                        let okAction = UIAlertAction(title: "OK".localized(), style: .default) { (_) in
                            vc.navigationController?.popToRootViewController(animated: true)
                        }
                        vc.addAlert(title: "congratulationsTitle".localized(), message: "passwordFlowCongratulationsMessage".localized(), actions: [okAction])
                    default:
                        vc.addAlert(message: error)
                    }
                }
            default:
                vc.addAlert(message: validationResult.errorMessage)
            }
        }
    }
    
    func didTapBackButton(vc: UpdateUserInfoViewController, parentViewController: UIViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
    
}
