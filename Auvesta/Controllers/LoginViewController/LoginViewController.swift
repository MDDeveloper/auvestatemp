import UIKit

protocol LoginViewControllerDelegate: AnyObject {
    func didTapLoginButton(vc: LoginViewController)
    func didTapSignUpButton(vc: LoginViewController)
    func didTapForgotPasswordButton(vc: LoginViewController)
    func loginWithBiometricks(vc: LoginViewController)
}

protocol LoginViewControllerDataSource: AnyObject {
    
}


class LoginViewController: UIViewController {
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet private weak var emailFieldBackground: UIView!
    @IBOutlet private weak var passwordFieldBackground: UIView!
    
    @IBOutlet weak var loginButton: ButtonWithActivityIndicator!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signUpLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var delegate: LoginViewControllerDelegate?
    private var dataSource: LoginViewControllerDataSource?
    private var controller: LoginController?
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardWillHide()
        guard AuthManager.shared.isBiometricksEnabled else { return }
        loginWithBiometricks()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setController(viewController: self)
        setupUI()
        launchObservers()
        
        #if DEBUG
        debug()
        #endif
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: self)
    }
    
    // MARK: - Methods
    
//    func showAlertViewController() {
//          let storyboard = UIStoryboard(name: "AlertViewController", bundle: nil)
//          let controller = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
//          
//          let sb = UIStoryboard(name: "NewUserInfoViewController", bundle: nil)
//          let st = sb.instantiateViewController(withIdentifier: "NewUserInfoViewController") as! NewUserInfoViewController
//          
//          controller.viewController = self
//          controller.delegate = st as? AlertViewControllerDelegate
//          
//          controller.onViewDidLoad = {
//              controller.alertCloseButton.isHidden = true
//              controller.alertTitleLabel.text = "emailAlertVcTitle".localized()
//              controller.alertTextLabel.text = "emailAlertVcText".localized()
//              controller.alertMainActionButton.setTitle("emailAlertVcMainActionButton".localized(), for: .normal)
//              controller.alertSecondaryActionButton.setTitle("emailAlertVcSecondActionButton".localized(), for: .normal)
//              controller.alertChangeEmailButton.setTitle("emailAlertVcChangeEmailActionButton".localized(), for: .normal)
//          }
//          
//          controller.modalPresentationStyle = .overCurrentContext
//          present(controller, animated: true)
//      }
    
    private func debug() {
         //valid credentials
        
         emailTextField.text = "info@krause-kollegen.de"
         passwordTextField.text = "Pw2Bxlum!"
         //passwordTextField.text = "Pw2Bxlum!"
         //emailTextField.text = "info@werner-ortmann.com"
        
        let flag = UIView()
        flag.backgroundColor = .red
        flag.frame = CGRect(x: 50, y: 50, width: 70, height: 70)
        flag.layer.cornerRadius = 35
        view.addSubview(flag)
    }
    
    private func loginWithBiometricks() {
        delegate?.loginWithBiometricks(vc: self)
    }
    
    private func launchObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)),
                                               name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    private func setController(viewController: LoginViewController) {
        controller = LoginController(viewController: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    private func setupUI() {
        localize()
        loginButton.layer.cornerRadius = loginButton.frame.size.height / 2
        emailFieldBackground.layer.borderWidth = 0.2
        emailFieldBackground.layer.borderColor = #colorLiteral(red: 0.919342339, green: 0.7299711108, blue: 0.3602794707, alpha: 1)
        emailFieldBackground.layer.cornerRadius = 10
        passwordFieldBackground.layer.borderWidth = 0.2
        passwordFieldBackground.layer.borderColor = #colorLiteral(red: 0.919342339, green: 0.7299711108, blue: 0.3602794707, alpha: 1)
        passwordFieldBackground.layer.cornerRadius = 10
    }
    
    private func localize() {
        loginButton.setTitle("loginButton".localized(), for: .normal)
        forgotPasswordButton.setTitle("forgotButton".localized(), for: .normal)
        signUpLabel.text = "signUpLabel".localized()
        signUpButton.setTitle("signUpButton".localized(), for: .normal)
        emailTextField.attributedPlaceholder = NSAttributedString(string: "emailPlaceholder".localized(), attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.4952307343, green: 0.4994661808, blue: 0.5147477388, alpha: 1)])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "passwordPlaceholder".localized(), attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.4952307343, green: 0.4994661808, blue: 0.5147477388, alpha: 1)])
    }
    
    private func moveResponder(textField: UITextField) {
        if textField == emailTextField {
            textField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            passwordTextField.resignFirstResponder()
            keyboardWillHide()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        keyboardWillHide()
        view.endEditing(true)
    }
    
    @objc private func keyboardWillHide() {
        if self.view.frame.origin.y != 0{
            self.view.transform = .identity
        }
    }
    
    @objc private func keyboardWillShow(sender: NSNotification) {
        if let keyboardSize = (sender.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -keyboardSize.height / 3)
            }
        }
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapLoginButton(_ sender: UIButton) {
        delegate?.didTapLoginButton(vc: self)
    }
    
    @IBAction func didTapForgotPasswordButton(_ sender: UIButton) {
        delegate?.didTapForgotPasswordButton(vc: self)
    }
    
    @IBAction func didTapSignUpButton(_ sender: UIButton) {
        delegate?.didTapSignUpButton(vc: self)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        moveResponder(textField: textField)
        return true
    }
}
