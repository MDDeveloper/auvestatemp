import UIKit

class PartnerRegistrationController: NSObject {
    
    private var viewController: PartnerRegistrationViewController?
    
    init(vc: PartnerRegistrationViewController?) {
        self.viewController = vc
    }
}

extension PartnerRegistrationController: PartnerRegistrationViewControllerDelegate {
    func didTapFinishRegistrationButton(vc: PartnerRegistrationViewController, onboardingVc: UIViewController) {
        let validationResult = vc.validate()
        let okAction = UIAlertAction(title: "OK".localized(), style: .cancel) { (action) in
            
        }
        switch validationResult {
        case .emptyFields, .incorrectPassword:
            vc.addAlert(title: "wrongPasswordTitle".localized(), message: "wrongPasswordMessageForPartner".localized(), actions: [okAction])
        case .incorrectConfirmationPassword:
            vc.addAlert(title: "wrongPasswordConfirmTitle".localized(), message: "wrongPasswordConfirmMessage".localized(), actions: [okAction])
        case .valid:
            vc.showTermsVC()
        }
    }
    
    
    func didTapBackButton(vc: PartnerRegistrationViewController) {
        vc.navigationController?.popToRootViewController(animated: true)
    }
}

extension PartnerRegistrationController: PartnerRegistrationViewControllerDataSource {
    func getUserInfo(vc: PartnerRegistrationViewController) -> AuvestaUser {
        return AuvestaUser.shared
    }
    
    func downloadPartnerInfo(vc: PartnerRegistrationViewController, completion: @escaping () -> ()) {
        vc.finishRegistrationButton.startActivityIndicator()
        RegistrationNetworkManager.getPartnerRegistrationDetails { (status, error) in
            vc.finishRegistrationButton.stopActivityIndicator()
            switch status {
            case 200:
                completion()
            default:
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
    
    func getUserPassportImage(vc: PartnerRegistrationViewController) -> UIImage {
        return AuvestaUser.shared.passportPhoto ?? UIImage()
    }
}

extension PartnerRegistrationController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:], completionHandler: nil)
        return false
    }
    
}

extension PartnerRegistrationController: PartnershipTermsViewControllerDelegate {
    func didTapAcceptButton(vc: PartnershipTermsViewController, newsletter: Bool) {
        vc.acceptButton.startActivityIndicator()
        guard let passwordString = viewController?.passwordTextField.text else { return }
        let body = ["password": passwordString]
        
        RegistrationNetworkManager.acceptPartnerTerms(body: body) { (status, error) in
            switch status {
            case 200:
                let body: [String: Any] = ["news": newsletter, "newsletterID": 1]
                
                AccountNetworkManager.changeNewsletter(body: body) { (status, error) in
                    vc.acceptButton.stopActivityIndicator()
                    switch status {
                    case 200:
                        
                        // TODO: - Fix navigation because of double init pools ViewController
                        
                        AuvestaUser.shared.partnerNewsletter = newsletter
                        AuvestaUser.shared.isPartner = true
                        AppDelegate.shared.rootViewController.showMainScreen()
                    case 401:
                        vc.navigationController?.popToRootViewController(animated: true)
                    default:
                        vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
                    }
                }
            default:
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
}
