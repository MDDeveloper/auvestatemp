import UIKit

protocol PartnerRegistrationViewControllerDelegate: AnyObject {
    func didTapFinishRegistrationButton(vc: PartnerRegistrationViewController, onboardingVc: UIViewController)
    func didTapBackButton(vc: PartnerRegistrationViewController)
}

protocol PartnerRegistrationViewControllerDataSource: AnyObject {
    func downloadPartnerInfo(vc: PartnerRegistrationViewController, completion: @escaping ()->())
    func getUserPassportImage(vc: PartnerRegistrationViewController) -> UIImage
//    func getUserEmail(vc: PartnerRegistrationViewController) -> String
//    func getUserFirstName(vc: PartnerRegistrationViewController) -> String
//    func getUserLastName(vc: PartnerRegistrationViewController) -> String
    func getUserInfo(vc: PartnerRegistrationViewController) -> AuvestaUser
}

class PartnerRegistrationViewController: UIViewController {
    enum ValidationResult: String {
        case valid
        case emptyFields
        case incorrectPassword
        case incorrectConfirmationPassword
    }
    
    // MARK: - @IBOutlet & Variables
    //mobilePlaceholder
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var announceStringLabel: UILabel!
    @IBOutlet weak var finishRegistrationButton: ButtonWithActivityIndicator!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var telephoneTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    @IBOutlet weak var nationalityTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var completeImageView: UIImageView!
    @IBOutlet weak var termsAndConditionsBackgroundView: UIView!
    
    @IBOutlet private weak var passwordBackgroundView: UIView!
    @IBOutlet private weak var confirmPasswordBackgroundView: UIView!
    @IBOutlet private weak var emailBackgroundView: UIView!
    @IBOutlet private weak var firstNameBackgroundView: UIView!
    @IBOutlet private weak var lastNameBackgroundView: UIView!
    @IBOutlet private weak var telephoneBackgroundView: UIView!
    @IBOutlet private weak var mobileBackgroundView: UIView!
    @IBOutlet private weak var birthdayBackgroundView: UIView!
    @IBOutlet private weak var genderBackgroundView: UIView!
    @IBOutlet private weak var companyNameBackgroundView: UIView!
    @IBOutlet private weak var cityBackgroundView: UIView!
    @IBOutlet private weak var streetBackgroundView: UIView!
    @IBOutlet private weak var zipBackgroundView: UIView!
    @IBOutlet private weak var nationalityBackgroundView: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var controller: PartnerRegistrationController?
    private var delegate: PartnerRegistrationViewControllerDelegate?
    private var dataSource: PartnerRegistrationViewControllerDataSource?
    var onboardingVC: UIViewController?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        localize()
        setController(viewController: self)
        
        dataSource?.downloadPartnerInfo(vc: self, completion: {
            guard let user = self.dataSource?.getUserInfo(vc: self) else { return }
            self.loadUserSettings(user: user)
        })
    }
    
    // MARK: - Methods
    
    func showTermsVC() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: PartnershipTermsViewController.self)) as! PartnershipTermsViewController
        viewController.viewController = self
        viewController.delegate = controller
        
        viewController.modalPresentationStyle = .overCurrentContext
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func localize() {
        passwordTextField.placeholder = "passwordPlaceholder".localized()
        confirmPasswordTextField.placeholder = "passwordConfirmPlaceholder".localized()
        telephoneTextField.placeholder = "telephonePlaceholder".localized()
        headerTitleLabel.text = "partnerRegistrationVcHeader".localized()
        announceStringLabel.text = "partnerRegistrationCheckFilledInfo".localized()
        finishRegistrationButton.setTitle("partnerRegistrationFinishButton".localized(), for: .normal)
    }
    
    private func setController(viewController: PartnerRegistrationViewController) {
        controller = PartnerRegistrationController(vc: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    private func setupUI() {
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .fullScreen
        
        setupView(view: emailBackgroundView)
        setupView(view: firstNameBackgroundView)
        setupView(view: lastNameBackgroundView)
        setupView(view: telephoneBackgroundView)
        setupView(view: mobileBackgroundView)
        setupView(view: birthdayBackgroundView)
        setupView(view: genderBackgroundView)
        setupView(view: companyNameBackgroundView)
        setupView(view: cityBackgroundView)
        setupView(view: streetBackgroundView)
        setupView(view: zipBackgroundView)
        setupView(view: nationalityBackgroundView)
        setupView(view: passwordBackgroundView)
        setupView(view: confirmPasswordBackgroundView)
        
        finishRegistrationButton.layer.cornerRadius = 5
        finishRegistrationButton.isEnabled = true
    }
    
    private func moveResponder(textField: UITextField) {
        if textField == passwordTextField {
            textField.resignFirstResponder()
            confirmPasswordTextField.becomeFirstResponder()
        } else if textField == confirmPasswordTextField {
            textField.endEditing(true)
        }
    }
    
//    private func loadUserSettings() {
//        emailTextField.text = dataSource?.getUserEmail(vc: self)
//        firstNameTextField.text = dataSource?.getUserFirstName(vc: self)
//        lastNameTextField.text = dataSource?.getUserLastName(vc: self)
//    }
    
    private func loadUserSettings(user: AuvestaUser) {
        emailTextField.text = user.email
        firstNameTextField.text = user.firstName
        lastNameTextField.text = user.lastName
        telephoneTextField.text = user.telephone
        mobileTextField.text = user.mobilePhone
        birthdayTextField.text = user.birthdayDate
        cityTextField.text = user.city
        streetTextField.text = user.street
        zipTextField.text = user.zipCode
        nationalityTextField.text = user.nationality
        genderTextField.text = user.genderType.localized()
        
        if user.companyName.isEmpty {
            companyNameBackgroundView.isHidden = true
            cityBackgroundView.topAnchor.constraint(equalTo: genderBackgroundView.bottomAnchor, constant: 10).isActive = true
        }
        companyNameTextField.text = user.companyName
    }
    
    func validate() -> ValidationResult {
        guard
            !passwordTextField.text!.isEmpty,
            !confirmPasswordTextField.text!.isEmpty else { return .emptyFields }
        guard passwordTextField.text!.isValidPassword(name: firstNameTextField.text!, surname: lastNameTextField.text!, date: birthdayTextField.text!) else { return .incorrectPassword }
        guard passwordTextField.text == confirmPasswordTextField.text else { return .incorrectConfirmationPassword }
        return .valid
    }
    
    private func setupView(view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 0.2
        view.layer.borderColor = #colorLiteral(red: 0.745223105, green: 0.754733026, blue: 0.7544236779, alpha: 1)
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self)
    }
    
    @IBAction func didTapFinishRegistrationButton(_ sender: UIButton) {
        delegate?.didTapFinishRegistrationButton(vc: self, onboardingVc: onboardingVC!)
    }
}

extension PartnerRegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        moveResponder(textField: textField)
        return true
    }
}
