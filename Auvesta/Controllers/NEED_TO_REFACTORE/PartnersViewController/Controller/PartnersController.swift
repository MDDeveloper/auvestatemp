import UIKit

class PartnersController {
    private var viewController: PartnersViewController?
    private var currentPartner: AuvestaPartner?
    
    init(viewController: PartnersViewController) {
        self.viewController = viewController
    }
}

extension PartnersController: PartnersViewControllerDelegate {
    func didTapPartnerCell(vc: PartnersViewController, cell: UITableViewCell) {
        let cell = cell as? PartnersTableViewCell
        let selectedPartnerVc = vc.storyboard?.instantiateViewController(withIdentifier: "PartnersViewController") as! PartnersViewController
        selectedPartnerVc.partnerSponsorID = cell?.partnerData?.partnerSponsorID
        vc.navigationController?.pushViewController(selectedPartnerVc, animated: true)
    }
    
    func didTapBackButton(vc: PartnersViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
}

extension PartnersController: PartnersViewControllerDataSource {
    func getCurrentPartnerImage(vc: PartnersViewController) {
        guard let url = URL(string: currentPartner!.partnerImageLink) else { return }
        vc.partnerImageView.sd_setImage(with: url, completed: nil)
    }
    
    func getCurrentPartnerData(vc: PartnersViewController, partnerID: String) {
        vc.notificationsButton.startActivityIndicator()
        MainNetworkManager.getReferralInfo(referralID: partnerID) { (status, partner, error) in
            vc.notificationsButton.stopActivityIndicator()
            switch status {
            case 200:
                self.currentPartner = partner
                DispatchQueue.main.async {
                    vc.partnersTableView.reloadData()
                    vc.updatePartnerInfo(partner: partner!)
                }
            case 401:
                vc.navigationController?.popToRootViewController(animated: true)
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
        
        //        MainNetworkManager.shared.updateReferralNetworkData(referralID: partnerID) { (status, partner, error) in
        //            vc.notificationsButton.stopActivityIndicator()
        //            switch status {
        //            case 200:
        //                self.currentPartner = partner
        //                DispatchQueue.main.async {
        //                    vc.partnersTableView.reloadData()
        //                    vc.updatePartnerInfo(partner: partner!)
        //                }
        //            case 401:
        //                vc.navigationController?.popToRootViewController(animated: true)
        //            default:
        //                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        //                vc.addAlert(title: "somethingWrongTitle".localized(), message: error!, actions: [okAction])
        //            }
        //        }
    }
    
    func getReferralsCount(vc: PartnersViewController) -> Int {
        return currentPartner?.partners.count ?? 0
    }
    
    func getCurrentReferral(vc: PartnersViewController, indexPath: IndexPath) -> AuvestaPartner {
        return currentPartner!.partners[indexPath.row]
    }
}
