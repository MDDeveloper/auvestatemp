import UIKit

protocol PartnersViewControllerDelegate: AnyObject {
    func didTapBackButton(vc: PartnersViewController)
    func didTapPartnerCell(vc: PartnersViewController, cell: UITableViewCell)
}

protocol PartnersViewControllerDataSource: AnyObject {
    func getCurrentPartnerData(vc: PartnersViewController, partnerID: String)
    func getCurrentPartnerImage(vc:PartnersViewController)
    func getCurrentReferral(vc: PartnersViewController, indexPath: IndexPath) -> AuvestaPartner
    func getReferralsCount(vc: PartnersViewController) -> Int
}

class PartnersViewController: UIViewController {
    
    @IBOutlet private weak var partnersTableHeaderLabel: UILabel!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet weak var notificationsButton: ButtonWithActivityIndicator!
    @IBOutlet weak var partnerImageView: UIImageView!
    @IBOutlet private weak var partnerBackgroundView: UIView!
    @IBOutlet private weak var partnerImageBackgroundView: UIView!
    @IBOutlet private weak var partnerNameLabel: UILabel!
    @IBOutlet private weak var partnersCountLabel: UILabel!
    @IBOutlet private weak var partnerExternalIdLabel: UILabel!
    
    @IBOutlet weak var partnersTableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate: PartnersViewControllerDelegate?
    var dataSource: PartnersViewControllerDataSource?
    var controller: PartnersController?
    var partnerSponsorID: String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        localize()
        setController(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.dataSource?.getCurrentPartnerData(vc: self, partnerID: self.partnerSponsorID!)
    }
    
    func updatePartnerInfo(partner: AuvestaPartner) {
        partnersCountLabel.text = "\(partner.partners.count)" + "partnersVcTableHeader".localized()
        partnerNameLabel.text = partner.partnerName ?? "not loaded"
        partnerExternalIdLabel.text = partner.externalPartnerId ?? ""
        guard let url = URL(string: partner.partnerImageLink) else { return }
        partnerImageView.sd_setImage(with: url, completed: nil)
    }
    
    private func localize() {
        partnersTableHeaderLabel.text = "partnersVcHeader".localized()
        headerTitleLabel.text = "partnersVcTableHeader".localized()
    }
    
    private func setController(viewController: PartnersViewController) {
        controller = PartnersController(viewController: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    private func updateUI() {
        partnerBackgroundView.layer.cornerRadius = 10
        partnerBackgroundView.layer.shadowColor = UIColor.black.cgColor
        partnerBackgroundView.layer.shadowRadius = 10
        partnerBackgroundView.layer.shadowOpacity = 0.3
        
        partnerImageBackgroundView.layer.cornerRadius = partnerImageBackgroundView.bounds.size.height / 2
        partnerImageBackgroundView.layer.borderColor = #colorLiteral(red: 0.8270336986, green: 0.6961463094, blue: 0.3867461383, alpha: 1)
        partnerImageBackgroundView.layer.borderWidth = 1
        
        partnerImageView.layer.cornerRadius = partnerImageView.bounds.size.height / 2
        partnerImageView.layer.borderColor = #colorLiteral(red: 0.989490211, green: 0.9902383685, blue: 0.9896060824, alpha: 1)
        partnerImageView.layer.borderWidth = 4
    }
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self)
    }
}

extension PartnersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? PartnersTableViewCell else { return }
        delegate?.didTapPartnerCell(vc: self, cell: cell)
    }
}

extension PartnersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.getReferralsCount(vc: self) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "partnersCell", for: indexPath) as! PartnersTableViewCell
        cell.selectionStyle = .none
        cell.partnerData = dataSource?.getCurrentReferral(vc: self, indexPath: indexPath)
        return cell
    }
}
