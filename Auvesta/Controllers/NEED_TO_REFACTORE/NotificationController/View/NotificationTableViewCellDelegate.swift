import Foundation
import UIKit

protocol NotificationTableViewCellDelegate: AnyObject {
    func didTappedViewButton()
}
