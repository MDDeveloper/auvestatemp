import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var notificationBodyLabel: UILabel!
    
    var delegate: NotificationTableViewCellDelegate?
    
    @IBAction func didTappedViewButton(_ sender: UIButton) {
        delegate?.didTappedViewButton()
    }
    

}
