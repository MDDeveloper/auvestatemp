import UIKit

//MARK: - Notifications view controller delegate

protocol NotificationsViewControllerDelegate {
    func didBackButtonTapped(vc: NotificationViewController)
}

//MARK: - Notifications view controller data source

protocol NotificationsViewControllerDataSource {
    
}

//MARK: - NotificationsViewController

class NotificationViewController: UIViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var notificationTableView: UITableView!
    
    //MARK: - Properties
    
    var delegate: NotificationsViewControllerDelegate?
    var dataSource: NotificationsViewControllerDataSource?
    
    var notifications: [AUNotification] = [] {
        didSet {
            DispatchQueue.main.async {
                self.notificationTableView.reloadData()
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notifications = CoreDataManager.shared.fetchNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    //MARK: - Methods
    
    private func setupUI() {
        guard notifications.isEmpty else { return }
        let emptyTableLabel = UILabel()
        emptyTableLabel.font = .systemFont(ofSize: 17)
        emptyTableLabel.text = "You don't have notifications yet"
        emptyTableLabel.textAlignment = .center
        emptyTableLabel.textColor = .gray
        emptyTableLabel.numberOfLines = 0
        emptyTableLabel.bounds.size = CGSize(width: 200, height: 50)
        emptyTableLabel.center = view.center
        view.addSubview(emptyTableLabel)
    }
    
    @IBAction func didBackButtonTapped(_ sender: UIButton) {
        delegate?.didBackButtonTapped(vc: self)
    }
    
}

//MARK: - Table view Data Source

extension NotificationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as? NotificationTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        let notification = notifications.reversed()[indexPath.row]
        cell.titleLabel.text = notification.title
        cell.notificationBodyLabel.text = notification.body
        

        return cell
    }
    
    
}

//MARK: - Table view Delegate

extension NotificationViewController: UITableViewDelegate {
    
}

//MARK: - NotificationTableViewCellDelegate

extension NotificationViewController: NotificationTableViewCellDelegate {
    func didTappedViewButton() {
        guard let navigationVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
        guard let tabBarVC = navigationVC.topViewController as? TabBarViewController else { return }
        tabBarVC.selectedIndex = 0
    }
}


