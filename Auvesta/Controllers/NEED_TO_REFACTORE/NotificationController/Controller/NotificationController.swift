import Foundation
import UIKit

final class NotificationController {
    
    
    init(vc: NotificationViewController) {
        vc.delegate = self
        vc.dataSource = self
    }
}

extension NotificationController: NotificationsViewControllerDelegate {
    func didBackButtonTapped(vc: NotificationViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationController: NotificationsViewControllerDataSource {
    
}
