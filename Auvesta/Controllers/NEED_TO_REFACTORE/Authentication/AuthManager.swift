import Foundation
import Locksmith

final class AuthManager {
    
    static let shared = AuthManager()
    
    private let account: String = AuvestaUser.shared.username
    private let biometriksService = BiometriksService()
    
    //MARK: - Authentification Properties
        
    var isBiometricksEnabled: Bool {
        get { return UserDefaults.standard.bool(forKey: "isBiometricksEnabled") }
        set { UserDefaults.standard.set(newValue, forKey: "isBiometricksEnabled") }
    }
    
    var biometriksSettingsText: String {
         return biometriksService.biometriksSettingsText
    }
    
    var biometryAlertText: String? {
        return biometriksService.biometryAlertText
    }
    
    var authentificationError: String? {
        return biometriksService.errorMessage
    }
    
    var canEvaluate: Bool {
        return biometriksService.canEvaluate
    }
    
    var isCredentialsSaved: Bool {
        get { return UserDefaults.standard.bool(forKey: "isSaved") }
        set { UserDefaults.standard.set(newValue, forKey: "isSaved") }
    }
    
    //MARK: - Updating Credentials
    
    func updateAccountCredentials(_ password: String, email: String) {
        do {
            guard password != "", email != "" else { print("empty fields"); return }
            try Locksmith.updateData(data: ["password" : password, "email" : email ], forUserAccount: account)
            isCredentialsSaved = true
            print("SAVED--------------------")
            return
        } catch  {
            print(error)
            //fatalError()
        }
    }
    
    func updateAccountToken(_ token: String) {
        do {
            try Locksmith.updateData(data: ["token" : token ], forUserAccount: "token")
            print("UPDATE TOKEN-------------------")
        } catch {
            print(error)
            //fatalError("Didn't saved token")
        }
    }
    
    //MARK: - Getting Credentials
    
    func getLoginCredentials() -> [String: String] {
        let dictionary = Locksmith.loadDataForUserAccount(userAccount: account)
        guard let authDictionary = dictionary as? [String: String] else { return [:] }
        return authDictionary
    }
    
    func getAccountToken() -> String {
        guard let dict = Locksmith.loadDataForUserAccount(userAccount: "token") as? [String: String] else { return "" }
        guard let token = dict["token"] else { return "" }
        return token
    }
    
    //MARK: - Deleting
    
    func deleteSavedCredentials() {
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: account)
            print("DELETE -------------------")
        } catch let deleteError {
          
        }
    }
    
    //MARK: - Checking Credentials
    
    func isAlreadySaved(_ password: String, email: String) -> Bool {
        guard isCredentialsSaved else { return false }
        guard let accountCredentials = Locksmith.loadDataForUserAccount(userAccount: account) else { return false }
        guard let savedPassword = accountCredentials["password"] as? String else { return false }
        guard let savedEmail = accountCredentials["email"] as? String else { return false }
        guard
            password == savedPassword,
            email == savedEmail else { return false }
        return true
    }
    
    //MARK: - Biometry Methods
    
    func authentificateUser(vc: UIViewController, _ complition: @escaping ((String, String) -> Void)) {
        let credentials = getLoginCredentials()
        guard credentials != [:] else { deleteSavedCredentials(); return }
        guard let password = credentials["password"] else { return  }
        guard let email = credentials["email"] else { return }
        biometriksService.authentificateUser(vc: vc, password: password, email: email, complition: complition)
    }
    
    func setBiometriksPermission(vc: UIViewController, _ complition: @escaping ((Bool) -> Void)) {
        if isBiometricksEnabled {
            isBiometricksEnabled = false
            complition(false)
        } else {
            biometriksService.setPermission(vc: vc) { success in
                self.isBiometricksEnabled = success
                complition(success)
            }
        }
        
    }
    
     //MARK: - Other
    
    func logOutUser() {
        deleteSavedCredentials()
        isBiometricksEnabled = false
        isCredentialsSaved = false
    }
    
    private init() {
        
    }
}

