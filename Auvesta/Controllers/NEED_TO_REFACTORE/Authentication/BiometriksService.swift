import Foundation
import LocalAuthentication
import UIKit

final class BiometriksService {
    
    private let authContext = LAContext()
    private var authentificationText = "signInText".localized()
    
    private var biometryType: String {
        get { UserDefaults.standard.string(forKey: "biometryType") ?? "" }
        set { UserDefaults.standard.set(newValue, forKey: "biometryType") }
    }
    
    var errorMessage: String?
    
    var biometriksSettingsText: String {
        guard canEvaluate else { return "BiometricksDiscryptionText".localized() + biometryType }

        switch authContext.biometryType {
        case .faceID:
            biometryType = "Face ID"
            return "BiometricksDiscryptionText".localized() + "Face ID"
        case .touchID:
            biometryType = "Touch ID"
            return "BiometricksDiscryptionText".localized() + "Touch ID"
        default:
           return "BiometricksDiscryptionText".localized() + biometryType
        }
    }
    
    var biometryAlertText: String {
        guard canEvaluate else { return "Do you want use biometriks to sign in?" }

        switch authContext.biometryType {
        case .faceID:
            biometryType = "Face ID"
            return  "faceIDpermission".localized()
        case .touchID:
            biometryType = "Touch ID"
            return  "touchIDpermission".localized()
        default:
           return "Do you want use biometriks to sign in?"
        }
    }
    
    var permissionText: String {
        guard canEvaluate else { return "allowBiometriksText".localized() + biometryType + "?" }
        
        switch authContext.biometryType {
        case .faceID:
            biometryType = "Face ID"
            return "allowBiometriksText".localized() + "Face ID?"
        case .touchID:
            biometryType = "Touch ID"
            return "allowBiometriksText".localized() + "Touch ID?"
        default:
            return "allowBiometriksText".localized() + biometryType + "?"
        }
    }
    
    var canEvaluate: Bool {
        return authContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
    }
    
    func authentificateUser(vc: UIViewController, password: String, email: String, complition: @escaping ((String, String) -> Void)) {
        guard canEvaluate else { return }
        
        authContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: authentificationText) { [unowned self] success, error in
            guard !success else { complition(password, email); return }
            guard let error = error else { return }
            switch error {
            case LAError.authenticationFailed: self.errorMessage = "Authentication Failed"
            case LAError.biometryNotEnrolled: self.errorMessage = "Biometry Not Enrolled"
            case LAError.passcodeNotSet: self.errorMessage = "Passcode Not Set"
            case LAError.biometryNotAvailable: self.errorMessage = "Biometry Not Available"
            case LAError.biometryLockout: self.errorMessage = "Biometry Lockout"
            default: self.errorMessage = nil
            }
            self.showErrorAlert(vc: vc)
        }
    }
    
    func setPermission(vc: UIViewController, _ complition: @escaping ((Bool) -> Void))  {
        guard canEvaluate else { return }
        
        authContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: permissionText) { [unowned self] success, error in
            guard !success else { complition(true); return }
            guard let error = error else { return }
            switch error {
            case LAError.authenticationFailed: self.errorMessage = "Authentication Failed"
            case LAError.biometryNotEnrolled: self.errorMessage = "Biometry Not Enrolled"
            case LAError.passcodeNotSet: self.errorMessage = "Passcode Not Set"
            case LAError.biometryNotAvailable: self.errorMessage = "Biometry Not Available"
            case LAError.biometryLockout: self.errorMessage = "Biometry Lockout"
            default: self.errorMessage = nil
            }
            self.showErrorAlert(vc: vc)
            complition(false)
        }
    }
    
    private func showErrorAlert(vc: UIViewController) {
        guard let error = errorMessage else { return }
        let alertController = UIAlertController(title: "AllStars", message: error, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel)
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            vc.present(alertController, animated: true, completion: nil)
        }
    }
    
}
