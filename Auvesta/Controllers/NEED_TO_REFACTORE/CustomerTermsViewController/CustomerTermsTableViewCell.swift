import UIKit

class CustomerTermsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkboxButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    var indexPath: IndexPath?
    var delegate: CustomerTermsCellDelegate?
    var data: CustomerTerms? {
        didSet {
            updateData()
        }
    }
    
    private func updateData() {
        guard
            let data = data,
            let indexPath = indexPath else { return }
        textView.attributedText = configure(terms: data, indexPath: indexPath)
    }
    
    private func configure(terms: CustomerTerms, indexPath: IndexPath) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        var valueString = NSAttributedString()
        var linkString = NSMutableAttributedString()
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let attributes: [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.4283095598, green: 0.426494807, blue: 0.4946352839, alpha: 1),
                                                          NSAttributedString.Key.paragraphStyle: paragraph,
                                                          NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        
        
        switch indexPath.row {
        case 0:
            valueString = NSAttributedString(string: terms.checkboxOneText + "\n", attributes: attributes)
            mutableAttributedString.append(valueString)
            
            linkString = NSMutableAttributedString(string: terms.tacText + "\n")
            linkString.addAttribute(.link, value: terms.tacLink, range: NSRange(location: 0, length: linkString.string.count))
            mutableAttributedString.append(linkString)
            
            linkString = NSMutableAttributedString(string: terms.distanceSellingText + "\n")
            linkString.addAttribute(.link, value: terms.distanceSellingLink, range: NSRange(location: 0, length: linkString.string.count))
            mutableAttributedString.append(linkString)
            
            linkString = NSMutableAttributedString(string: terms.revocationText + "\n")
            linkString.addAttribute(.link, value: terms.revocationLink, range: NSRange(location: 0, length: linkString.string.count))
            mutableAttributedString.append(linkString)
            
            return mutableAttributedString
        case 1:
            valueString = NSAttributedString(string: terms.checkboxTwoText + "\n", attributes: attributes)
            linkString = NSMutableAttributedString(string: terms.privacyText)
            linkString.addAttribute(.link, value: terms.privacyLink, range: NSRange(location: 0, length: linkString.string.count))
            mutableAttributedString.append(valueString)
            mutableAttributedString.append(linkString)
            return mutableAttributedString
        case 2:
            valueString = NSAttributedString(string: terms.checkboxThreeText, attributes: attributes)
        case 3:
            valueString = NSAttributedString(string: terms.checkboxFourDescriptionText, attributes: attributes)
        default:
            break
        }
        
        mutableAttributedString.append(valueString)
        return mutableAttributedString
    }
    
    @IBAction func didTapCheckboxButton(_ sender: UIButton) {
        guard let indexPath = indexPath else { return }
        delegate?.didTapActionButton(cell: self, indexPath: indexPath)
    }
}
