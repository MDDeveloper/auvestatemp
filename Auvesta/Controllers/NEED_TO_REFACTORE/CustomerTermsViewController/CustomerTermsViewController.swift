import UIKit

protocol CustomerTermsViewControllerDelegate: AnyObject {
    func didTapAgreeButton(vc: CustomerTermsViewController)
    func didTapDisagreeButton(vc: CustomerTermsViewController)
    func didTapNewsletterButton(vc: CustomerTermsViewController, sender: UIButton)
}

class CustomerTermsViewController: UIViewController {
    
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var termsTableView: UITableView!
    @IBOutlet private weak var agreeButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var backgroundView: UIView!
    
    var viewController: UIViewController?
    var delegate: CustomerTermsViewControllerDelegate?
    var checkboxStateArray = [Bool]()
    var customerTermsData: CustomerTerms?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsTableView.dataSource = self
        termsTableView.delegate = self
        setupUI()
        downloadTerms()
    }
    
    private func downloadTerms() {
        agreeButton.startActivityIndicator()
        RegistrationNetworkManager.getCustomerTerms { (status, terms, error) in
            self.agreeButton.stopActivityIndicator()
            switch status {
            case 200:
                DispatchQueue.main.async {
                    self.customerTermsData = terms
                    self.checkboxStateArray = [false, false, false, false]
                    self.agreeButton.setTitle(terms?.buttonText, for: .normal)
                    self.termsTableView.reloadData()
                    self.agreeButton.isEnabled = true
                }
            default:
                self.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
    
    private func setupUI() {
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.shadowColor = UIColor.black.cgColor
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundView.layer.shadowOpacity = 0.4
        backgroundView.layer.shadowRadius = 4
        
        agreeButton.layer.cornerRadius = 5
        agreeButton.setTitle("", for: .normal)
        headerTitleLabel.text = "customerTermsVcHeader".localized()
        
        agreeButton.isEnabled = false
    }
    
    @IBAction func didTapAgreeButton(_ sender: ButtonWithActivityIndicator) {
        guard
            checkboxStateArray[0] == true,
            checkboxStateArray[1] == true,
            checkboxStateArray[2] == true else {
                addAlert(title: "emptyCheckboxTitle".localized(),
                         message: "emptyCheckboxMessage".localized())
                return
        }
        delegate?.didTapAgreeButton(vc: self)
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        delegate?.didTapDisagreeButton(vc: self)
    }
}

extension CustomerTermsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checkboxStateArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomerTermsTableViewCell
        cell.delegate = self
        cell.indexPath = indexPath
        cell.checkboxButton.isSelected = checkboxStateArray[indexPath.row]
        cell.data = customerTermsData

        return cell
    }
}

extension CustomerTermsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension CustomerTermsViewController: CustomerTermsCellDelegate {
    func didTapActionButton(cell: CustomerTermsTableViewCell, indexPath: IndexPath) {
        guard indexPath.row != 3 else {
            delegate?.didTapNewsletterButton(vc: self, sender: cell.checkboxButton)
            return
        }
        
        cell.checkboxButton.isSelected.toggle()
        self.checkboxStateArray[indexPath.row] = cell.checkboxButton.isSelected
    }
}
