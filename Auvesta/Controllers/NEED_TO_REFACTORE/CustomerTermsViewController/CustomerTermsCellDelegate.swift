import Foundation

protocol CustomerTermsCellDelegate: AnyObject {
    func didTapActionButton(cell: CustomerTermsTableViewCell, indexPath: IndexPath)
}
