import Foundation
import UserNotifications

final class AUPushNotification: UNNotification {
    var type: AUNotificationType {
        guard let type = request.content.userInfo["type"] as? String else { return .unknown }
        switch type {
        case "depot": return .depotView
        case "pools": return .poolsView
        case "refer": return .referView
        case "account": return .accountView
        case "network": return .networkView
        case "eservice": return .eService
        default:
            return .unknown
        }
    }
    
    var category: String? {
        return request.content.categoryIdentifier
    }
    
}
