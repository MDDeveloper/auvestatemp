import Foundation

enum AUNotificationType: String {
    case depotView = "depot"
    case networkView = "network"
    case poolsView = "pools"
    case referView = "refer"
    case eService = "eservice"
    case accountView = "account"
    case unknown = ""
}
