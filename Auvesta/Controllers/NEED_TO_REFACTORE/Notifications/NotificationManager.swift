import Foundation
import UserNotifications
import Firebase

final class NotificationManager: NSObject {
    
    //MARK: - Properties
    
    private let gcmMessageIDKey = "gcm.message_id"
    
    //MARK: - Methods
    
    func setDelegates() {
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
    }
    
    func requestAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        setNotificationsCategories()
    }
    
    func setNotificationsCategories() {
        let viewAction = UNNotificationAction(identifier: "viewAction", title: "View", options: [.foreground])
        let deleteAction = UNNotificationAction(identifier: "deleteAction", title: "Delete", options: .destructive)
        
        let interactivePushCategory = UNNotificationCategory(identifier: "viewPush", actions: [viewAction, deleteAction], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([interactivePushCategory])
    }
    
    private func viewNotification(with type: AUNotificationType) {
        guard let window = UIApplication.shared.delegate?.window as? UIWindow else { return }
        guard let navVC = window.rootViewController as? UINavigationController else { return }
        guard let tabBar = navVC.topViewController as? TabBarViewController else { return }
        
        switch type {
        case .accountView: tabBar.selectedIndex = 3
        case .referView: tabBar.selectedIndex = 2
        case .depotView: tabBar.selectedIndex = 1
        case .poolsView: tabBar.selectedIndex = 0
        case .networkView: tabBar.selectedIndex = 2
        case .eService: tabBar.selectedIndex = 1
        default:
            print("UNKNOWN")
        }
        //view tab bar
    }
    
}

//MARK: - User Notifications Delegate

extension NotificationManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        print("OPEN SETIINGS______----------")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        guard let auNotification = response.notification as? AUPushNotification else { return }
        // when response is recieved
        let body = auNotification.request.content.body
        let title = auNotification.request.content.title
        let type = auNotification.type
        viewNotification(with: type)
        CoreDataManager.shared.createNotification(of: AUNotification.self, body: body, title: title)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //before presentastion i foreground
        completionHandler([.alert, .sound])
        print("WILL PRESENT")
        guard let auNotification = notification as? AUPushNotification else { return }
        let body = auNotification.request.content.body
        let title = auNotification.request.content.title
        CoreDataManager.shared.createNotification(of: AUNotification.self, body: body, title: title)
        
        
    }
}

//MARK: - Firebase Messaging Delegate

extension NotificationManager: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.setValue(fcmToken, forKey: "FireBaseToken")
         
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
               print("Received data message: \(remoteMessage.appData)")
    }
}

//MARK: - Handling Silent Notifications

extension NotificationManager {
    func didRecievedSilentNotification(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
    }
}
