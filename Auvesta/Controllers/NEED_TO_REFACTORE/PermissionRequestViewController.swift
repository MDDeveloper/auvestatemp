import UIKit

protocol PermissionRequestViewControllerDelegate {
    func didTapOkButton(vc: PermissionRequestViewController, parentViewController: UIViewController?)
    func didTapCancelButton(vc: PermissionRequestViewController)
}

class PermissionRequestViewController: UIViewController {
    
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var okButton: UIButton!
    
    @IBOutlet weak var permissionImageView: UIImageView!
    @IBOutlet weak var permissionLabel: UILabel!
    
    var viewController: UIViewController?
    var delegate: PermissionRequestViewControllerDelegate?
    var onViewDidLoad: (()->())? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalPresentationStyle = .fullScreen
        onViewDidLoad?()
        setupUI()
    }
    
    private func setupUI() {
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.shadowColor = UIColor.black.cgColor
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundView.layer.shadowOpacity = 0.4
        backgroundView.layer.shadowRadius = 4
        
        permissionImageView.layer.cornerRadius = 13
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 3)
        shadowView.layer.shadowOpacity = 0.4
        shadowView.layer.shadowRadius = 4
        shadowView.layer.cornerRadius = 13
        
        okButton.layer.cornerRadius = 5
    }
    
    @IBAction private func didTapOkButton(_ sender: UIButton) {
        delegate?.didTapOkButton(vc: self, parentViewController: nil)
    }
    
    @IBAction private func didTapCancelButton(_ sender: UIButton) {
        delegate?.didTapCancelButton(vc: self)
    }
}
