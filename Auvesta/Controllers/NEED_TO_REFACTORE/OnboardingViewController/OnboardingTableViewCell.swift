import UIKit

class OnboardingTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var imageBackgroundView: UIView!
    @IBOutlet private weak var uspImageView: UIImageView!
    @IBOutlet private weak var uspLabel: UILabel!
    
    var data: PartnershipUsp? {
        didSet {
            uspImageView.image = nil
            self.updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func updateData() {
        guard let usp = data else { return }
        
        switch usp.type {
        case .title, .header, .item:
            uspLabel?.attributedText = getUspAttributedString(text: usp.text)
            guard let url = URL(string: usp.imageLink) else { return }
            uspImageView.sd_setImage(with: url, completed: nil)
        }
    }
    
    private func setupUI() {
        uspImageView.layer.cornerRadius = uspImageView.bounds.size.height / 2
        imageBackgroundView.layer.cornerRadius = imageBackgroundView.bounds.size.height / 2
        imageBackgroundView.backgroundColor = #colorLiteral(red: 0.03540587769, green: 0.03511084539, blue: 0.04567808444, alpha: 0.297463613)
    }
    
    private func getUspAttributedString(text: String) -> NSMutableAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        let attributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9371492863, green: 0.7090170383, blue: 0.2507658601, alpha: 1)]
        let textString = NSAttributedString(string: text, attributes: attributes)
        mutableAttributedString.append(textString)
        return mutableAttributedString
    }
}
