import UIKit

protocol OnboardingViewControllerDelegate: AnyObject {
    func didTapActionButton(vc: OnboardingViewController, parentViewController: UIViewController?)
    func didTapCancelButton(vc: OnboardingViewController, parentViewController: UIViewController?)
}

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: ButtonWithActivityIndicator!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var uspTableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate: OnboardingViewControllerDelegate?
    var viewController: UIViewController?
    var onViewDidLoad: (()->())? = nil
    private var uspData = [PartnershipUsp]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalPresentationStyle = .fullScreen
        onViewDidLoad?()
        setupUI()
        downloadUspData()
    }
    
    private func downloadUspData() {
        RegistrationNetworkManager.getUspDetails { (status, usp, error) in
            switch status {
            case 200:
                guard let sortedUsp = usp?.sorted() else { return }
                self.uspData = sortedUsp
                DispatchQueue.main.async {
                    self.uspTableView.reloadData()
                }
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                self.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    private func setupUI() {
        modalTransitionStyle = .crossDissolve
        actionButton.layer.cornerRadius = 10
        titleLabel.text = "partnershipOnboardingVcHeader".localized()
        actionButton.setTitle("partnershipOnboardingActionButton".localized(), for: .normal)
        cancelButton.setTitle("partnershipOnboardingCancelButton".localized(), for: .normal)
        uspTableView.dataSource = self
        uspTableView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "becomePartnerSegue" {
            let partnerRegistrationVc = segue.destination as! PartnerRegistrationViewController
            partnerRegistrationVc.modalPresentationStyle = .fullScreen
            partnerRegistrationVc.onboardingVC = self
        }
    }
    
    @IBAction func didTapActionButton(_ sender: ButtonWithActivityIndicator) {
        delegate?.didTapActionButton(vc: self, parentViewController: viewController)
    }
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        delegate?.didTapCancelButton(vc: self, parentViewController: viewController)
    }
}

extension OnboardingViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return uspData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "onboardingCell", for: indexPath) as! OnboardingTableViewCell
        cell.data = uspData[indexPath.row]
        return cell
    }
}
