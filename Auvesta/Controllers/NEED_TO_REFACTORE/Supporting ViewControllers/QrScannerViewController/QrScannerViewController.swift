import UIKit
import AVFoundation

protocol QrScannerViewControllerDelegate: AnyObject {
    func didFoundAndDecodeQrCode(qrVc: QrScannerViewController, sponsorID: String, scanSponsordIdViewController: ScanSponsorIdViewController)
    func didTapBackbutton(qrVc: QrScannerViewController)
}

class QrScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet private weak var decodedQrLabel: UILabel!
    @IBOutlet private weak var viewForCapturedImage: UIView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    private var captureSession: AVCaptureSession?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var qrCodeFrameView: UIView?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var delegate: QrScannerViewControllerDelegate?
    private var controller: ScanSponsorIdController?
    
    var scanSponsorIdViewController: ScanSponsorIdViewController?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        isModalInPresentation = false
        headerTitleLabel.text = "qrVcHeader".localized()
        setController(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkAuthorizationStatus()
    }
    
    // MARK: - Methods
    
    private func setController(viewController: QrScannerViewController) {
        controller = ScanSponsorIdController(QrVc: viewController)
        viewController.delegate = controller
    }
    
    private func checkAuthorizationStatus() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
        
        switch authStatus {
        case .authorized:
            startScan()
        case .notDetermined:
            requestCameraPermissions()
        case .denied:
            showPermissionSettingsVC()
        default:
            showPermissionSettingsVC()
        }
    }
    
    private func requestCameraPermissions() {
        AVCaptureDevice.requestAccess(for: .video) { (granted) in
            if granted {
                DispatchQueue.main.async {
                    self.startScan()
                }
            } else {
                DispatchQueue.main.async {
                    self.showPermissionSettingsVC()
                }
            }
        }
    }
    
    private func showPermissionSettingsVC() {
        let storyboard = UIStoryboard(name: "AlertViewController", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        
        controller.viewController = self
        controller.delegate = self
        controller.onViewDidLoad = {
            controller.oneTimeCodeTextField.isHidden = true
            controller.alertTitleLabel.text = "Camera permission denied"
            controller.alertTextLabel.text = "You can turn it on in the settings or enter SponsorID manually."
            controller.alertMainActionButton.setTitle("Go to settings", for: .normal)
            controller.alertSecondaryActionButton.setTitle("Enter manually", for: .normal)
            controller.alertCloseButton.isHidden = true
        }
        
        controller.modalPresentationStyle = .overCurrentContext
        present(controller, animated: true)
    }

    private func startScan() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInWideAngleCamera],
                                                                      mediaType: AVMediaType.video,
                                                                      position: .back)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("error, didn't find device with camera")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = viewForCapturedImage.bounds
            viewForCapturedImage.layer.addSublayer(videoPreviewLayer!)
            
            captureSession?.startRunning()
            
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = #colorLiteral(red: 0.9036028385, green: 0.7037108541, blue: 0.185172528, alpha: 1)
                qrCodeFrameView.layer.cornerRadius = 15
                qrCodeFrameView.layer.borderWidth = 4
                viewForCapturedImage.addSubview(qrCodeFrameView)
                viewForCapturedImage.bringSubviewToFront(qrCodeFrameView)
            }
        } catch {
            print(error)
            return
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            decodedQrLabel.text = "No QR code is detected"
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                decodedQrLabel.text = metadataObj.stringValue
                delegate?.didFoundAndDecodeQrCode(qrVc: self, sponsorID: metadataObj.stringValue!, scanSponsordIdViewController: scanSponsorIdViewController!)
            }
        }
    }
    
    @IBAction func didTapBackbutton(_ sender: UIButton) {
        delegate?.didTapBackbutton(qrVc: self)
    }
}

extension QrScannerViewController: AlertViewControllerDelegate {
    func didTapMainActionButton(vc: AlertViewController, parentViewController: UIViewController) {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
    
    func didTapSecondaryActionButton(vc: AlertViewController, parentViewController: UIViewController) {
        let qrScannerViewController = parentViewController as! QrScannerViewController
        
        vc.dismiss(animated: true) {
            qrScannerViewController.dismiss(animated: true, completion: {
                qrScannerViewController.scanSponsorIdViewController?.sponsorIdTextField.becomeFirstResponder()
            })
        }
    }
}
