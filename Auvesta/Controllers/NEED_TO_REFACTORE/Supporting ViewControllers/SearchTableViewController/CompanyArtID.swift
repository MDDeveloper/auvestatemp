import Foundation

class CompanyArtID {
    let id: Int
    let text: String

    static func getArray(from jsonArray: Any) -> [CompanyArtID] {
        guard let jsonArray = jsonArray as? [Any] else { return [CompanyArtID]() }
        guard let jsonDictionary = jsonArray as? Array<[String:Any]> else { return [CompanyArtID]() }
        return jsonDictionary.compactMap{ CompanyArtID(jsonArray: $0) }
    }

    init(jsonArray: [String:Any]) {
        let id = jsonArray["id"] as? Int
        let text = jsonArray["text"] as? String

        self.id = id ?? 0
        self.text = text ?? ""
    }
}
