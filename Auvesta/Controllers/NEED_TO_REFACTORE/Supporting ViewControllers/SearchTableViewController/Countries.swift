import Foundation

class Country {
    let iso: String
    let is3: String
    let countryName: String
    let countryPhoneCode: Int
    
    static func getArray(from jsonArray: Any) -> [Country] {
        guard let jsonArray = jsonArray as? [Any] else { return [Country]() }
        guard let jsonDictionary = jsonArray[0] as? Array<[String:Any]> else { return [Country]() }
        return jsonDictionary.compactMap{ Country(jsonArray: $0) }
    }

    init(jsonArray: [String:Any]) {        
        let name = jsonArray["countryName"] as? String
        let is03 = jsonArray["is03"] as? String
        let countryPhoneCode = jsonArray["countryPhoneCode"] as? Int
        let iso = jsonArray["iso"] as? String
        
        self.iso = iso ?? ""
        self.countryName = name ?? ""
        self.is3 = is03 ?? ""
        self.countryPhoneCode = countryPhoneCode ?? 0
    }
}
