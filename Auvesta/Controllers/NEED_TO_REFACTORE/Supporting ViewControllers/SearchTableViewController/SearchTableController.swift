import UIKit

class SearchTableController: NSObject {
    private var viewController: SearchTableViewController?
    
    var countriesArray = [Country]()
    var currentCountriesArray = [Country]()
    
    var artIdArray = [CompanyArtID]()
    var currentArtIdArray = [CompanyArtID]()
    
    
    private func updateCompanyArtID() {
        RegistrationNetworkManager.getCompanyArtIdData { (status, artIDs, error) in
            switch status {
            case 200:
                guard let artIDs = artIDs else { return }
                self.artIdArray = artIDs
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                self.viewController?.addAlert(title: "", message: error, actions: [okAction])
            }
            self.currentArtIdArray = self.artIdArray
            DispatchQueue.main.async {
                self.viewController?.tableView.reloadData()
            }
        }
    }
    
    private func updateCountries() {
        RegistrationNetworkManager.getCountriesDetails { (status, countries, error) in
            switch status {
            case 200:
                guard let countries = countries else { return }
                self.countriesArray = countries
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                self.viewController?.addAlert(title: "", message: error, actions: [okAction])
            }
            self.currentCountriesArray = self.countriesArray
            DispatchQueue.main.async {
                self.viewController?.tableView.reloadData()
            }
        }
    }
    
    init(viewController: SearchTableViewController) {
        super.init()
        self.viewController = viewController
        switch viewController.currentCase {
        case .country:
            self.updateCountries()
        case .artID:
            self.updateCompanyArtID()
        }
    }
}

extension SearchTableController: SearchTableViewControllerDelegate {
    func didTapCancelButton(vc: SearchTableViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
}

extension SearchTableController: SearchTableViewControllerDataSource {
    func getCurrentArtID(vc: SearchTableViewController, indexPath: IndexPath) -> CompanyArtID {
        return currentArtIdArray[indexPath.row]
    }
    
    func getCurrentCountry(vc: SearchTableViewController, indexPath: IndexPath) -> Country {
        return currentCountriesArray[indexPath.row]
    }
    
    func getCurrentCountryTitle(vc: SearchTableViewController, indexPath: IndexPath) -> String {
        switch vc.currentCase {
        case .artID:
            return currentArtIdArray[indexPath.row].text
        case .country:
            return currentCountriesArray[indexPath.row].countryName
        }
    }
    
    func getItemsCount(vc: SearchTableViewController) -> Int {
        switch vc.currentCase {
        case .artID:
            return currentArtIdArray.count
        case .country:
            return currentCountriesArray.count
        }
    }
    
}

extension SearchTableController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentCountriesArray = countriesArray
            currentArtIdArray = artIdArray
            viewController?.tableView.reloadData()
            return
        }
        
        switch viewController!.currentCase {
        case .country:
            currentCountriesArray = countriesArray.filter({ country -> Bool in
                guard let text = searchBar.text else { return false }
                return country.countryName.contains(text)
            })
        case .artID:
            currentArtIdArray = artIdArray.filter({ artID -> Bool in
                guard let text = searchBar.text else { return false }
                return artID.text.contains(text)
            })
        }
        viewController?.tableView.reloadData()
    }
}

extension SearchTableController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCountry = currentCountriesArray[indexPath.row]
        viewController?.selectedCountry = currentCountry
    }
}
