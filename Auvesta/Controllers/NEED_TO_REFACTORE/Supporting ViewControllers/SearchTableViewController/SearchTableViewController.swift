import UIKit

protocol SearchTableViewControllerDelegate: AnyObject {
    func didTapCancelButton(vc: SearchTableViewController)
}

protocol SearchTableViewControllerDataSource: AnyObject {
    func getItemsCount(vc: SearchTableViewController) -> Int
    func getCurrentCountryTitle(vc: SearchTableViewController, indexPath: IndexPath) -> String
    func getCurrentCountry(vc: SearchTableViewController, indexPath: IndexPath) -> Country
    func getCurrentArtID(vc:SearchTableViewController, indexPath: IndexPath) -> CompanyArtID
}

class SearchTableViewController: UIViewController {
    enum SearchCase: CaseIterable {
        case country
        case artID
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var delegate: SearchTableViewControllerDelegate?
    var dataSource: SearchTableViewControllerDataSource?
    private var controller: SearchTableController?
    var onViewDidLoad: (()->())? = nil
    var currentCase: SearchCase = .country //default
    
    var selectedCountry: Country?
    var selectedArtID: CompanyArtID?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalPresentationStyle = .fullScreen
        setController(viewController: self)
        tableView.delegate = controller
        searchBar.delegate = controller
        onViewDidLoad?()
    }
    
    private func setController(viewController: SearchTableViewController) {
        controller = SearchTableController(viewController: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        delegate?.didTapCancelButton(vc: self)
    }
}

extension SearchTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.getItemsCount(vc: self) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        switch currentCase {
        default:
            cell.textLabel?.text = dataSource?.getCurrentCountryTitle(vc: self, indexPath: indexPath)
            return cell
        }
    }
}
