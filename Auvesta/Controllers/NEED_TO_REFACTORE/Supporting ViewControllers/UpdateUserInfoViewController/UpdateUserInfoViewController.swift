import UIKit

protocol UpdateUserInfoViewControllerDelegate: AnyObject {
    func didTapBackButton(vc: UpdateUserInfoViewController, parentViewController: UIViewController)
    func didTapApplyButton(vc: UpdateUserInfoViewController, parentViewController: UIViewController?, requestType: Any)
}

class UpdateUserInfoViewController: UIViewController {
    enum Configuration {
        case defaultConfiguration
        case forgotPasswordGetOneTimeCode
        case forgotPasswordGetToken
        case forgotPasswordChange
        case changePsn
        case changePassword
    }
    
    enum PasswordValidationResult {
        case emptyFields
        case incorrectPasswordConfirmation
        case incorrectPassword
        case valid
        
        var errorMessage: String {
            switch self {
            case .emptyFields: return "emptyFieldsMessage".localized()
            case .incorrectPasswordConfirmation: return "wrongPasswordConfirmMessage".localized()
            case .incorrectPassword: return "wrongPasswordMessage".localized()
            case .valid: return ""
            }
        }
    }
    
    @IBOutlet weak var applyButton: ButtonWithActivityIndicator!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var currentRequestTitleLabel: UILabel!
    @IBOutlet weak var currentRequestDescriptionLabel: UILabel!
    
    @IBOutlet weak var firstFieldBackgroundView: UIView!
    @IBOutlet weak var secondFieldBackgroundView: UIView!
    @IBOutlet weak var thirdFieldBackgroundField: UIView!
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate: UpdateUserInfoViewControllerDelegate?
    var viewController: UIViewController?
    var onViewDidLoad: (()->())? = nil
    var configuration = Configuration.defaultConfiguration
    var requestType: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        onViewDidLoad?()
        setupUI()
        setupConfiguration(configuration)
    }
    
    private func setupConfiguration(_ configuration: Configuration) {
        switch configuration {
        case .defaultConfiguration:
            print(configuration)
        case .forgotPasswordGetOneTimeCode:
            currentRequestTitleLabel.text = "forgotPasswordFlowTitle".localized()
            currentRequestDescriptionLabel.text = "forgotPasswordFlowDescriptionStep1".localized()
            currentRequestDescriptionLabel.isHidden = false
            firstTextField.placeholder = "emailPlaceholder".localized()
            secondFieldBackgroundView.isHidden = true
            thirdFieldBackgroundField.isHidden = true
        case .forgotPasswordGetToken:
            currentRequestTitleLabel.text = "forgotPasswordFlowTitle".localized()
            currentRequestDescriptionLabel.text = "forgotPasswordFlowDescriptionStep2".localized()
            currentRequestDescriptionLabel.isHidden = false
            firstTextField.placeholder = "oneTimePasswordPlaceholder".localized()
            secondFieldBackgroundView.isHidden = true
            thirdFieldBackgroundField.isHidden = true
        case .forgotPasswordChange:
            currentRequestTitleLabel.text = "forgotPasswordFlowTitle".localized()
            currentRequestDescriptionLabel.text = "forgotPasswordFlowDescriptionStep3".localized()
            currentRequestDescriptionLabel.isHidden = false
            firstTextField.placeholder = "newPasswordPlaceholder".localized()
            secondTextField.placeholder = "passwordConfirmationPlaceholder".localized()
            thirdFieldBackgroundField.isHidden = true
            secureTextEntry()
        case .changePsn:
            currentRequestTitleLabel.text = "changePsnTitle".localized()
            secondFieldBackgroundView.isHidden = true
            thirdFieldBackgroundField.isHidden = true
            firstTextField.placeholder = "changePsnPlaceholder".localized()
        case .changePassword:
            currentRequestTitleLabel.text = "changePasswordTitle".localized()
            firstTextField.placeholder = "oldPasswordPlaceholder".localized()
            secondTextField.placeholder = "newPasswordPlaceholder".localized()
            thirdTextField.placeholder = "newPasswordConfirmPlaceholder".localized()
            secureTextEntry()
        }
    }
    
    private func setupUI() {
        modalTransitionStyle = .crossDissolve
        
        setupView(view: firstFieldBackgroundView)
        setupView(view: secondFieldBackgroundView)
        setupView(view: thirdFieldBackgroundField)
        
        applyButton.setTitle("applyButton".localized(), for: .normal)
        applyButton.layer.cornerRadius = 5
    }
    
    private func setupView(view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 0.2
        view.layer.borderColor = #colorLiteral(red: 0.745223105, green: 0.754733026, blue: 0.7544236779, alpha: 1)
    }
    
    private func secureTextEntry() {
        firstTextField.isSecureTextEntry = true
        secondTextField.isSecureTextEntry = true
        thirdTextField.isSecureTextEntry = true
    }
    
    func validateNewPassword() -> PasswordValidationResult {
        guard
            !firstTextField.text!.isEmpty,
            !secondTextField.text!.isEmpty else { return .emptyFields }
        guard firstTextField.text!.isValidPassword(name: "", surname: "", date: "") else { return .incorrectPassword }
        guard firstTextField.text == secondTextField.text else { return .incorrectPasswordConfirmation }
        return .valid
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        view.endEditing(true)
    }
    
    @IBAction func didTapApplyButton(_ sender: UIButton) {
        delegate?.didTapApplyButton(vc: self, parentViewController: viewController, requestType: requestType!)
    }
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self, parentViewController: viewController!)
    }
}

extension UpdateUserInfoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
