import UIKit
import MessageUI

protocol ReferFriendViewControllerDelegate: AnyObject {
    func didTapBackButton(vc: ReferFriendViewController)
    func didTapCopySponsorIdButton(vc: ReferFriendViewController)
    func didTapShareButton(vc: ReferFriendViewController, client: ReferFriendViewController.ShareClient)
}

protocol ReferFriendViewControllerDataSource: AnyObject {
    func downloadUserInfo(vc: ReferFriendViewController)
}

class ReferFriendViewController: UIViewController {
    enum ShareClient: Int {
        case whatsApp = 0
        case telegram = 1
        case mail = 2
    }
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet weak var notificationButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var currentPlanStringLabel: UILabel!
    @IBOutlet private weak var commissionStringLabel: UILabel!
    @IBOutlet private weak var commissionPeriodStringLabel: UILabel!
    @IBOutlet private weak var totalPartnersStringLabel: UILabel!
    @IBOutlet private weak var activePartnersStringLabel: UILabel!
    @IBOutlet private weak var sponsorIdStringLabel: UILabel!
    @IBOutlet private weak var shareStringLabel: UILabel!
    
    @IBOutlet private weak var userInfoBackgroundView: UIView!
    @IBOutlet private weak var userIconBackgroundView: UIView!
    @IBOutlet private weak var sponsorIdBackgroundView: UIView!
    
    @IBOutlet private weak var userImageView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var userSubscriptionPlanLabel: UILabel!
    @IBOutlet private weak var userPartnersCountLabel: UILabel!
    
    @IBOutlet private weak var userSubmissionCountLabel: UILabel!
    @IBOutlet private weak var userExternalIdLabel: UILabel!
    @IBOutlet private weak var userContractCountLabel: UILabel!
    @IBOutlet private weak var userPendingCountLabel: UILabel!
    
    @IBOutlet private weak var userQrImageView: UIImageView!
    @IBOutlet var userSponsorIdLabel: UILabel!
    
    @IBOutlet weak var copyButton: UIButton!
    
    private var controller: ReferFriendController?
    private var delegate: ReferFriendViewControllerDelegate?
    private var dataSource: ReferFriendViewControllerDataSource?
    
    var userInfo: AuvestaUser?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setController(viewController: self)
        updateUI()
        localize()
        dataSource?.downloadUserInfo(vc: self)
    }
    
    // MARK: - Methods
    
    private func setController(viewController: ReferFriendViewController) {
        controller = ReferFriendController(vc: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    private func localize() {
        headerTitleLabel.text = "referVcHeader".localized()
        currentPlanStringLabel.text = "networkCurrentPlan".localized()
        commissionStringLabel.text = "networkCommission".localized()
        commissionPeriodStringLabel.text = "networkCommissionPeriod".localized()
        totalPartnersStringLabel.text = "networkTotalPartners".localized()
        activePartnersStringLabel.text = "networkActivePartners".localized()
        sponsorIdStringLabel.text = "referSponsorId".localized()
        shareStringLabel.text = "referShare".localized()
    }
    
    func updateData(user: AuvestaUser) {
        userSponsorIdLabel.text = user.sponsorID
        userImageView.image = user.userPhoto ?? UIImage()
        userNameLabel.text = user.firstName + " " + user.lastName
        userSubscriptionPlanLabel.text = user.subscriptionType
        userSubmissionCountLabel.text = String(format: "%.2f", user.commission)
        userContractCountLabel.text = "\(user.totalContractsCount)"
        userPendingCountLabel.text = "\(user.pendingContractsCount)"
        userPartnersCountLabel.text = "\(user.partnersCount)" + "networkPartnersString".localized()
        userQrImageView.image = generateQRCode(from: user.sponsorID)
        userExternalIdLabel.text = user.externalPartnerId
        userExternalIdLabel.isHidden = false
    }
    
    private func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    private func updateUI() {
        userInfoBackgroundView.layer.cornerRadius = 10
        userInfoBackgroundView.layer.shadowColor = UIColor.black.cgColor
        userInfoBackgroundView.layer.shadowRadius = 10
        userInfoBackgroundView.layer.shadowOpacity = 0.3
        
        userIconBackgroundView.layer.cornerRadius = userIconBackgroundView.bounds.size.height / 2
        userIconBackgroundView.layer.borderColor = #colorLiteral(red: 0.8270336986, green: 0.6961463094, blue: 0.3867461383, alpha: 1)
        userIconBackgroundView.layer.borderWidth = 1
        
        userImageView.layer.cornerRadius = userImageView.bounds.size.height / 2
        userImageView.layer.borderColor = #colorLiteral(red: 0.989490211, green: 0.9902383685, blue: 0.9896060824, alpha: 1)
        userImageView.layer.borderWidth = 4
        
        sponsorIdBackgroundView.layer.cornerRadius = 10
        sponsorIdBackgroundView.layer.borderColor = #colorLiteral(red: 0.8832614422, green: 0.8839334846, blue: 0.8833655715, alpha: 1)
        sponsorIdBackgroundView.layer.borderWidth = 1
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self)
    }
    
    @IBAction func didTapCopySponsorIdButton(_ sender: UIButton) {
        delegate?.didTapCopySponsorIdButton(vc: self)
    }
    
    @IBAction func didTapWhatsAppButton(_ sender: UIButton) {
        guard let client = ShareClient(rawValue: sender.tag) else { return }
        delegate?.didTapShareButton(vc: self, client: client)
    }
}

extension ReferFriendViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let error = error {
            print(#function, error.localizedDescription)
        }
        dismiss(animated: true, completion: nil)
    }
}
