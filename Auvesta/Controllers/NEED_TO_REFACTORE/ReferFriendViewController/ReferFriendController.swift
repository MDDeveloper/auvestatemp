import UIKit
import MessageUI

class ReferFriendController {
    private var viewController: ReferFriendViewController?
    private let impactFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
    private var savedSponsorID: String?
    
    private func openAppWithUrlScheme(referralText: String, client: ReferFriendViewController.ShareClient) {
        var appScheme: String
        guard let formattedText = referralText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }

        switch client {
        case .telegram:
            appScheme = "tg://msg?text=\(formattedText)"
        case .whatsApp:
            appScheme = "WhatsApp://send?text=\(formattedText)"
        case .mail:
            return
        }
        
        guard let appSchemeURL = URL(string: appScheme) else { return }
        if UIApplication.shared.canOpenURL(appSchemeURL as URL) {
            UIApplication.shared.open(appSchemeURL, options: [:], completionHandler: nil)
        } else {
            viewController?.addAlert(message: "referAppNotFound".localized())
        }
    }
    
    private func showMailComposerViewController(referralText: String) {
        guard MFMailComposeViewController.canSendMail() else {
            viewController?.addAlert(message: "referAppNotFound".localized())
            return
        }
        
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = viewController
        mailComposer.setSubject("AllStars")
        mailComposer.setMessageBody(referralText, isHTML: false)
        viewController?.present(mailComposer, animated: true, completion: nil)
    }
    
    init(vc: ReferFriendViewController) {
        self.viewController = vc
    }
}

extension ReferFriendController: ReferFriendViewControllerDataSource {
    func downloadUserInfo(vc: ReferFriendViewController) {
        vc.notificationButton.startActivityIndicator()
        MainNetworkManager.getUserInfo { (status, error) in
            switch status {
            case 200:
                MainNetworkManager.getNetworkData { (status, error) in
                    vc.notificationButton.stopActivityIndicator()
                    switch status {
                    case 200:
                        DispatchQueue.main.async {
                            vc.updateData(user: AuvestaUser.shared)
                        }
                    case 401:
                        vc.navigationController?.popToRootViewController(animated: true)
                    default:
                        vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
                    }
                }
            case 401:
                vc.navigationController?.popToRootViewController(animated: true)
            default:
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
}

extension ReferFriendController: ReferFriendViewControllerDelegate {
    func didTapShareButton(vc: ReferFriendViewController, client: ReferFriendViewController.ShareClient) {
        MainNetworkManager.getReferralLinkText { (status, text, error) in
            guard status == 200 else {
                vc.addAlert(message: error)
                return
            }
            
            switch client {
            case .whatsApp, .telegram:
                self.openAppWithUrlScheme(referralText: text, client: client)
            case .mail:
                self.showMailComposerViewController(referralText: text)
            }
        }
    }
    
    func didTapBackButton(vc: ReferFriendViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
    func didTapCopySponsorIdButton(vc: ReferFriendViewController) {
        UIPasteboard.general.string = vc.userSponsorIdLabel.text
        impactFeedbackGenerator.prepare()
        impactFeedbackGenerator.impactOccurred()
        
        savedSponsorID = vc.userSponsorIdLabel.text
        vc.userSponsorIdLabel.text = "Copied!"
        vc.copyButton.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            vc.userSponsorIdLabel.text = self.savedSponsorID
            vc.copyButton.isEnabled = true
        }
    }
}
