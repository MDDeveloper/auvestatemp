import UIKit

class DashboardController {
    private let viewController: DashboardViewController
    
    fileprivate var portfoliosData = [AuvestaPortfolio]()
    
    func getPortfolioItems(currentPortfolioType: AuvestaPortfolio.PortfolioType) -> [PortfolioItem] {
        guard !portfoliosData.isEmpty else { return [PortfolioItem]() }
        switch currentPortfolioType {
        case .metal:
            guard let currentItems = portfoliosData[0].portfolioItems else { return [PortfolioItem]() }
            return currentItems
        case .coin:
            fatalError()
        case .crypto:
            fatalError()
        }
    }
    
    init(viewController: DashboardViewController) {
        self.viewController = viewController
        viewController.dataSource = self
        viewController.delegate = self
    }
}

extension DashboardController: DashboardViewControllerDataSource {
    func getDashboardDataAndReload(vc: DashboardViewController) {
        vc.notificationButton.startActivityIndicator()
        MainNetworkManager.getDashboard { (status, portfolios, error) in
            vc.notificationButton.stopActivityIndicator()
            switch status {
            case 200:
                guard let portfolios = portfolios else { return }
                DispatchQueue.main.async {
                    self.portfoliosData = portfolios
                    if self.isTotalPortfoliosValueEmpty(vc: vc) {
                        vc.performBuyOfferView()
                    } else {
                        vc.hideBuyOfferView()
                    }
                    vc.updateLineChartViewData(data: self.portfoliosData[0].chartOne,
                                               xAxis: self.portfoliosData[0].chartDataX)
                    vc.portfolioCollectionView.reloadData()
                    vc.portfolioItemsCollectionView.reloadData()
                }
            case 401:
                vc.navigationController?.popToRootViewController(animated: true)
            default:
                print("------>", error)
            }
        }
    }

    func getPortfolioData(vc: DashboardViewController, indexPath: IndexPath) -> AuvestaPortfolio {
        let currentPortfolio = portfoliosData[indexPath.row]
        return currentPortfolio
    }
    
    func getPortfolioItemData(vc: DashboardViewController, indexPath: IndexPath, type: AuvestaPortfolio.PortfolioType) -> PortfolioItem {
        let portfolioItemsForCurrentType = getPortfolioItems(currentPortfolioType: type)
        let currentItem = portfolioItemsForCurrentType[indexPath.row]
        return currentItem
    }
    
    func getPortfolioCount(vc: DashboardViewController) -> Int {
        return portfoliosData.count
    }
    
    func getPortfolioItemsCount(vc: DashboardViewController, type: AuvestaPortfolio.PortfolioType) -> Int {
        let portfolioItemsForCurrentType = getPortfolioItems(currentPortfolioType: type)
        return portfolioItemsForCurrentType.count
    }
    
    func isTotalPortfoliosValueEmpty(vc: DashboardViewController) -> Bool {
        guard !portfoliosData.isEmpty else { return true }
        for portfolio in portfoliosData {
            guard var stringPrice = portfolio.price else { return true }
            AuvestaCurrency.getPriceValue(&stringPrice)
            guard let price = Double(stringPrice) else { return true }
            guard price != 0 else { return true }
        }
        return false
    }
}

extension DashboardController: DashboardViewControllerDelegate {
    func buyButtonPressed(vc: DashboardViewController) {
        guard let items = portfoliosData.first?.portfolioItems else { return }
        guard let firstItem = items.first else { return }
        guard let itemLink = firstItem.linkUrl else { return }
        guard let url = URL(string: itemLink) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
