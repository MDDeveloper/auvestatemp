import UIKit
import Charts

protocol DashboardViewControllerDelegate: AnyObject {
    func buyButtonPressed(vc: DashboardViewController)
}

protocol DashboardViewControllerDataSource: AnyObject {
    func getPortfolioData(vc: DashboardViewController, indexPath: IndexPath) -> AuvestaPortfolio
    func getPortfolioCount(vc: DashboardViewController) -> Int
    func getPortfolioItemsCount(vc: DashboardViewController, type: AuvestaPortfolio.PortfolioType) -> Int
    func getPortfolioItemData(vc: DashboardViewController, indexPath: IndexPath, type: AuvestaPortfolio.PortfolioType) -> PortfolioItem
    func getDashboardDataAndReload(vc: DashboardViewController)
    func isTotalPortfoliosValueEmpty(vc: DashboardViewController) -> Bool
}

class DashboardViewController: UIViewController {
    enum PortfolioType: String {
        case crypto = "CRYPTO PORTFOLIO"
        case metal = "METAL PORTFOLIO"
        case coin = "COIN PORTFOLIO"
    }
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet private weak var portfolioValueChartHeader: UILabel!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet weak var chartValueStackView: UIStackView!
    @IBOutlet weak var selectedValueTextLabel: UILabel!
    @IBOutlet weak var selectedValueLabel: UILabel!
    @IBOutlet weak var notificationButton: ButtonWithActivityIndicator!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var portfolioCollectionView: UICollectionView!
    @IBOutlet weak var portfolioItemsCollectionView: UICollectionView!
    @IBOutlet weak var buyOfferView: UIVisualEffectView!
    @IBOutlet weak var buyOfferLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var depotTabBarItem: UITabBarItem!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    weak var delegate: DashboardViewControllerDelegate?
    weak var dataSource: DashboardViewControllerDataSource?
    
    var portfolioType: PortfolioType = .metal
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calculatePortfolioCellFrame()
        calculatePortfolioItemCellFrame(currentItemType: portfolioType)
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataSource?.getDashboardDataAndReload(vc: self)
    }
    
    // MARK: - Methods
    
    func performBuyOfferView() {
        buyOfferView.isHidden = false
    }

    func hideBuyOfferView() {
        buyOfferView.isHidden = true
    }
    
    private func setupUI() {
        updateLineChartViewUI(chart: lineChartView)
        portfolioValueChartHeader.text = "dashboardChartHeader".localized()
        selectedValueTextLabel.text = "dashboardSelectedChartValue".localized()
        headerTitleLabel.text = "dashboardVcHeader".localized()
        buyOfferLabel.text = "buyOfferText".localized()
        buyNowButton.setTitle("buyNowButton".localized(), for: .normal)
    }
    
    func updateLineChartViewData(data: [Double], xAxis: [String]) {
        let mainChartDataEntry = prepareChartDataEntry(chartData: data)
        let mainChartLine = LineChartDataSet(entries: mainChartDataEntry, label: nil)
        
        mainChartLine.colors = [#colorLiteral(red: 0.944860518, green: 0.6891974807, blue: 0.2085942924, alpha: 1)]
        mainChartLine.mode = .horizontalBezier
        mainChartLine.lineWidth = 3
        
        mainChartLine.drawValuesEnabled = false
        mainChartLine.drawCirclesEnabled = true
        mainChartLine.circleRadius = 7
        mainChartLine.circleHoleRadius = 4
        mainChartLine.circleColors = [#colorLiteral(red: 0.9001699686, green: 0.6781448722, blue: 0.1709142923, alpha: 1)]
        mainChartLine.circleHoleColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let gradientForMainChartLine = getGradientFilling()
        mainChartLine.fill = Fill.fillWithLinearGradient(gradientForMainChartLine, angle: 90.0)
        mainChartLine.drawFilledEnabled = true
        
        let chartData = LineChartData(dataSets: [mainChartLine])
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxis)
        lineChartView.data = chartData
    }
    
    private func prepareChartDataEntry(chartData: [Double]) -> [ChartDataEntry] {
        var dataEntries = [ChartDataEntry]()
        
        for i in 0..<chartData.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: chartData[i])
            dataEntries.append(dataEntry)
        }
        return dataEntries
    }
    
    private func updateLineChartViewUI(chart: LineChartView) {
        chart.delegate = self
        chart.backgroundColor = UIColor.clear
        
        chart.xAxis.labelPosition = .bottom
        chart.legend.enabled = false
        chart.extraLeftOffset = 0
        chart.extraRightOffset = 0
        
        chart.xAxis.spaceMax = 0.1
        chart.xAxis.spaceMin = 0.1
        chart.xAxis.yOffset = 10
        chart.xAxis.gridColor = .clear
        chart.xAxis.axisLineColor = .clear
        chart.xAxis.labelTextColor = #colorLiteral(red: 0.5107976794, green: 0.5150662065, blue: 0.5186151266, alpha: 1)
        chart.xAxis.granularity = 1
        
        chart.leftAxis.enabled = false
        chart.rightAxis.enabled = false
        
        chart.setScaleEnabled(false)
        chart.highlightPerTapEnabled = true
        chart.highlightPerDragEnabled = false
    }
    
    private func getGradientFilling() -> CGGradient {
        let colorTop = #colorLiteral(red: 0.937254902, green: 0.7607843137, blue: 0.3058823529, alpha: 1).cgColor
        let colorBottom = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        let gradientColors = [colorTop, colorBottom] as CFArray
        let colorLocations: [CGFloat] = [0.7, 0.0]
        return CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)!
    }
    
    private func animateCollectionViewAndReloadData(collectionView: UICollectionView) {
        UIView.transition(with: collectionView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            collectionView.reloadData()
        }, completion: nil)
    }
    
    private func reloadPortfolioItemCollectionViewAfterScroll() {
        
        var centerPoint = view.convert(view.center, to: portfolioCollectionView)
        centerPoint.y = portfolioCollectionView.frame.height / 2
        
        if let centerIndexPath = portfolioCollectionView.indexPathForItem(at: centerPoint) {
            let cell = portfolioCollectionView.cellForItem(at: centerIndexPath) as! DashboardPortfolioCollectionViewCell
            reloadPortfolioItemCollectionView(cell: cell)
        }
    }
    
    private func reloadPortfolioItemCollectionView(cell: DashboardPortfolioCollectionViewCell) {
        guard cell.portfolioNameLabel.text != portfolioType.rawValue else { return }
        
        let cellNameCategory = cell.portfolioNameLabel.text
        
        if cellNameCategory == "COIN PORTFOLIO" {
            portfolioType = .coin
        } else if cellNameCategory == "CRYPTO PORTFOLIO" {
            portfolioType = .crypto
        } else if cellNameCategory == "METAL PORTFOLIO" {
            portfolioType = .metal
        }
        
        calculatePortfolioItemCellFrame(currentItemType: portfolioType)
        animateCollectionViewAndReloadData(collectionView: portfolioItemsCollectionView)
    }
    
    private func centerCellHorizontallyWhenScrolling(scrollView: UIScrollView, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == portfolioCollectionView {
            let layout = self.portfolioCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
            let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
            
            var offset = targetContentOffset.pointee
            let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
            let roundedIndex = round(index)
            
            offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
            targetContentOffset.pointee = offset
        } else if scrollView == portfolioItemsCollectionView {
            
            switch portfolioType {
            case .coin, .crypto:
                let layout = self.portfolioItemsCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
                let cellHeightIncludingSpacing = layout.itemSize.height + layout.minimumInteritemSpacing
                
                var offset = targetContentOffset.pointee
                let index = (offset.y + scrollView.contentInset.top) / cellHeightIncludingSpacing
                let roundedIndex = round(index)
                
                offset = CGPoint(x: -scrollView.contentInset.left, y: roundedIndex * cellHeightIncludingSpacing - scrollView.contentInset.top)
                targetContentOffset.pointee = offset
            case .metal:
                return
            }
        }
    }
    
    private func calculatePortfolioCellFrame() {
        let portfolioItemCollectionViewSize = portfolioItemsCollectionView.bounds.size
        
        let cellWidth = floor(portfolioItemCollectionViewSize.width * 0.72 )
        let cellHeight = floor(portfolioItemCollectionViewSize.height * 0.8 )
        
        let insetX = (view.bounds.width - cellWidth) / 2
        let insetY = CGFloat(10)
        
        let layout = portfolioCollectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        portfolioCollectionView.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
    }
    
    private func calculatePortfolioItemCellFrame(currentItemType: PortfolioType) {
        let portfolioItemCollectionViewSize = portfolioItemsCollectionView.bounds.size
        
        var cellWidth: CGFloat?
        var cellHeight: CGFloat?
        
        var insetX: CGFloat?
        var insetY: CGFloat?
        
        let layout = portfolioItemsCollectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        
        if currentItemType == .coin {
            layout.scrollDirection = .vertical
            cellWidth = floor(portfolioItemCollectionViewSize.width * 0.95 )
            cellHeight = floor(portfolioItemCollectionViewSize.height * 0.95 )
            
            insetX = (view.bounds.width - cellWidth!) / 2
            insetY = (portfolioItemCollectionViewSize.height - cellHeight!) / 4
            
        } else if currentItemType == .crypto {
            layout.scrollDirection = .vertical
            cellWidth = floor(portfolioItemCollectionViewSize.width * 0.95 )
            cellHeight = floor(portfolioItemCollectionViewSize.height * 0.45 )
            
            insetX = (view.bounds.width - cellWidth!) / 2
            insetY = 0
            
        } else if currentItemType == .metal {
            layout.scrollDirection = .horizontal
            cellWidth = floor(portfolioItemCollectionViewSize.width * 0.35 )
            cellHeight = floor(portfolioItemCollectionViewSize.height * 0.9 )
            
            insetX = 5
            insetY = (portfolioItemCollectionViewSize.height - cellHeight!) / 2
        }
        
        guard let width = cellWidth, let height = cellHeight, let x = insetX, let y = insetY else { return }
        
        layout.itemSize = CGSize(width: width, height: height)
        portfolioItemsCollectionView.contentInset = UIEdgeInsets(top: y, left: x, bottom: y, right: x)
    }
    
    //MARK: - IBActions
    
    @IBAction func buyButtonPressed(_ sender: UIButton) {
        delegate?.buyButtonPressed(vc: self)
    }
    
}

extension DashboardViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == portfolioCollectionView {
            guard let portfolioCount = dataSource?.getPortfolioCount(vc: self) else {
                assertionFailure()
                return 0 }
            
            return portfolioCount
        } else if collectionView == portfolioItemsCollectionView {
            
            switch portfolioType {
            case .coin:
                guard let portfolioItemsCount = dataSource?.getPortfolioItemsCount(vc: self, type: .coin) else {
                    assertionFailure()
                    return 0 }
                return portfolioItemsCount
            case .crypto:
                guard let portfolioItemsCount = dataSource?.getPortfolioItemsCount(vc: self, type: .crypto) else {
                    assertionFailure()
                    return 0 }
                return portfolioItemsCount
            case .metal:
                guard let portfolioItemsCount = dataSource?.getPortfolioItemsCount(vc: self, type: .metal) else {
                    assertionFailure()
                    return 0 }
                return portfolioItemsCount
            }
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == portfolioCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "portfolioCollectionCell", for: indexPath) as! DashboardPortfolioCollectionViewCell
            
            guard let portfolioData = dataSource?.getPortfolioData(vc: self, indexPath: indexPath) else {
                assertionFailure()
                return cell }
            
            cell.portfolio = portfolioData
            return cell
        } else if collectionView == portfolioItemsCollectionView {
            
            switch portfolioType {
            case .coin:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coinDashboardCell", for: indexPath) as! DashboardCoinCollectionViewCell
                guard let portfolioItem = dataSource?.getPortfolioItemData(vc: self, indexPath: indexPath, type: .coin) else {
                    assertionFailure()
                    return cell }
                
                cell.portfolioItem = portfolioItem
                return cell
                
            case .crypto:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cryptoDashboardCell", for: indexPath) as! DashboardCryptoCollectionViewCell
                guard let portfolioItem = dataSource?.getPortfolioItemData(vc: self, indexPath: indexPath, type: .crypto) else {
                    assertionFailure()
                    return cell }
                
                cell.portfolioItem = portfolioItem
                return cell
                
            case .metal:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "metallDashboardCell", for: indexPath) as! DashboardMetalCollectionViewCell
                guard let portfolioItem = dataSource?.getPortfolioItemData(vc: self, indexPath: indexPath, type: .metal) else {
                    assertionFailure()
                    return cell }
                
                cell.portfolioItem = portfolioItem
                return cell
            }
        } else {
            return UICollectionViewCell()
        }
    }
}

extension DashboardViewController: UIScrollViewDelegate, UICollectionViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        self.centerCellHorizontallyWhenScrolling(scrollView: scrollView, targetContentOffset: targetContentOffset)
        guard scrollView == portfolioCollectionView else { return }
        self.reloadPortfolioItemCollectionViewAfterScroll()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == portfolioCollectionView else { return }
        self.reloadPortfolioItemCollectionViewAfterScroll()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard collectionView == portfolioItemsCollectionView else { return }
        switch portfolioType {
        case .metal:
            let selectedCell = collectionView.cellForItem(at: indexPath) as? DashboardMetalCollectionViewCell
            guard let linkUrl = selectedCell?.portfolioItem!.linkUrl else { return }
            guard let url = URL(string: linkUrl) else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        default:
            return
        }
    }
}

extension DashboardViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        chartValueStackView.isHidden = false
        selectedValueLabel.text = "\(highlight.y)€"
    }
}
