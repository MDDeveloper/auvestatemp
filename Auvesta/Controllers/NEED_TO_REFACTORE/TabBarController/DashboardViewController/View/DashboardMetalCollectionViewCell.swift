import UIKit
import SDWebImage

class DashboardMetalCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var gradientBackground: MetalBackgroundView!
    @IBOutlet private weak var metalImageView: UIImageView!
    
    @IBOutlet weak var amountStringLabel: UILabel!
    @IBOutlet private weak var metalNameLabel: UILabel!
    @IBOutlet private weak var metalValueLabel: UILabel!
    @IBOutlet private weak var metalAmountLabel: UILabel!
    @IBOutlet private weak var metalPendingLabel: UILabel!
    
    var portfolioItem: PortfolioItem? {
        didSet {
            self.updateData()
            self.metalImageView.image = nil
        }
    }
    
    private func updateData() {
        amountStringLabel.text = "metalCellAmount".localized()
        if let portfolioItem = portfolioItem {
            metalNameLabel.text = portfolioItem.name
            metalAmountLabel.text = "\(portfolioItem.amount!)"
            metalValueLabel.text = "\(portfolioItem.value!)"
            metalImageView.image = portfolioItem.image
            gradientBackground.startColor = (portfolioItem.colors?.first)!
            gradientBackground.endColor = (portfolioItem.colors?.last)!
            guard let url = URL(string: portfolioItem.imageUrl!) else { return }
            metalImageView.sd_setImage(with: url, completed: nil)
        } else {
            metalNameLabel.text = nil
            metalAmountLabel.text = nil
            metalValueLabel.text = nil
            metalImageView.image = nil
            metalPendingLabel.text = nil
        }
    }
}

class MetalBackgroundView: UIView {
    private var gradientLayer = CAGradientLayer()
    
    var startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            self.updateGradientLayer(gradientLayer: gradientLayer, startColor: self.startColor, endColor: self.endColor)
        }
    }
    var endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            self.updateGradientLayer(gradientLayer: gradientLayer, startColor: self.startColor, endColor: self.endColor)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradientLayer.frame = self.bounds
        self.layer.cornerRadius = 10
        gradientLayer.cornerRadius = 10
        
        updateLayerWithShadow(layer: self.layer)
    }
    
    private func updateLayerWithShadow(layer: CALayer) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 2
    }
    
    private func updateGradientLayer(gradientLayer: CAGradientLayer, startColor: UIColor, endColor: UIColor) {
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
}
