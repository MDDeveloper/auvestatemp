import UIKit

class DashboardCryptoCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var cryptoImageView: UIImageView!
    @IBOutlet private weak var gradientBackground: CryptoBackgroundView!
    @IBOutlet private weak var cryptoNameLabel: UILabel!
    @IBOutlet private weak var cryptoValueLabel: UILabel!
    @IBOutlet private weak var cryptoAmountLabel: UILabel!
    
    var portfolioItem: PortfolioItem? {
        didSet {
            self.updateData()
        }
    }
    
    private func updateData() {
        if let portolioItem = portfolioItem {
            cryptoNameLabel.text = portolioItem.name
            cryptoAmountLabel.text = portolioItem.amount
            cryptoValueLabel.text = "\(portolioItem.value!)"
            cryptoImageView.image = portolioItem.image
            
            gradientBackground.startColor = (portolioItem.colors?.first)!
            gradientBackground.endColor = (portolioItem.colors?.last)!
        } else {
            cryptoNameLabel.text = nil
            cryptoAmountLabel.text = nil
            cryptoValueLabel.text = nil
            cryptoImageView.image = nil
        }
    }
}

class CryptoBackgroundView: UIView {
    private let gradientLayer = CAGradientLayer()
    
    var startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            self.updateGradientLayer(gradientLayer: gradientLayer, startColor: self.startColor, endColor: self.endColor)
        }
    }
    var endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            self.updateGradientLayer(gradientLayer: gradientLayer, startColor: self.startColor, endColor: self.endColor)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradientLayer.frame = self.bounds
        self.layer.cornerRadius = 10
        gradientLayer.cornerRadius = 10
        
        updateLayerWithShadow(layer: self.layer)
    }
    
    private func updateLayerWithShadow(layer: CALayer) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 2
    }
    
    private func updateGradientLayer(gradientLayer: CAGradientLayer, startColor: UIColor, endColor: UIColor) {
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
}
