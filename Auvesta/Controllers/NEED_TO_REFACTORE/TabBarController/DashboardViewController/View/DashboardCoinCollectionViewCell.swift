import UIKit

class DashboardCoinCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var coinImageView: UIImageView!
    @IBOutlet private weak var gradientBackground: CoinBackgroundView!
    
    @IBOutlet private weak var coinNameLabel: UILabel!
    @IBOutlet private weak var coinPiecesLabel: UILabel!
    @IBOutlet private weak var coinAmountValueLabel: UILabel!
    @IBOutlet private weak var coinValueLabel: UILabel!
    
    var portfolioItem: PortfolioItem? {
        didSet {
            self.updateData()
        }
    }
    
    private func updateData() {
        if let portolioItem = portfolioItem {
            coinNameLabel.text = portolioItem.name
            coinPiecesLabel.text = portolioItem.pieces
            coinAmountValueLabel.text = portolioItem.amount
            coinValueLabel.text = "\(portolioItem.value!)"
            coinImageView.image = portolioItem.image
            
            gradientBackground.startColor = (portolioItem.colors?.first)!
            gradientBackground.endColor = (portolioItem.colors?.last)!
        } else {
            coinNameLabel.text = nil
            coinPiecesLabel.text = nil
            coinAmountValueLabel.text = nil
            coinValueLabel.text = nil
            coinImageView.image = nil
        }
    }
}

class CoinBackgroundView: UIView {
    private let gradientLayer = CAGradientLayer()
    
    var startColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            self.updateGradientLayer(gradientLayer: gradientLayer, startColor: self.startColor, endColor: self.endColor)
        }
    }
    var endColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            self.updateGradientLayer(gradientLayer: gradientLayer, startColor: self.startColor, endColor: self.endColor)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradientLayer.frame = self.bounds
        self.layer.cornerRadius = 10
        gradientLayer.cornerRadius = 10
        
        updateLayerWithShadow(layer: self.layer)
        
    }
    
    private func updateLayerWithShadow(layer: CALayer) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 4
    }
    
    private func updateGradientLayer(gradientLayer: CAGradientLayer, startColor: UIColor, endColor: UIColor) {
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
}
