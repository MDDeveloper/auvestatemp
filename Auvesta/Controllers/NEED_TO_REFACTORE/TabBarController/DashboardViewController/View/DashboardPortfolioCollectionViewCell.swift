import UIKit

class DashboardPortfolioCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var portfolioImageView: UIImageView!
    @IBOutlet private weak var portfolioPiecesLabel: UILabel!
    @IBOutlet private weak var portfolioMoneyValueLabel: UILabel!
    
    @IBOutlet weak var portfolioDepotIDLabel: UILabel!
    @IBOutlet weak var portfolioNameLabel: UILabel!
    
    var portfolio: AuvestaPortfolio? {
        didSet {
            self.updateData()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateUI()
    }
    
    private func updateData() {
        if let portfolio = portfolio {
            switch portfolio.portfolioType {
            case .metal:
                portfolioNameLabel.text = portfolio.name
                portfolioPiecesLabel.isHidden = true
                portfolioMoneyValueLabel.text = portfolio.price ?? ""
                portfolioDepotIDLabel.text = portfolio.depotID ?? ""
            default:
                portfolioNameLabel.text = portfolio.name
                portfolioPiecesLabel.text = portfolio.amount
                portfolioMoneyValueLabel.text = portfolio.price ?? ""
            }
        } else {
            portfolioNameLabel.text = nil
            portfolioPiecesLabel.text = nil
            portfolioMoneyValueLabel.text = nil
        }
    }
    
    private func updateUI() {
        portfolioImageView.layer.cornerRadius = 10
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        shadowView.layer.shadowOpacity = 0.3
        shadowView.layer.shadowRadius = 2
        shadowView.layer.cornerRadius = 10
    }
}
