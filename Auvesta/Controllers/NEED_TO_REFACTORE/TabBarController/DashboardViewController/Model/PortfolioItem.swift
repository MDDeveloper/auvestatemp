import UIKit

class PortfolioItem {
    enum ItemType: String {
        case crypto
        case metal
        case coin
    }
    
    var itemType: ItemType?
    var name: String?
    var amount: String?
    var value: String?
    var linkUrl: String?
    var pending: String?
    var pieces: String?
    var image: UIImage?
    var imageUrl: String?
    var colors: [UIColor]?
    
    static func getArray(from jsonArray: Any) -> [PortfolioItem] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [PortfolioItem]() }
        return jsonArray.compactMap{ PortfolioItem(jsonArray: $0) }
    }
    
    init(jsonArray: [String:Any]) {
        let type = jsonArray["portfolioType"] as? String
        let name = jsonArray["name"] as? String
        let imageUrl = jsonArray["imageUrl"] as? String
        let value = jsonArray["value"] as? Double
        let amount = jsonArray["amount"] as? Double
        let pending = jsonArray["pending"] as? Double
        let pieces = jsonArray["pieces"] as? Double
        let valueCurrency = jsonArray["valueCurrency"] as? Int
        let pendingCurrency = jsonArray["pendingCurrency"] as? Int
        let amountCurrency = jsonArray["amountMeasure"] as? Int
        let piecesCurrency = jsonArray["piecesCurrency"] as? Int
        let startColor = jsonArray["startColor"] as? String
        let finishColor = jsonArray["finishColor"] as? String
        let link = jsonArray["ItemLink"] as? String
        
        guard let itemType = type else { return }
        self.itemType = ItemType(rawValue: itemType)!
        self.name = name ?? ""
        self.imageUrl = imageUrl ?? ""
        self.linkUrl = link ?? ""
        self.amount = "\(String(format: "%.4f", amount ?? 0))\(AuvestaCurrency(rawValue: amountCurrency ?? -1)!.stringIcon)"
        self.value = "\(String(format: "%.2f", value ?? 0))\(AuvestaCurrency(rawValue: valueCurrency ?? -1)!.stringIcon)"
        self.pending = "\(String(format: "%.2f", pending ?? 0))\(AuvestaCurrency(rawValue: pendingCurrency ?? -1)!.stringIcon)"
        self.pieces = "\(pieces ?? 0)\(AuvestaCurrency(rawValue: piecesCurrency ?? -1)!.stringIcon)"
        self.colors = [UIColor(hex: "#\(startColor ?? "ffe700")ff")!,
                       UIColor(hex: "#\(finishColor ?? "ffe700")ff")!]
    }
    
    init(itemType: ItemType, name: String, amount: String, value: String?, pending: String?, pieces: String?, image: UIImage?, colors: [UIColor]) {
        self.itemType = itemType
        self.name = name
        self.amount = amount
        self.value = value
        self.pending = pending
        self.pieces = pieces
        self.image = image
        self.colors = colors
        
    }
}
