import UIKit

class AuvestaPortfolio {
    enum PortfolioType: String {
        case crypto
        case metal
        case coin
    }
    
    var portfolioType: PortfolioType?
    var name: String?
    var chartDataX = [String]()
    var chartOne = [Double]()
    var amount: String?
    var price: String?
    var depotID: String? {
        return AuvestaUser.shared.externalDepotId
    }
    var portfolioItems: [PortfolioItem]?
    
    static func getArray(from jsonArray: Any) -> [AuvestaPortfolio] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [AuvestaPortfolio]() }
        return jsonArray.compactMap{ AuvestaPortfolio(jsonArray: $0) }
    }
    
    init(jsonArray: [String:Any]) {
        let type = jsonArray["portfolioType"] as? String
        let name = jsonArray["name"] as? String
        let amount = jsonArray["amount"] as? Double
        let price = jsonArray["price"] as? Double
        let amountCurrency = jsonArray["amountCurrency"] as? Int
        let priceCurrency = jsonArray["priceCurrency"] as? Int
        let portfolioItems = jsonArray["portfolioItems"] as Any
        
        guard let portfolioType = type else { return }
        self.portfolioType = PortfolioType(rawValue: portfolioType)!
        self.name = name ?? ""
        self.amount = "\(String(format: "%.2f", amount ?? 0))\(AuvestaCurrency(rawValue: amountCurrency ?? -1)!.stringIcon)"
        self.price = "\(String(format: "%.2f", price ?? 0))\(AuvestaCurrency(rawValue: priceCurrency ?? -1)!.stringIcon)"
        self.portfolioItems = PortfolioItem.getArray(from: portfolioItems)
        
        guard let chartData = jsonArray["chart"] as? [String:Any] else { return }
        guard let xData = chartData["xData"] as? [String] else { return }
        guard let chartOne = chartData["chartOne"] as? [Double] else { return }
        
        self.chartDataX = xData
        self.chartOne = chartOne
    }
}
