import UIKit
import Stripe

class TabBarViewController: UITabBarController {
    private var dashboardController: DashboardController!
    private var networkController: NetworkController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let fireBaseToken = UserDefaults.standard.string(forKey: "FireBaseToken") {
            NetworkService.request(.sendFirebaseRegisterToken, body: ["id": fireBaseToken]) { (_, _, _) in }
        }
        
        preparePoolsVC()
        prepareViewControllers()
    }
    
    private func prepareViewControllers() {
        self.viewControllers?.forEach({ (vc) in
            
            if let dashboardViewController = vc as? DashboardViewController {
                dashboardController = DashboardController(viewController: dashboardViewController)
                dashboardViewController.depotTabBarItem.title = "tabBarDepotName".localized()
            
            } 
        })
    }
    
    func preparePoolsVC() {
        let isPartner = AuvestaUser.shared.isPartner
        let poolsIndex = 0
        
        if isPartner {
            guard let _ = viewControllers![poolsIndex] as? PoolsViewController else {
                let poolsVC = storyboard?.instantiateViewController(withIdentifier: "PoolsViewController") as! PoolsViewController
                viewControllers?.insert(poolsVC, at: poolsIndex)
                return
            }
            print("----> PoolsViewController already loaded.", #function)
        } else {
            guard poolsIndex < viewControllers!.count else { return }
            viewControllers?.remove(at: poolsIndex)
        }
        
    }
}
