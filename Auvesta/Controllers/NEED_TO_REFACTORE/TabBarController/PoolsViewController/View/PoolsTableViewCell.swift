import UIKit

protocol PoolsTableViewCellDelegate {
    func didTapActionButton(cell: PoolsTableViewCell, upgradeLink: String, sender: ButtonWithActivityIndicator)
}

class PoolsTableViewCell: UITableViewCell {
    private enum PoolType {
        case active
        case notQualified
    }
    
    @IBOutlet private weak var setupGoalLabel: UILabel!
    @IBOutlet private weak var setupGoalProgressView: CustomProgressView!
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var incentiveImageView: UIImageView!
    @IBOutlet private weak var incentiveTypeImageView: UIImageView!
    @IBOutlet private weak var incentiveNameLabel: UILabel!
    @IBOutlet private weak var progressView: CustomProgressView!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var revenueGoalLabel: UILabel!
//    @IBOutlet private weak var goalValueLabel: UILabel!
    @IBOutlet private weak var actionButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var incentiveBlurView: UIView!
    
    private var upgradeLink = ""
    var delegate: PoolsTableViewCellDelegate?
    
    var data: AuvestaIncentive? {
        didSet{
            incentiveImageView.image = nil
            updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func updateData() {
        guard let incentive = data else { return }
        
        upgradeLink = incentive.upgradeLink
        
        switch incentive.isEnabled {
        case true:
            localize(poolType: .active)
            setupButtonUI(poolType: .active)
            setupIncentiveBlur(poolType: .active)
            revenueGoalLabel.attributedText = configureRevenueGoalString(incentive: incentive)
            revenueGoalLabel.isHidden = false
        default:
            revenueGoalLabel.isHidden = true
            localize(poolType: .notQualified)
            setupButtonUI(poolType: .notQualified)
            setupIncentiveBlur(poolType: .notQualified)
        }
        
        setupGoalLabel.attributedText = configureSetupFeeString(incentive: incentive)
        setupGoalProgressView.progress = incentive.setupPercent
        progressView.progress = incentive.progressPercent
        incentiveNameLabel.text = incentive.title
        messageLabel.text = incentive.description
        guard let url = URL(string: incentive.imageUrl) else { return }
        incentiveImageView?.sd_setImage(with: url, completed: nil)
    }
    
    private func configureSetupFeeString(incentive: AuvestaIncentive) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        let poolValueString = NSAttributedString(string: "setupFeeProcess".localized())
        let colorAttribute = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8744713068, green: 0.7059164643, blue: 0.3019405901, alpha: 1)]
        
        let formattedGoal = "\(String(format: "%.2f", incentive.setupGoal)) \(incentive.currency.stringIcon)"
        let formattedTotal = " / \(String(format: "%.0f", incentive.setupGoalTotal)) Mio. \(incentive.currency.stringIcon)"
        
        let valueString = NSAttributedString(string: (formattedGoal + formattedTotal), attributes: colorAttribute)
        mutableAttributedString.append(poolValueString)
        mutableAttributedString.append(valueString)
        return mutableAttributedString
    }
    
    private func configureRevenueGoalString(incentive: AuvestaIncentive) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        let poolValueString = NSAttributedString(string: "revenueGoal".localized())
        let colorAttribute = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8744713068, green: 0.7059164643, blue: 0.3019405901, alpha: 1)]
        
        let formattedGoal = "\(String(format: "%.2f", incentive.goal)) \(incentive.currency.stringIcon)"
        let formattedTotal = " / \(String(format: "%.0f", incentive.goalTotal)) Mio. \(incentive.currency.stringIcon)"
        
        let valueString = NSAttributedString(string: (formattedGoal + formattedTotal), attributes: colorAttribute)
        mutableAttributedString.append(poolValueString)
        mutableAttributedString.append(valueString)
        return mutableAttributedString
    }
    
    private func localize(poolType: PoolType) {
        switch poolType {
        case .active:
            messageLabel.text = "startNowToGetPrize".localized()
//            revenueGoalLabel.text = "revenueGoal".localized()
        case .notQualified:
            messageLabel.text = "youAreNotQualified".localized()
            revenueGoalLabel.text = "youHaveToUpgrade".localized()
        }
    }
    
    private func setupUI() {
        shadowView.layer.cornerRadius = 10
        shadowView.clipsToBounds = true
    }
    
    private func setupIncentiveBlur(poolType: PoolType) {
        switch poolType {
        case .active:
            incentiveBlurView.alpha = 0
        case .notQualified:
            incentiveBlurView.alpha = 0.6
        }
    }
    
    private func setupButtonUI(poolType: PoolType) {
        actionButton.layer.cornerRadius = actionButton.bounds.height / 2
        
        switch poolType {
        case .active:
            actionButton.setTitle("startButton".localized(), for: .normal)
            actionButton.backgroundColor = #colorLiteral(red: 0.1436192095, green: 0.147418946, blue: 0.188558042, alpha: 1)
            actionButton.setTitleColor(.white, for: .normal)
            actionButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        case .notQualified:
            actionButton.setTitle("upgradeButton".localized(), for: .normal)
            actionButton.backgroundColor = #colorLiteral(red: 0.8811834455, green: 0.7158268094, blue: 0.2948135138, alpha: 1)
            actionButton.setTitleColor(.black, for: .normal)
            actionButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        }
    }
    
    @IBAction func didTapActionButton(_ sender: ButtonWithActivityIndicator) {
        delegate?.didTapActionButton(cell: self, upgradeLink: upgradeLink, sender: sender)
    }
    
}
