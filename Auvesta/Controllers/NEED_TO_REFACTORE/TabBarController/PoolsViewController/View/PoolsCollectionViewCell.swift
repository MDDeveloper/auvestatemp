import UIKit
import UICircularProgressRing

class PoolsCollectionViewCell: UICollectionViewCell {
    private enum PoolState: Int {
        case notActive = 0
        case active = 1
        case completed = 2
    }
    
    @IBOutlet private weak var poolBackgroundView: UIView!
    @IBOutlet private weak var poolNameLabel: UILabel!
    @IBOutlet private weak var poolMessageLabel: UILabel!
    @IBOutlet private weak var poolDeadlineLabel: UILabel!
    @IBOutlet private weak var upgradeButton: UIButton!
    
    @IBOutlet private weak var poolValueLabel: UILabel!
    @IBOutlet private weak var profitLabel: UILabel!
    
    @IBOutlet private weak var sharesQualifiedLabel: UILabel!
    @IBOutlet private weak var sharesQualifiedAmountLabel: UILabel!
    
    @IBOutlet private weak var mySharesLabel: UILabel!
    @IBOutlet private weak var mySharesAmountLabel: UILabel!
    
    @IBOutlet private weak var qualifiedPeopleLabel: UILabel!
    @IBOutlet private weak var qualifiedPeopleAmountLabel: UILabel!
    
    @IBOutlet private weak var firstCircleView: UIView!
    @IBOutlet private weak var roundedFirstBackgroundView: UIView!
    @IBOutlet private weak var firstCompleteImageView: UIImageView!
    @IBOutlet private weak var firstCompleteLabel: UILabel!
    @IBOutlet private weak var firstInfoStackView: UIStackView!
    @IBOutlet private weak var firstPercentLabel: UILabel!
    @IBOutlet private weak var firstValueNameLabel: UILabel!
    @IBOutlet private weak var firstValueAmountLabel: UILabel!
    
    @IBOutlet private weak var secondCircleView: UIView!
    @IBOutlet private weak var roundedSecondBackgroundView: UIView!
    @IBOutlet private weak var secondCompleteImageView: UIImageView!
    @IBOutlet private weak var secondCompleteLabel: UILabel!
    @IBOutlet private weak var secondInfoStackView: UIStackView!
    @IBOutlet private weak var secondPercentLabel: UILabel!
    @IBOutlet private weak var secondValueNameLabel: UILabel!
    @IBOutlet private weak var secondValueAmountLabel: UILabel!
    
    var metalRevenueProgressView = UICircularProgressRing()
    private var setupFeeProgressView = UICircularProgressRing()
    
    private var upgradeLink = ""
    private var deadlineDate: NSDate?
    private var countdownTimer = Timer()
    
    var data: AuvestaPool? {
        didSet{
            updateData()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupUI()
        localize()
        updateData()
    }
    
    private func updateData() {
        guard let pool = data else { return }
        
        upgradeLink = pool.upgradeLink
        poolNameLabel.text = pool.name
        let formattedPoolAmount = String(format: "%.2f", pool.totalPoolAmount)
        let formattedProfitAmount = String(format: "%.2f", pool.myProfitAmount)
        poolValueLabel.attributedText = configurePoolValueString(value: "\(formattedPoolAmount)\(pool.currency.stringIcon)")
        profitLabel.attributedText = configureProfitString(value: "\(formattedProfitAmount)\(pool.currency.stringIcon)")
        sharesQualifiedAmountLabel.text = "\(pool.sharesQualified)"
        qualifiedPeopleAmountLabel.text = "\(pool.qualifiedPeople)"
        mySharesAmountLabel.text = "\(pool.myShares)"
        
        switch pool.isEnabled {
        case true:
            poolMessageLabel.text = pool.description
            startDeadlineTimer(stringDate: pool.deadlineStringDate)
            poolMessageLabel.isHidden = false
            poolDeadlineLabel.isHidden = false
            upgradeButton.isHidden = true
        default:
            poolMessageLabel.isHidden = true
            poolDeadlineLabel.isHidden = true
            upgradeButton.isHidden = false
        }
        
        updateProgressViewData(progressView: metalRevenueProgressView, value: pool.percentProgress)
        updateProgressViewData(progressView: setupFeeProgressView, value: pool.feePercentProgress)
        
        configureRevenueInfo(with: pool)
        configureFeeInfo(with: pool)
    }
    
    private func startDeadlineTimer(stringDate: String) {
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        deadlineDate = releaseDateFormatter.date(from: stringDate)! as NSDate
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc
    func updateTime() {
        let currentDate = Date()
        let calendar = Calendar.current
        
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: deadlineDate! as Date)
//        let countdown = "\(diffDateComponents.day ?? 0) Days \(diffDateComponents.hour ?? 0) Hours \(diffDateComponents.minute ?? 0) Minutes Left"
        poolDeadlineLabel.attributedText = configureDeadlineString(dateComponents: diffDateComponents)
    }
    
    private func setupRevenueUI(state: PoolState) {
        for view in self.roundedFirstBackgroundView.subviews {
            view.isHidden = true
        }
        
        switch state {
        case .active, .notActive:
            firstInfoStackView.isHidden = false
            self.firstPercentLabel.isHidden = false
            self.firstValueNameLabel.isHidden = false
            self.firstValueAmountLabel.isHidden = false
        case .completed:
            self.firstCompleteLabel.isHidden = false
            self.firstCompleteImageView.isHidden = false
        }
    }
    
    private func setupFeeUI(state: PoolState) {
        for view in self.roundedSecondBackgroundView.subviews {
            view.isHidden = true
        }
        
        switch state {
        case .active, .notActive:
            secondInfoStackView.isHidden = false
            self.secondPercentLabel.isHidden = false
            self.secondValueNameLabel.isHidden = false
            self.secondValueAmountLabel.isHidden = false
        case .completed:
            self.secondCompleteLabel.isHidden = false
            self.secondCompleteImageView.isHidden = false
        }
    }
    
    private func configureRevenueInfo(with data: AuvestaPool) {
        guard let currentState = PoolState(rawValue: data.revenueState) else { return }
        
        switch currentState {
        case .active, .notActive:
            firstPercentLabel.text = "\(data.percentProgress)%"
            let formattedAmount = String(format: "%.2f", data.remainingAmount)
            firstValueAmountLabel.text = "\(formattedAmount) \(data.currency.stringIcon)"
            setupRevenueUI(state: currentState)
        case .completed:
            setupRevenueUI(state: currentState)
        }
    }
    
    private func configureFeeInfo(with data: AuvestaPool) {
        guard let currentState = PoolState(rawValue: data.setupFeeState) else { return }
        
        switch currentState {
        case .active, .notActive:
            secondPercentLabel.text = "\(data.feePercentProgress)%"
            let formattedAmount = String(format: "%.2f", data.feeRemainingAmount)
            secondValueAmountLabel.text = "\(formattedAmount) \(data.currency.stringIcon)"
            setupFeeUI(state: currentState)
        case .completed:
            setupFeeUI(state: currentState)
        }
    }
    
    private func configureDeadlineString(dateComponents: DateComponents) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        let colorAttribute = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5921034217, green: 0.5921911597, blue: 0.592084229, alpha: 1)]

        let days = NSAttributedString(string: "\(dateComponents.day ?? 0)")
        let daysString = NSAttributedString(string: " Days", attributes: colorAttribute)
        
        let hours = NSAttributedString(string: "\(dateComponents.hour ?? 0)")
        let hoursString = NSAttributedString(string: " Hours", attributes: colorAttribute)
        
        let minutes = NSAttributedString(string: "\(dateComponents.minute ?? 0)")
        let minutesString = NSAttributedString(string: " Minutes left", attributes: colorAttribute)
        
        mutableAttributedString.append(days)
        mutableAttributedString.append(daysString)
        mutableAttributedString.append(hours)
        mutableAttributedString.append(hoursString)
        mutableAttributedString.append(minutes)
        mutableAttributedString.append(minutesString)
        return mutableAttributedString
    }
    
    private func configurePoolValueString(value: String) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        let poolValueString = NSAttributedString(string: "poolValueString".localized())
        let colorAttribute = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8744713068, green: 0.7059164643, blue: 0.3019405901, alpha: 1)]
        let valueString = NSAttributedString(string: value, attributes: colorAttribute)
        mutableAttributedString.append(poolValueString)
        mutableAttributedString.append(valueString)
        return mutableAttributedString
    }
    
    private func configureProfitString(value: String) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString()
        let poolValueString = NSAttributedString(string: "profitString".localized())
        let colorAttribute = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8744713068, green: 0.7059164643, blue: 0.3019405901, alpha: 1)]
        let valueString = NSAttributedString(string: value, attributes: colorAttribute)
        mutableAttributedString.append(poolValueString)
        mutableAttributedString.append(valueString)
        return mutableAttributedString
    }
    
    private func localize() {
        poolValueLabel.text = "poolValueString".localized()
        profitLabel.text = "profitString".localized()
        sharesQualifiedLabel.text = "sharesQualifiedString".localized()
        mySharesLabel.text = "mySharesString".localized()
        qualifiedPeopleLabel.text = "qualifiedPeopleString".localized()
        firstValueNameLabel.text = "totalMetalString".localized()
        firstCompleteLabel.text = "totalMetalString".localized()
        secondValueNameLabel.text = "setupFeeString".localized()
        secondCompleteLabel.text = "setupFeeString".localized()
        upgradeButton.setTitle("upgradeButton".localized(), for: .normal)
    }
    
    private func updateProgressViewData(progressView: UICircularProgressRing, value: Double) {
        let colors = value == 100 ? [#colorLiteral(red: 0.3927343786, green: 0.6394734979, blue: 0.3967420161, alpha: 1),#colorLiteral(red: 0.4216424227, green: 0.7610625625, blue: 0.4028561413, alpha: 1)] : [#colorLiteral(red: 0.8145852685, green: 0.660461843, blue: 0.2676816881, alpha: 1),#colorLiteral(red: 0.8985717297, green: 0.7268576026, blue: 0.2927324772, alpha: 1)]
//        if value == 100 {
//            colors = [#colorLiteral(red: 0.3927343786, green: 0.6394734979, blue: 0.3967420161, alpha: 1),#colorLiteral(red: 0.4216424227, green: 0.7610625625, blue: 0.4028561413, alpha: 1)]
//        } else {
//            colors = [#colorLiteral(red: 0.8145852685, green: 0.660461843, blue: 0.2676816881, alpha: 1),#colorLiteral(red: 0.8985717297, green: 0.7268576026, blue: 0.2927324772, alpha: 1)]
//        }
        
        let gradient = UICircularRingGradientOptions(startPosition: .topLeft,
                                                     endPosition: .topRight,
                                                     colors: colors,
                                                     colorLocations: [CGFloat(0.0), CGFloat(1.0)])
        progressView.gradientOptions = gradient
        DispatchQueue.main.async {
            progressView.startProgress(to: CGFloat(value), duration: 3)
        }
    }
    
    private func setupCircularProgressViewUI(progressView: UICircularProgressRing, background: UIView) {
        let width = CGFloat(10)
        let colors = [#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)]
        let gradient = UICircularRingGradientOptions(startPosition: .topLeft,
                                                     endPosition: .topRight,
                                                     colors: colors,
                                                     colorLocations: [CGFloat(0.0), CGFloat(1.0)])
        
        
        progressView.frame = CGRect(x: 0, y: 0, width: background.frame.width, height: background.frame.height)
        progressView.minValue = 0
        progressView.maxValue = 100
        progressView.value = 0
        progressView.fullCircle = false
        progressView.style = .ontop
        progressView.shouldShowValueText = false
        progressView.startAngle = -90
        progressView.endAngle = 270
        
        progressView.innerRingWidth = width
        progressView.gradientOptions = gradient
        
        progressView.outerRingWidth = width
        progressView.outerRingColor = #colorLiteral(red: 0.8939015865, green: 0.8940303922, blue: 0.8938733935, alpha: 1)
        progressView.outerCapStyle = .round
        
        background.addSubview(progressView)
        
        NSLayoutConstraint.activate([progressView.topAnchor.constraint(equalTo: background.topAnchor),
                                     progressView.leadingAnchor.constraint(equalTo: background.leadingAnchor),
                                     progressView.trailingAnchor.constraint(equalTo: background.trailingAnchor),
                                     progressView.bottomAnchor.constraint(equalTo: background.bottomAnchor)])
    }
    
    private func setupUI() {
        poolBackgroundView.layer.cornerRadius = 10
        upgradeButton.layer.cornerRadius = upgradeButton.bounds.size.height / 2
        
        setupRoundedBackgrounds(views: [roundedFirstBackgroundView, roundedSecondBackgroundView])
        setupCircularProgressViewUI(progressView: metalRevenueProgressView, background: firstCircleView)
        setupCircularProgressViewUI(progressView: setupFeeProgressView, background: secondCircleView)
    }
    
    private func setupRoundedBackgrounds(views: [UIView]) {
        for view in views {
            view.layer.cornerRadius = view.bounds.size.height / 2
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowRadius = 2
            view.layer.shadowOpacity = 0.15
            view.layer.shadowOffset = CGSize(width: 0, height: 0)
        }
    }
    
    @IBAction func didTapUpgradeButton(_ sender: UIButton) {
        guard let url = URL(string: upgradeLink) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
