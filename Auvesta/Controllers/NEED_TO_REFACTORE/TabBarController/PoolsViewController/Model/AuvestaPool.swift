import UIKit

class AuvestaPool {
    var name: String = ""
    var description: String = ""
    var percentProgress: Double = 0
    var remainingAmount: Double = 0
    var currency: AuvestaCurrency = .euro
    var totalPoolAmount: Double = 0
    var myProfitAmount: Double = 0
    var sharesQualified: Int = 0
    var qualifiedPeople: Int = 0
    var myShares: Int = 0
    var deadlineStringDate: String = ""
    var isEnabled: Bool = false
    var revenueState: Int = 0
    var setupFeeState: Int = 0
    var feeRemainingAmount: Double = 0
    var feePercentProgress: Double = 0
    var upgradeLink: String = ""
    
    static func getArray(from jsonArray: Any) -> [AuvestaPool] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [AuvestaPool]() }
        return jsonArray.compactMap{ AuvestaPool(json: $0) }
    }
    
    init(json: [String:Any]) {
        guard
            let name = json["poolName"] as? String,
            let description = json["poolDescription"] as? String,
            let percent = json["percentProgress"] as? Double,
            let remainingAmount = json["remainingAmount"] as? Double,
            let currencyInt = json["currency"] as? Int,
            let totalPoolAmount = json["totalPoolAmount"] as? Double,
            let profit = json["myProfitAmount"] as? Double,
            let sharesQualified = json["sharesQualified"] as? Int,
            let qualifiedPeople = json["qualifiedPeople"] as? Int,
            let myShares = json["myShares"] as? Int,
            let isEnabled = json["enabled"] as? Bool,
            let revenueState = json["revenueState"] as? Int,
            let setupFeeState = json["setupFeeState"] as? Int,
            let feePercentProgress = json["setupFeePercent"] as? Double,
            let upgradeLink = json["upgradeLink"] as? String,
            let setupFeeAmount = json["setupFeeMissing"] as? Double,
            let deadline = json["deadline"] as? String else { return }
        
        self.name =  name
        self.description = description
        self.percentProgress = percent
        self.remainingAmount = remainingAmount
        self.feeRemainingAmount = setupFeeAmount
        self.currency = AuvestaCurrency(rawValue: currencyInt)!
        self.totalPoolAmount = totalPoolAmount
        self.myProfitAmount = profit
        self.sharesQualified = sharesQualified
        self.qualifiedPeople = qualifiedPeople
        self.myShares = myShares
        self.isEnabled = isEnabled
        self.revenueState = revenueState
        self.setupFeeState = setupFeeState
        self.feePercentProgress = feePercentProgress
        self.upgradeLink = upgradeLink
        self.deadlineStringDate = String(deadline.dropLast(8))
    }
}
