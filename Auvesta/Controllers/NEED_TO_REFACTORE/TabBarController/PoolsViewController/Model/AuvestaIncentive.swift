import UIKit

class AuvestaIncentive {
    var imageUrl: String = ""
    var title: String = ""
    var description: String = ""
    var currency: AuvestaCurrency = .euro
    var isEnabled: Bool = false
    var upgradeLink: String = ""

    var progressPercent: Float = 0
    var goal: Double = 0
    var goalTotal: Double = 0
    
    var setupPercent: Float = 0
    var setupGoal: Double = 0
    var setupGoalTotal: Double = 0
    
    static func getArray(from jsonArray: Any) -> [AuvestaIncentive] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [AuvestaIncentive]() }
        return jsonArray.compactMap{ AuvestaIncentive(json: $0) }
    }
    
    init(json: [String:Any]) {
        guard
            let title = json["title"] as? String,
            let url = json["imageUrl"] as? String,
            let description = json["description"] as? String,
            let goal = json["goal"] as? Double,
            let currencyInt = json["currency"] as? Int,
            let percent = json["percentProgress"] as? Double,
            let isEnabled = json["enabled"] as? Bool,
            let upgradeLink = json["upgradeLink"] as? String,
            let setupPercent = json["percentSetup"] as? Double,
            let revenueGoalTotal = json["goalTotal"] as? Double,
            let setupGoalTotal = json["setupGoalTotal"] as? Double,
            let setupGoal = json["setupGoal"] as? Double else { return }
        
        self.title =  title
        self.imageUrl = url
        self.description = description
        self.goal = goal
        self.currency = AuvestaCurrency(rawValue: currencyInt)!
        self.progressPercent = Float((percent) / 100)
        self.setupPercent = Float((setupPercent) / 100)
        self.goalTotal = revenueGoalTotal / 1000000
        self.setupGoalTotal = setupGoalTotal / 1000000
        self.setupGoal = setupGoal
        self.isEnabled = isEnabled
        self.upgradeLink = upgradeLink
    }
}
