import UIKit

protocol PoolsViewControllerDataSource {
    func getPoolsCount(vc: PoolsViewController) -> Int
    func getPoolData(vc: PoolsViewController, indexPath: IndexPath) -> AuvestaPool
    func getIncentivesCount(vc: PoolsViewController) -> Int
    func getIncentiveData(vc: PoolsViewController, indexPath: IndexPath) -> AuvestaIncentive
}

class PoolsViewController: UIViewController {
    
    @IBOutlet weak var poolsTableView: UITableView!
    @IBOutlet weak var poolsPageControl: UIPageControl!
    @IBOutlet weak var poolsCollectionView: UICollectionView!
    @IBOutlet weak var tableViewHeaderLabel: UILabel!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var notificationButton: ButtonWithActivityIndicator!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var poolsArray = [AuvestaPool]() {
        didSet {
            DispatchQueue.main.async {
                self.poolsCollectionView.reloadData()
                self.poolsPageControl.numberOfPages = self.poolsArray.count
            }
        }
    }
    private var incentivesArray = [AuvestaIncentive]() {
        didSet {
            DispatchQueue.main.async {
                self.poolsTableView.reloadData()
                self.poolsPageControl.numberOfPages = self.poolsArray.count
            }
        }
    }
        
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        localize()
        calculatePoolCellFrame()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        downloadPoolsData()
    }
    
    func downloadPoolsData() {
        self.notificationButton.startActivityIndicator()
        MainNetworkManager.getPools { (status, pools, incentives, error) in
            let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
            switch status {
            case 200:
                guard let pools = pools, pools.count > 0, let incentives = incentives, incentives.count > 0 else {
                    self.notificationButton.stopActivityIndicator()
                    self.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
                    return
                }
                self.poolsArray = pools
                self.incentivesArray = incentives
                
                self.notificationButton.stopActivityIndicator()
            default:
                self.notificationButton.stopActivityIndicator()
                self.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    private func setupUI() {
        poolsPageControl.numberOfPages = poolsArray.count
        poolsPageControl.currentPage = 0
    }
    
    private func localize() {
        headerTitleLabel.text = "poolsHeader".localized()
        tableViewHeaderLabel.text = "poolsTableViewHeader".localized()
    }
    
    private func calculatePoolCellFrame() {
        let collectionViewSize = poolsCollectionView.bounds.size
        
        let cellWidth = floor(collectionViewSize.width * 0.9 )
        let cellHeight = floor(collectionViewSize.height * 1 )
        
        let insetX = (view.bounds.width - cellWidth) / 2
        let insetY = CGFloat(0)
        
        let layout = poolsCollectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        poolsCollectionView.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
    }
    
    private func centerCellHorizontallyWhenScrolling(scrollView: UIScrollView, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let scrollView = poolsCollectionView else { return }
        let layout = self.poolsCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
    
}

extension PoolsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        incentivesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "poolCell", for: indexPath) as! PoolsTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.data = incentivesArray[indexPath.row]
        return cell
    }
    
}

extension PoolsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
}

extension PoolsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        poolsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "poolCell", for: indexPath) as! PoolsCollectionViewCell
        cell.data = poolsArray[indexPath.row]
        return cell
    }
}

extension PoolsViewController: UICollectionViewDelegate, UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView == poolsCollectionView else { return }
        centerCellHorizontallyWhenScrolling(scrollView: scrollView, targetContentOffset: targetContentOffset)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == poolsCollectionView else { return }
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        poolsPageControl.currentPage = Int(pageNumber)
    }
}

extension PoolsViewController: PoolsTableViewCellDelegate {

    func didTapActionButton(cell: PoolsTableViewCell, upgradeLink: String, sender: ButtonWithActivityIndicator) {
        guard let buttonText = sender.titleLabel?.text else { return }

        switch buttonText {
        case "upgradeButton".localized():
            guard let url = URL(string: upgradeLink) else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        case "startButton".localized():
            let referVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: ReferFriendViewController.self)) as! ReferFriendViewController
            self.navigationController?.pushViewController(referVC, animated: true)
        default:
            print("unknown action", #function, #file)
            return
        }
    }

}
