import UIKit

class AccountController {
    private enum Newsletter {
        case depot
        case partner
        
        var backendID: Int {
            switch self {
            case .depot: return 0
            case .partner: return 1
            }
        }
    }
    
    var viewController: AccountViewController?
    
    private var settingsCells = UserAccountSettings.loadCells(user: AuvestaUser.shared)
    
    // MARK: Methods
    
    private func loadUpdateInfoVC(vc: UIViewController, configuration: UpdateUserInfoViewController.Configuration, request: AccountSettingsCell) {
        let storyboard = UIStoryboard(name: "UpdateUserInfoViewController", bundle: nil)
        let updateVC = storyboard.instantiateViewController(withIdentifier: "UpdateUserInfoViewController") as! UpdateUserInfoViewController
        
        updateVC.delegate = self
        updateVC.viewController = vc
        updateVC.requestType = request.cellInfo
        updateVC.configuration = configuration
        vc.present(updateVC, animated: true)
    }
    
    private func changeNewsletter(vc: AccountViewController, sender: Any, newsletter: Newsletter) {
        let switchCell = sender as! CustomSwitch
        let body: [String: Any] = ["news": switchCell.isOn, "newsletterID": newsletter.backendID]
        
        AccountNetworkManager.changeNewsletter(body: body) { (status, error) in
            switch status {
            case 200:
                switch newsletter {
                case .depot:
                    AuvestaUser.shared.depotNewsletter = switchCell.isOn
                case .partner:
                    AuvestaUser.shared.partnerNewsletter = switchCell.isOn
                }
            case 401:
                vc.navigationController?.popToRootViewController(animated: true)
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
                    switchCell.isOn.toggle()
                })
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    init(viewController: AccountViewController) {
        self.viewController = viewController
        viewController.dataSource = self
        viewController.delegate = self
    }
}

// MARK: AccountViewControllerDataSource

extension AccountController: AccountViewControllerDataSource {
    func updateUserInfo(vc: AccountViewController) {
        vc.notificationsButton.startActivityIndicator()
        
        MainNetworkManager.getUserInfo { (status, error) in
            vc.notificationsButton.stopActivityIndicator()
            vc.notificationsButton.isHidden = false
            
            switch status {
            case 200:
                DispatchQueue.main.async {
                    self.settingsCells = UserAccountSettings.loadCells(user: AuvestaUser.shared)
                    vc.settingsTableView.reloadData()
                    vc.loadUserSettings(user: AuvestaUser.shared)
                }
            case 401:
                vc.navigationController?.popToRootViewController(animated: true)
            default:
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
    
    func getCell(vc: AccountViewController, indexPath: IndexPath) -> AccountSettingsCell {
        return settingsCells[indexPath.row]
    }
    
    func getCellsCount(vc: AccountViewController) -> Int {
        return settingsCells.count
    }
}

// MARK: AccountViewControllerDelegate

extension AccountController: AccountViewControllerDelegate {
    func didTapCellActionButton(vc: AccountViewController, cellData: AccountSettingsCell, sender: Any) {
        switch cellData.cellInfo {
        case .psn:
            self.loadUpdateInfoVC(vc: vc, configuration: .changePsn, request: cellData)
        case .password:
            self.loadUpdateInfoVC(vc: vc, configuration: .changePassword, request: cellData)
        case .biometricks:
            AuthManager.shared.setBiometriksPermission(vc: vc) {_ in 
                DispatchQueue.main.async {
                    self.updateUserInfo(vc: vc)
                }
            }
        case .depotNewsletter:
            self.changeNewsletter(vc: vc, sender: sender, newsletter: .depot)
        case .partnerNewsletter:
            self.changeNewsletter(vc: vc, sender: sender, newsletter: .partner)
        case .logout:            
            AppDelegate.shared.rootViewController.showLoginScreen()
            AuvestaUser.shared.resetPrivateProperties()
            AuthManager.shared.logOutUser()
        default:
            print(cellData.cellInfo)
        }
    }
    
    func didTapUserImageButton(vc: AccountViewController) {
        vc.showImagePickerAlert()
    }
}

// MARK: UpdateUserInfoDelegate

extension AccountController: UpdateUserInfoViewControllerDelegate {
    func didTapApplyButton(vc: UpdateUserInfoViewController, parentViewController: UIViewController?, requestType: Any) {
        vc.applyButton.startActivityIndicator()
        let type = requestType as! AccountSettingsCell.cellInfo
        
        switch type {
        case .username:
            guard
                let firstName = vc.firstTextField.text,
                let lastName = vc.secondTextField.text else { return }
            let body: [String:Any] = ["firstName":firstName, "lastName":lastName]
            
            AccountNetworkManager.changeUsername(body: body) { (status, error) in
                vc.applyButton.stopActivityIndicator()
                switch status {
                case 200:
                    vc.dismiss(animated: true, completion: nil)
                case 401:
                    vc.dismiss(animated: true) {
                        vc.viewController?.tabBarController?.navigationController?.popToRootViewController(animated: true)
                    }
                default:
                    let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                    vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
                }
            }
        case .email: return
        case .psn:
            guard let psn = vc.firstTextField.text else { return }
            let body: [String: Any] = ["psn": psn]
            
            AccountNetworkManager.changePsn(body: body) { (status, error) in
                switch status {
                case 200:
                    vc.dismiss(animated: true) {
                        vc.viewController?.viewWillAppear(true)
                    }
                case 401:
                    vc.dismiss(animated: true) {
                        vc.viewController?.tabBarController?.navigationController?.popToRootViewController(animated: true)
                    }
                default:
                    let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                    vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
                }
            }
        case .password:
            guard
                let oldPassword = vc.firstTextField.text,
                let newPassword = vc.secondTextField.text,
                let confirmPassword = vc.thirdTextField.text else { return }
            let plainPassword: [String: Any] = ["password": newPassword, "confirmPassword": confirmPassword]
            let body: [String: Any] = ["oldPassword": oldPassword, "plainPassword": plainPassword]
            
            AccountNetworkManager.changePassword(body: body) { (status, error) in
                vc.applyButton.stopActivityIndicator()
                switch status {
                case 200:
                    vc.dismiss(animated: true, completion: nil)
                case 401:
                    vc.dismiss(animated: true) {
                        vc.viewController?.tabBarController?.navigationController?.popToRootViewController(animated: true)
                    }
                default:
                    let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                    vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
                }
            }
        default:
            return
        }
    }
    
    func didTapBackButton(vc: UpdateUserInfoViewController, parentViewController: UIViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
}
