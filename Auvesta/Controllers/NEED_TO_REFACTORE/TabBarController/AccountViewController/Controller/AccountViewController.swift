import UIKit

protocol AccountViewControllerDelegate: AnyObject {
    func didTapUserImageButton(vc: AccountViewController)
    func didTapCellActionButton(vc: AccountViewController, cellData: AccountSettingsCell, sender: Any)
}

protocol AccountViewControllerDataSource: AnyObject {
    func updateUserInfo(vc: AccountViewController)
    func getCellsCount(vc: AccountViewController) -> Int
    func getCell(vc: AccountViewController, indexPath: IndexPath) -> AccountSettingsCell
}

class AccountViewController: UIViewController {
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var imageContainerView: UIView!
    @IBOutlet private weak var userImageView: UIImageView!
    @IBOutlet private weak var userImageButton: UIButton!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet weak var settingsTableView: UITableView!
    @IBOutlet weak var notificationsButton: ButtonWithActivityIndicator!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate: AccountViewControllerDelegate?
    var dataSource: AccountViewControllerDataSource?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataSource?.updateUserInfo(vc: self)
    }
    
    // MARK: - Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        guard let notificationVC = segue.destination as? NotificationViewController else { return }
        _ = NotificationController(vc: notificationVC)
    }
    
    private func setupUI() {
        imageContainerView.layer.cornerRadius = imageContainerView.bounds.height / 2
        imageContainerView.layer.borderWidth = 1
        imageContainerView.layer.borderColor = #colorLiteral(red: 0.8164352775, green: 0.6262487769, blue: 0.1418944895, alpha: 1)
        
        userImageView.layer.cornerRadius = userImageView.bounds.height / 2
        userImageView.layer.borderColor = #colorLiteral(red: 0.1483671665, green: 0.1580545008, blue: 0.1960128248, alpha: 1)
        userImageView.layer.borderWidth = 4
        headerTitleLabel.text = "accountVcHeader".localized()
        self.title = "accountVcHeader".localized()
    }
    
    func loadUserSettings(user: AuvestaUser) {
        if user.genderType == "C" {
            userNameLabel.text = user.companyName
        } else {
            userNameLabel.text = user.firstName + " " + user.lastName
        }
        guard let userImageUrl = URL(string: user.userImageLink) else { return }
        userImageView.sd_setImage(with: userImageUrl, completed: nil)
    }
    
    func showImagePickerAlert() {
        let picker = UIImagePickerController()
        picker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        let controller = UIAlertController(title: "selectSource".localized(), message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "cancelTitle".localized(), style: .cancel, handler: nil)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let camera = UIAlertAction(title: "cameraSource".localized(), style: .default) { (action) in
                picker.sourceType = .camera
                self.present(picker, animated: true, completion: nil)
            }
            controller.addAction(camera)
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let library = UIAlertAction(title: "photoLibrary".localized(), style: .default) { (action) in
                picker.sourceType = .photoLibrary
                self.present(picker, animated: true, completion: nil)
            }
            controller.addAction(library)
        }
        
        controller.addAction(cancel)
        controller.popoverPresentationController?.sourceView = self.view
        
        present(controller, animated: true, completion: nil)
    }
    
    private func configureCell(tableView: UITableView, cellData: AccountSettingsCell) -> UITableViewCell {
        switch cellData.cellType {
        case .editable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editCell") as! EditTableViewCell
            cell.delegate = self
            cell.accountCell = cellData
            return cell
        case .withSwitch:
            let cell = tableView.dequeueReusableCell(withIdentifier: "switchCell") as! SwitchTableViewCell
            cell.delegate = self
            cell.accountCell = cellData
            return cell
        case .withButton:
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell") as! ButtonTableViewCell
            cell.delegate = self
            cell.accountCell = cellData
            return cell
        }
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapUserImageButton(_ sender: UIButton) {
        delegate?.didTapUserImageButton(vc: self)
    }
    
    
}

// MARK: UITableViewDataSource

extension AccountViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource!.getCellsCount(vc: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentCellData = dataSource!.getCell(vc: self, indexPath: indexPath)
        let cell = configureCell(tableView: tableView, cellData: currentCellData)
        return cell
    }
    
    
}

// MARK: ImagePickerDelegate


extension AccountViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        notificationsButton.startActivityIndicator()
        
        AccountNetworkManager.changeUserImage(image: image) { (status, error) in
            self.notificationsButton.stopActivityIndicator()
            switch status {
            case 200:
                self.userImageView.image = image
            case 401:
                self.navigationController?.popToRootViewController(animated: true)
            default:
                self.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
        dismiss(animated: true, completion: nil)
    }
}

// MARK: NavigationControllerDelegate

extension AccountViewController: UINavigationControllerDelegate {
    
}

// MARK: AccountCellDelegate

extension AccountViewController: AccountTableViewCellDelegate {
    func didTapActionButton(cellData: AccountSettingsCell, sender: Any) {
        delegate?.didTapCellActionButton(vc: self, cellData: cellData, sender: sender)
    }
}
