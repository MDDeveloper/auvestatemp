import UIKit

class EditTableViewCell: UITableViewCell {
    @IBOutlet private weak var cellImageView: UIImageView!
    @IBOutlet private weak var cellNameLabel: UILabel!
    @IBOutlet private weak var cellContentTextField: UITextField!
    
    var delegate: AccountTableViewCellDelegate?
    
    var accountCell: AccountSettingsCell? {
        didSet {
            self.updateData()
        }
    }
    
    private func updateData() {
        if let accountCell = accountCell {
            cellImageView.image = accountCell.cellInfo.cellImage
            cellNameLabel.text = accountCell.cellInfo.stringName
            cellContentTextField.text = accountCell.cellContent
            cellContentTextField.isSecureTextEntry = accountCell.cellInfo.isSecure
        } else {
            cellImageView.image = nil
            cellNameLabel.text = nil
            cellContentTextField.text = nil
        }
    }
    
    
    @IBAction func didTapEditButton(_ sender: UIButton) {
        delegate?.didTapActionButton(cellData: accountCell!, sender: sender)
    }
}
