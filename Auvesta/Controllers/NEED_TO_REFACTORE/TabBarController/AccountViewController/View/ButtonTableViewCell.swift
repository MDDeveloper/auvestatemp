import UIKit

class ButtonTableViewCell: UITableViewCell {
    @IBOutlet private weak var cellImageView: UIImageView!
    @IBOutlet private weak var cellNameLabel: UILabel!
    @IBOutlet private weak var cellButton: UIButton!
    
    var delegate: AccountTableViewCellDelegate?
    
    var accountCell: AccountSettingsCell? {
        didSet {
            self.updateData()
        }
    }
    
    private func updateData() {
        if let accountCell = accountCell {
            cellImageView.image = accountCell.cellInfo.cellImage
            cellNameLabel.text = accountCell.cellInfo.stringName
            cellButton.setTitle(accountCell.cellInfo.buttonText, for: .normal)
        } else {
            cellImageView.image = nil
            cellNameLabel.text = nil
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let accountCell = accountCell else { return }
        switch accountCell.cellInfo {
        case .logout:
            logoutUI()
        default:
            return
        }
    }
    
    private func logoutUI() {
        cellNameLabel.isHidden = true
        cellButton.setTitleColor(#colorLiteral(red: 0.6653045416, green: 0.7182314396, blue: 0.7736361623, alpha: 1), for: .normal)
        cellButton.contentHorizontalAlignment = .leading
        cellButton.titleLabel?.font = .systemFont(ofSize: 13)
        cellButton.leftAnchor.constraint(equalTo: cellImageView.rightAnchor, constant: 15).isActive = true
    }
    
    @IBAction func didTapCellActionButton(_ sender: UIButton) {
        delegate?.didTapActionButton(cellData: accountCell!, sender: sender)
    }
}
