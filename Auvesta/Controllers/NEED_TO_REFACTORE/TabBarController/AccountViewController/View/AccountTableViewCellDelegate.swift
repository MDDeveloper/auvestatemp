import Foundation

protocol AccountTableViewCellDelegate: AnyObject {
    func didTapActionButton(cellData: AccountSettingsCell, sender: Any)
}
