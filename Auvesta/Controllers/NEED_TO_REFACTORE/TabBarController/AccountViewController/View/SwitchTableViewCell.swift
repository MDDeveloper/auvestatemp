import UIKit

class SwitchTableViewCell: UITableViewCell {

    @IBOutlet private weak var cellImageView: UIImageView!
    @IBOutlet private weak var cellNameLabel: UILabel!
    @IBOutlet private weak var cellCustomSwitch: CustomSwitch!
    
    var delegate: AccountTableViewCellDelegate?

    var accountCell: AccountSettingsCell? {
        didSet {
            self.updateData()
        }
    }
    
    private func updateData() {
        if let accountCell = accountCell {
            cellImageView.image = accountCell.cellInfo.cellImage
            cellNameLabel.text = accountCell.cellInfo.stringName
            cellCustomSwitch.isOn = accountCell.switchIsOn!
        } else {
            cellImageView.image = nil
            cellNameLabel.text = nil
        }
    }
    
    @IBAction func didTapSwitch(_ sender: CustomSwitch) {
        delegate?.didTapActionButton(cellData: accountCell!, sender: sender)
    }
}
