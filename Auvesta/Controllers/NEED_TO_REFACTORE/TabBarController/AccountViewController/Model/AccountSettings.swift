import UIKit

class UserAccountSettings {
    static func loadCells(user: AuvestaUser) -> [AccountSettingsCell] {
        var cells = [AccountSettingsCell(type: .editable,
                                         info: .password,
                                         content: "********"),
                     
                     AccountSettingsCell(type: .withSwitch,
                                         info: .biometricks,
                                         switchIsOn: AuthManager.shared.isBiometricksEnabled),
                     
                     AccountSettingsCell(type: .withSwitch,
                                         info: .depotNewsletter,
                                         switchIsOn: user.depotNewsletter),
                     
                     AccountSettingsCell(type: .withButton,
                                         info: .logout)
        ]
        
        let partnerNewsletterCell = AccountSettingsCell(type: .withSwitch, info: .partnerNewsletter, switchIsOn: user.partnerNewsletter)
        let changePsnCell = AccountSettingsCell(type: .editable, info: .psn, content: user.psn)
        
        if !user.partnerNewsletterIsHidden {
            cells.insert(partnerNewsletterCell, at: cells.count - 1)
        }
        
        if user.isPartner {
            cells.insert(changePsnCell, at: 0)
        }
        
        return cells
    }
}

class AccountSettingsCell {
    enum CellType {
        case editable
        case withSwitch
        case withButton
    }
    
    enum cellInfo {        
        case username
        case psn
        case email
        case password
        case depotNewsletter
        case partnerNewsletter
        case affiliate
        case logout
        case biometricks
        
        var stringName: String {
            switch self {
            case .username: return "usernameStringName".localized()
            case .psn: return "psnStringName".localized()
            case .email: return "emailStringName".localized()
            case .password: return "passwordStringName".localized()
            case .depotNewsletter: return "depotNlStringName".localized()
            case .partnerNewsletter: return "partnerNlStringName".localized()
            case .affiliate: return "affiliateStringName".localized()
            case .logout: return "logoutStringName".localized()
            case .biometricks: return AuthManager.shared.biometriksSettingsText 
            }
        }
        
        var cellImage: UIImage {
            switch self {
            case .username, .logout, .psn: return UIImage(named: "user settings icon")!
            case .email, .depotNewsletter, .partnerNewsletter: return UIImage(named: "email settings icon")!
            case .password, .affiliate, .biometricks: return UIImage(named: "secure settings icon")!
            }
        }
        
        var buttonText: String {
            switch self {
            case .affiliate: return "affiliateButtonText".localized()
            case .logout: return "logoutButtonText".localized()
            default: return ""
            }
        }
        
        var isSecure: Bool {
            switch self {
            case .password:
                return true
            default:
                return false
            }
        }
        
    }
    
    var cellType: CellType
    var cellInfo: cellInfo
    var cellContent: String?
    var switchIsOn: Bool?
    
    init(type: CellType,
         info: cellInfo,
         content: String? = nil,
         switchIsOn: Bool? = nil) {
        self.cellType = type
        self.cellInfo = info
        self.cellContent = content
        self.switchIsOn = switchIsOn
    }
}
