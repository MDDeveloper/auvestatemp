import UIKit
import SDWebImage

class ContractTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var contractImageBackgroundView: UIView!
    @IBOutlet private weak var contractImageView: UIImageView!
    
    @IBOutlet private weak var contractNameLabel: UILabel!
    @IBOutlet private weak var contractStatusLabel: UILabel!
    @IBOutlet private weak var contractAmountLabel: UILabel!
    @IBOutlet private weak var contractHolderExternalID: UILabel!
    
    var contractData: AuvestaContract? {
        didSet {
            updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        updateUI()
    }
    
    private func updateData() {
        if let contractData = contractData {
            contractNameLabel.text = contractData.contractHolderName
            contractAmountLabel.text = contractData.contractAmount
            contractStatusLabel.text = contractData.contractStatus.stringStatus
            contractStatusLabel.textColor = contractData.contractStatus.colorStatus
            contractHolderExternalID.text = contractData.externalPartnerId
            contractHolderExternalID.isHidden = false
            contractImageView.image = nil
            guard let url = URL(string: contractData.contractHolderImageLink) else {
                self.contractImageView.image = UIImage(named: "new user icon")
                return
            }
            contractImageView?.sd_setImage(with: url, completed: nil)
        } else {
            contractImageView.image = nil
            contractNameLabel.text = nil
            contractAmountLabel.text = nil
            contractStatusLabel.text = nil
        }
    }
    
    private func updateUI() {
        contractImageBackgroundView.layer.cornerRadius = contractImageBackgroundView.bounds.size.height / 2
        contractImageBackgroundView.layer.borderColor = #colorLiteral(red: 0.8270336986, green: 0.6961463094, blue: 0.3867461383, alpha: 1)
        contractImageBackgroundView.layer.borderWidth = 1
        
        contractImageView.layer.cornerRadius = contractImageView.bounds.size.height / 2
        contractImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        contractImageView.layer.borderWidth = 4
    }
}
