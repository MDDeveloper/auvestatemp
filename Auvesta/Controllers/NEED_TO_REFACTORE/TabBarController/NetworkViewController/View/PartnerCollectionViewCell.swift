import UIKit
import SDWebImage

class PartnerCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var partnerImageBackgroundView: UIView!
    @IBOutlet private weak var partnerImageView: UIImageView!
    @IBOutlet private weak var partnerNameLabel: UILabel!
    
    var partnerData: AuvestaPartner? {
        didSet {
            partnerImageView.image = nil
            self.updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        updateUI()
    }
    
    private func updateData() {
        if let partnerData = partnerData {
            partnerNameLabel.text = partnerData.partnerName
            guard let url = URL(string: partnerData.partnerImageLink) else {
                self.partnerImageView.image = UIImage(named: "new user icon")
                return
            }
            partnerImageView?.sd_setImage(with: url, completed: nil)
        } else {
            partnerImageView.image = nil
            partnerNameLabel.text = nil
        }
    }
    
    private func updateUI() {
        partnerImageBackgroundView.layer.cornerRadius = partnerImageBackgroundView.bounds.size.height / 2
        partnerImageBackgroundView.layer.borderColor = #colorLiteral(red: 0.8270336986, green: 0.6961463094, blue: 0.3867461383, alpha: 1)
        partnerImageBackgroundView.layer.borderWidth = 1
        
        partnerImageView.layer.cornerRadius = partnerImageView.bounds.size.height / 2
        partnerImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        partnerImageView.layer.borderWidth = 4
    }
}
