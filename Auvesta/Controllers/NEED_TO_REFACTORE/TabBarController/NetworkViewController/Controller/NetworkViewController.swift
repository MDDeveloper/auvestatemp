import UIKit

protocol NetworkViewControllerDelegate: AnyObject {
    func didTapReferFriendButton(vc: NetworkViewController)
    func didTapAllNetworkButton(vc: NetworkViewController)
}

protocol NetworkViewControllerDataSource: AnyObject {
    func checkPartnership(vc: NetworkViewController)
    func getUserNetworkData(vc: NetworkViewController)
    func getUserData(vc: NetworkViewController) -> AuvestaUser
    func getPartnersCountForCollectionView(vc: NetworkViewController) -> Int
    func getContractsCountForTableView(vc: NetworkViewController) -> Int
    func getPartnerDataForCollectionViewCell(vc: NetworkViewController, indexPath: IndexPath) -> AuvestaPartner
    func getContractDataForTableViewCell(vc:NetworkViewController, indexPath: IndexPath) -> AuvestaContract
}

class NetworkViewController: UIViewController {
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var currentPlanStringLabel: UILabel!
    @IBOutlet private weak var commissionStringLabel: UILabel!
    @IBOutlet private weak var commissionPeriodStringLabel: UILabel!
    @IBOutlet private weak var totalPartnersStringLabel: UILabel!
    @IBOutlet private weak var activePartnersStringLabel: UILabel!
    @IBOutlet private weak var showAllReferralsStringLabel: UIButton!
    @IBOutlet private weak var collectionViewHeaderStringLabel: UILabel!
    @IBOutlet private weak var tableViewHeaderStringLabel: UILabel!
    @IBOutlet weak var newUserMessageButton: UIButton!
    
    @IBOutlet weak var notificationsButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var userInfoBackgroundView: UIView!
    @IBOutlet private weak var userIconBackgroundView: UIView!
    @IBOutlet private weak var userImageView: UIImageView!
    
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var userSubscriptionPlanLabel: UILabel!
    @IBOutlet private weak var userPartnersCountLabel: UILabel!
    
    @IBOutlet private weak var userExternalPartnerIdLabel: UILabel!
    @IBOutlet private weak var userSubmissionCountLabel: UILabel!
    @IBOutlet private weak var userContractCountLabel: UILabel!
    @IBOutlet private weak var userPendingCountLabel: UILabel!
    @IBOutlet private weak var referFriendButton: UIButton!
    
    @IBOutlet weak var partnerCollectionView: UICollectionView!
    @IBOutlet weak var contractTableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var delegate: NetworkViewControllerDelegate?
    var dataSource: NetworkViewControllerDataSource?
    var selectedPartnerSponsorID: String?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        centerPartnerCollectionView()
        updateUI()
        localize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataSource?.checkPartnership(vc: self)
        
        guard AuvestaUser.shared.isPartner else { return }
        dataSource?.getUserNetworkData(vc: self)
    }
    
    // MARK: - Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "referFriendSegue" {
            let vc = segue.destination as! ReferFriendViewController
            vc.userInfo = dataSource!.getUserData(vc: self)
        } else if segue.identifier == "partnerDetailSegue" {
            let vc = segue.destination as! PartnersViewController
            vc.partnerSponsorID = selectedPartnerSponsorID!
        } else if segue.identifier == "allPartnersSegue" {
            let vc = segue.destination as! PartnersViewController
            vc.partnerSponsorID = dataSource?.getUserData(vc: self).sponsorID
        }
    }
    
    private func localize() {
        headerTitleLabel.text = "networkVcHeader".localized()
        referFriendButton.setTitle("referFriendButton".localized(), for: .normal)
        currentPlanStringLabel.text = "networkCurrentPlan".localized()
        commissionStringLabel.text = "networkCommission".localized()
        commissionPeriodStringLabel.text = "networkCommissionPeriod".localized()
        totalPartnersStringLabel.text = "networkTotalPartners".localized()
        activePartnersStringLabel.text = "networkActivePartners".localized()
        showAllReferralsStringLabel.setTitle("networkShowAllReferralsButton".localized(), for: .normal)
        collectionViewHeaderStringLabel.text = "networkCollectionViewHeader".localized()
        tableViewHeaderStringLabel.text = "networkTableViewHeader".localized()
    }
    
    private func updateUI() {
        userInfoBackgroundView.layer.cornerRadius = 10
        userInfoBackgroundView.layer.shadowColor = UIColor.black.cgColor
        userInfoBackgroundView.layer.shadowRadius = 10
        userInfoBackgroundView.layer.shadowOpacity = 0.3
        userIconBackgroundView.layer.cornerRadius = userIconBackgroundView.bounds.size.height / 2
        userIconBackgroundView.layer.borderColor = #colorLiteral(red: 0.8270336986, green: 0.6961463094, blue: 0.3867461383, alpha: 1)
        userIconBackgroundView.layer.borderWidth = 1
        
        userImageView.layer.cornerRadius = userImageView.bounds.size.height / 2
        userImageView.layer.borderColor = #colorLiteral(red: 0.989490211, green: 0.9902383685, blue: 0.9896060824, alpha: 1)
        userImageView.layer.borderWidth = 4
        
        referFriendButton.layer.cornerRadius = referFriendButton.bounds.size.height / 2
        newUserMessageButton.setTitle("networkNewPartnerMessage".localized(), for: .normal)
    }
    
    private func centerPartnerCollectionView() {
        let insetX = CGFloat(20)
        let insetY = CGFloat(10)
        
        partnerCollectionView.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)
    }
    
    func showErrorAlert(title: String, message: String, completion: ((Bool) -> ())? = nil ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (ok) in
            completion?(true)
        }
        alertController.addAction(okAction)
    }
    
    func updateData(user: AuvestaUser) {
        userNameLabel.text = user.username
        userSubscriptionPlanLabel.text = user.subscriptionType
        userSubmissionCountLabel.text = String(format: "%.2f", user.commission)
        userExternalPartnerIdLabel.text = user.externalPartnerId
        userContractCountLabel.text = "\(user.totalContractsCount)"
        userPendingCountLabel.text = "\(user.pendingContractsCount)"
        userPartnersCountLabel.text = "\(user.partnersCount)" + "networkPartnersString".localized()
        userExternalPartnerIdLabel.isHidden = false
        guard let userImageUrl = URL(string: user.userImageLink) else { return }
        userImageView.sd_setImage(with: userImageUrl, completed: nil)
    }
    
    private func centerCellHorizontallyWhenScrolling(scrollView: UIScrollView, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let layout = self.partnerCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapAllNetworkButton(_ sender: UIButton) {
        delegate?.didTapAllNetworkButton(vc: self)
    }
    
    @IBAction func didTapReferFriendButton(_ sender: UIButton) {
        delegate?.didTapReferFriendButton(vc: self)
    }
}

extension NetworkViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowsCount = dataSource!.getContractsCountForTableView(vc: self)
        newUserMessageButton.isHidden = rowsCount == 0 ? false : true
        return rowsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contractCell", for: indexPath) as! ContractTableViewCell
        cell.contractData = dataSource?.getContractDataForTableViewCell(vc: self, indexPath: indexPath)
        cell.selectionStyle = .none
        return cell
    }
}

extension NetworkViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView == contractTableView else { return }
        let cell = tableView.cellForRow(at: indexPath) as? ContractTableViewCell
        selectedPartnerSponsorID = cell?.contractData?.contractHolderID
        performSegue(withIdentifier: "partnerDetailSegue", sender: nil)
    }
}

extension NetworkViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource!.getPartnersCountForCollectionView(vc: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "partnerCell", for: indexPath) as! PartnerCollectionViewCell
        cell.partnerData = dataSource?.getPartnerDataForCollectionViewCell(vc: self, indexPath: indexPath)
        return cell
    }
}

extension NetworkViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? PartnerCollectionViewCell
        selectedPartnerSponsorID = cell?.partnerData?.partnerSponsorID
        performSegue(withIdentifier: "partnerDetailSegue", sender: nil)
    }
}

extension NetworkViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView == partnerCollectionView else { return }
        centerCellHorizontallyWhenScrolling(scrollView: scrollView, targetContentOffset: targetContentOffset)
    }
}

