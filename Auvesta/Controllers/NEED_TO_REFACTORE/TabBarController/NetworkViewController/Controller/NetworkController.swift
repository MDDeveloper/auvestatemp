import UIKit

class NetworkController {
    private let viewController: NetworkViewController
    
    init(viewController: NetworkViewController) {
        self.viewController = viewController
        viewController.dataSource = self
        viewController.delegate = self
    }
}

extension NetworkController: NetworkViewControllerDelegate {
    func didTapAllNetworkButton(vc: NetworkViewController) {
        vc.performSegue(withIdentifier: "allPartnersSegue", sender: nil)
    }
    
    func didTapReferFriendButton(vc: NetworkViewController) {
        vc.performSegue(withIdentifier: "referFriendSegue", sender: nil)
    }
}

extension NetworkController: NetworkViewControllerDataSource {
    func checkPartnership(vc: NetworkViewController) {
        if !AuvestaUser.shared.isPartner {
            let onboardingVC = vc.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
            onboardingVC.delegate = self
            onboardingVC.viewController = vc
            onboardingVC.modalPresentationStyle = .fullScreen
            vc.navigationController?.pushViewController(onboardingVC, animated: true)
        }
    }
    
    func getUserNetworkData(vc: NetworkViewController) {
        vc.notificationsButton.startActivityIndicator()
        MainNetworkManager.getUserInfo { (status, error) in
            switch status {
            case 200:
                MainNetworkManager.getNetworkData { (status, error) in
                    vc.notificationsButton.stopActivityIndicator()
                    switch status {
                    case 200:
                        DispatchQueue.main.async {
                            vc.partnerCollectionView.reloadData()
                            vc.contractTableView.reloadData()
                            vc.updateData(user: AuvestaUser.shared)
                        }
                    case 401:
                        vc.navigationController?.popToRootViewController(animated: true)
                    default:
                        let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                        vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
                    }
                }
            case 401:
                vc.navigationController?.popToRootViewController(animated: true)
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    func getUserData(vc: NetworkViewController) -> AuvestaUser {
        return AuvestaUser.shared
    }
    
    func getContractsCountForTableView(vc: NetworkViewController) -> Int {
        return AuvestaUser.shared.contracts.count
    }
    
    func getContractDataForTableViewCell(vc: NetworkViewController, indexPath: IndexPath) -> AuvestaContract {
        return AuvestaUser.shared.contracts[indexPath.row]
    }
    
    func getPartnersCountForCollectionView(vc: NetworkViewController) -> Int {
        return AuvestaUser.shared.partners.count
    }
    
    func getPartnerDataForCollectionViewCell(vc: NetworkViewController, indexPath: IndexPath) -> AuvestaPartner {
        return AuvestaUser.shared.partners[indexPath.row]
    }
}

extension NetworkController: OnboardingViewControllerDelegate {
    func didTapActionButton(vc: OnboardingViewController, parentViewController: UIViewController?) {
        vc.performSegue(withIdentifier: "becomePartnerSegue", sender: nil)
    }
    
    func didTapCancelButton(vc: OnboardingViewController, parentViewController: UIViewController?) {
        let networkViewController = parentViewController as? NetworkViewController
        networkViewController?.tabBarController?.selectedViewController = networkViewController?.tabBarController?.viewControllers![0]
        vc.navigationController?.popViewController(animated: true)
    }
}
