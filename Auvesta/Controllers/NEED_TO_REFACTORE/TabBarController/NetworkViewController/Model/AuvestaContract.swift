import UIKit

class AuvestaContract {
    enum ContractStatus: Int {
        case notDetected = -1
        case inactive = 0
        case active = 1
        
        var stringStatus: String {
            switch self {
            case .active: return "contractStatusActive".localized()
            case .inactive: return "contractStatusInactive".localized()
            case .notDetected: return "contractStatusError".localized()
            }
        }
        
        var colorStatus: UIColor {
            switch self {
            case .active: return #colorLiteral(red: 0, green: 0.6515724659, blue: 0.1275553703, alpha: 1)
            case .inactive: return #colorLiteral(red: 0.7919357419, green: 0.03562327102, blue: 0.05330144614, alpha: 1)
            case .notDetected: return #colorLiteral(red: 0.7919357419, green: 0.03562327102, blue: 0.05330144614, alpha: 1)
            }
        }
    }
    
    var contractStatus: ContractStatus
    var contractHolderName: String
    var contractHolderImageLink: String
    var contractAmount: String
    var contractHolderID: String
    var externalPartnerId: String

    static func getArray(from jsonArray: Any) -> [AuvestaContract] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [AuvestaContract]() }
        return jsonArray.compactMap{ AuvestaContract(jsonArray: $0) }
    }
    
    init(jsonArray: [String:Any]) {
        let fullName = jsonArray["userName"] as? String
        let status = jsonArray["status"] as? Int
        let amount = jsonArray["amount"] as? Double
        let currency = jsonArray["currency"] as? Int
        let userPhotoLink = jsonArray["userPhoto"] as? String
        let id = jsonArray["id"] as? Int
        let externalPartnerId = jsonArray["externalPartnerId"] as? Int
        
        self.contractStatus = ContractStatus(rawValue: status ?? -1)!
        self.contractHolderName = fullName ?? "error while loading name"
        self.contractAmount = "\(String(format: "%.2f", amount ?? 0.00))\(AuvestaCurrency(rawValue: currency ?? -1)!.stringIcon)"
        self.contractHolderImageLink = userPhotoLink ?? "error while loading link"
        self.externalPartnerId = String(externalPartnerId ?? 0)
        
        guard let holderID = id else {
            self.contractHolderID = "error in ID"
            return
        }
        self.contractHolderID = "\(holderID)"
    }
}
