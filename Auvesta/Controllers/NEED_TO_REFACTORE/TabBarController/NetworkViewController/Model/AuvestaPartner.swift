import UIKit

class AuvestaPartner {
    
    var parentSponsorID: String = ""
    var partnerImageLink: String = ""
    var partnerName: String = ""
    var partnerSponsorID: String = ""
    var partnerEmail: String = ""
    var externalPartnerId: String = ""
    var partners = [AuvestaPartner]()
    
    static func getArray(from jsonArray: Any) -> [AuvestaPartner] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [AuvestaPartner]() }
        return jsonArray.compactMap{ AuvestaPartner(directProfilesJsonArray: $0) }
    }
    
    init(directProfilesJsonArray: [String:Any]) {
        guard let user = directProfilesJsonArray["user"] as? [String:Any] else { return }
        let parentSponsorID = directProfilesJsonArray["parentSponsorID"] as? Int
        let email = user["email"] as? String
        let fullName = user["fullName"] as? String
        let partnerSponsorID = user["referrerUid"] as? Int
        let partnerImageLink = directProfilesJsonArray["profilePhoto"] as? String
        let externalID = directProfilesJsonArray["externalPartnerId"] as? Int
        
        self.externalPartnerId = String(externalID ?? 0)
        self.partnerName = fullName ?? "name loading error"
        //        self.parentSponsorID = String(parentSponsorID ?? 0) ?? "parentSponsorID loading error"
        self.partnerEmail = email ?? "email loading error"
        //        self.partnerSponsorID = partnerSponsorID ?? "partnerSponsorID loading error"
        self.partnerImageLink = partnerImageLink ?? "image link loading error"
        
        guard
            let parent = parentSponsorID,
            let partner = partnerSponsorID else { return }
        self.parentSponsorID = String(parent)
        self.partnerSponsorID = String(partner)
    }
    
    init(jsonArray: [String:Any]) {
        guard
            let userProfile = jsonArray["userProfile"] as? [String:Any],
            let user = userProfile["user"] as? [String:Any] else { return }
        let externalPartnerId = userProfile["externalPartnerId"] as? Int
        let parentSponsorID = userProfile["parentSponsorID"] as? Int
        let email = user["email"] as? String
        let fullName = user["fullName"] as? String
        let partnerSponsorID = user["referrerUid"] as? Int
        let partnerImageLink = userProfile["profilePhoto"] as? String
        
        self.partnerName = fullName ?? "name loading error"
        self.partnerEmail = email ?? "email loading error"
        self.partnerImageLink = partnerImageLink ?? "image link loading error"
        self.externalPartnerId = String(externalPartnerId ?? 0)
        self.parentSponsorID = String(parentSponsorID ?? 0)
        
        guard let partnerID = partnerSponsorID else {
            self.partnerSponsorID = "error in referrerUID"
            return
        }
        self.partnerSponsorID = "\(partnerID)"
    }
}
