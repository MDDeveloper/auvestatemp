import UIKit

class NetworkNavigationController: UINavigationController {
    private var networkController: NetworkController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareViewControllers()
    }
    
    private func prepareViewControllers() {
        self.viewControllers.forEach({ (vc) in
            if let networkViewController = vc as? NetworkViewController {
                networkViewController.title = "networkVcHeader".localized()
                networkController = NetworkController(viewController: networkViewController)
            }
        })
    }
}
