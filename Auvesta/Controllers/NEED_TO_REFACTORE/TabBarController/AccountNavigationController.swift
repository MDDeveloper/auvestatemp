import UIKit

class AccountNavigationController: UINavigationController {
    private var accountController: AccountController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareViewControllers()
    }
    
    private func prepareViewControllers() {
        self.viewControllers.forEach({ (vc) in
            if let accountViewController = vc as? AccountViewController {
                accountController = AccountController(viewController: accountViewController)
            }
        })
    }
}
