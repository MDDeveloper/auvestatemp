import UIKit

protocol PartnershipTermsViewControllerDelegate {
    func didTapAcceptButton(vc: PartnershipTermsViewController, newsletter: Bool)
}

class PartnershipTermsViewController: UIViewController {
    
    // MARK: IBOutlets and varibalbes:
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var acceptButton: ButtonWithActivityIndicator!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var partnershipTableView: UITableView!
    @IBOutlet weak var newsletterCheckboxButton: UIButton!
    @IBOutlet weak var newsletterLabel: UILabel!
    
    private var termsData = [PartnershipTerms]()
    var viewController: UIViewController?
    var delegate: PartnershipTermsViewControllerDelegate?
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        localization()
        downloadTermsData()
    }
    
    // MARK: Methods
    
    private func downloadTermsData() {
        RegistrationNetworkManager.getPartnerTerms { (status, terms, error) in
            switch status {
            case 200:
                var sortedTerms = terms!
                let element = sortedTerms.remove(at: 4)
                sortedTerms.insert(element, at: 5)
                self.termsData = sortedTerms
                DispatchQueue.main.async {
                    self.partnershipTableView.reloadData()
                }
            default:
                self.addAlert(title: "", message: error)
            }
        }
    }
    
    private func setupUI() {
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.shadowColor = UIColor.black.cgColor
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundView.layer.shadowOpacity = 0.4
        backgroundView.layer.shadowRadius = 4
        
        acceptButton.layer.cornerRadius = 5
        
        newsletterCheckboxButton.isEnabled = true
        
        newsletterLabel.text = StringResources.shared.partnerNewsletterText
    }
    
    private func localization() {
        acceptButton.setTitle("agreeButton".localized(), for: .normal)
        cancelButton.setTitle("disagreeButton".localized(), for: .normal)
        titleLabel.text = "partnershipVcHeader".localized()
    }
    
    @IBAction func didTapAcceptButton(_ sender: UIButton) {
        delegate?.didTapAcceptButton(vc: self, newsletter: newsletterCheckboxButton.isSelected)
    }
    
    @IBAction func didTapCheckbox(_ sender: UIButton) {
        sender.isSelected.toggle()
        acceptButton.isEnabled = sender.isSelected

        let buttonColor = acceptButton.isEnabled ? #colorLiteral(red: 0.8744713068, green: 0.7059164643, blue: 0.3019405901, alpha: 1) : #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        acceptButton.backgroundColor = buttonColor
    }
    
    @IBAction func didTapNewsletterCheckbox(_ sender: UIButton) {
        sender.isSelected.toggle()
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension PartnershipTermsViewController: UITableViewDelegate {
    
}

extension PartnershipTermsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return termsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = termsData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "reusableCell", for: indexPath) as! PartnershipTermsTableViewCell
        cell.termsData = cellData
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
