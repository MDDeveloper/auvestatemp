import UIKit

class PartnershipTermsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var termsDataTextView: UITextView!
    
    var termsData: PartnershipTerms? {
        didSet {
            self.updateData()
        }
    }
    
    private func updateData() {
        guard let terms = termsData else { return }
        switch terms.type {
        case .header, .line:
            let attributedString = NSAttributedString.init(string: terms.text)
            termsDataTextView.attributedText = attributedString
        case .link:
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .left
            
            let attributedString = NSMutableAttributedString(string: terms.text, attributes: [NSAttributedString.Key.paragraphStyle: paragraph])
            attributedString.addAttribute(.link, value: terms.link, range: NSRange(location: 0, length: terms.text.count))
            termsDataTextView.attributedText = attributedString
        }
    }
}
