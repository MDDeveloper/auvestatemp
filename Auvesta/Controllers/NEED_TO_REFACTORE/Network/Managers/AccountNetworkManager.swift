import UIKit
import Alamofire

class AccountNetworkManager {
    typealias Completion = (_ status: Int, _ error: String)->()
    
    private static let headers = NetworkService.headers
    
    static func changeUsername(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changeUsername, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changePsn(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changePsn, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changePassword(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changePassword, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changeNewsletter(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changeNewsletter, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changeUserImage(image: UIImage, completion: @escaping Completion) {
        let resizedImage = image.resizeImage(CGSize(width: 600, height: 600))
        guard let imageData = resizedImage.jpegData(compressionQuality: 0) else { return }
        
        AF.upload(multipartFormData: { $0.append(imageData, withName: "file", fileName: "userImage.jpg",mimeType: "image/jpeg")},
                  to: NetworkService.url(.changeUserImage),
                  headers: headers)
            .response { response in
                guard let status = response.response?.statusCode else { return }
                switch status {
                case 200:
                    completion(status, "")
                default:
                    guard let error = NetworkService.captureErrorFromData(data: response.data) else {
                        completion(status, "somethingWentWrong".localized())
                        return
                }
                completion(status, error)
            }
        }
    }
    
    
}
