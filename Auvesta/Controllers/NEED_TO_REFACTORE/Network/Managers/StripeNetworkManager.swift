import Foundation
import Stripe

class StripeNetworkManager {

    typealias StripeKeyCompletion = (_ status: Int, _ stripeKey: StripeKey?, _ error: String)->()
    typealias StripeIntentCompletion = (_ status: Int, _ stripeIntent: StripeIntent?, _ error: String)->()
    typealias StripeCheckCompletion = (_ status: Int, _ stripeCheck: StripeCheck?, _ error: String)->()
    typealias StripeIntentCancel = (_ status: Int, _ error: String)->()
    
    static func getStripeIntent(body: [String: Any], completion: @escaping StripeIntentCompletion) {
        NetworkService.request(.getStripeIntent, body: body) { (status, error, value) in
            switch status {
            case 200:
                guard let info = value as? [String: Any] else { return }
                let stripeIntent = StripeIntent(value: info)
                completion(status, stripeIntent, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func getStripeCheck(body: [String: Any], completion: @escaping StripeCheckCompletion) {
        NetworkService.request(.getStripeCheck, body: body) { (status, error, value) in
            switch status {
            case 200:
                guard let info = value as? [String: Any] else { return }
                let stripeCheck = StripeCheck(value: info)
                completion(status, stripeCheck, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func getStripeKey(completion: @escaping StripeKeyCompletion) {
        NetworkService.request(.getStripeKey, auth: true) { (status, error, value) in
            switch status {
            case 200:
                guard let value = value else { return }
                let stripeKey = StripeKey(value: value)
                completion(status, stripeKey, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func sendStripeCancel(body: [String: Any], completion: @escaping StripeIntentCancel) {
        NetworkService.request(.stripeIntentCancel, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
}
