//
//  RegistrationNetworkManager.swift
//  Auvesta
//
//  Created by meowsawyer on 14/11/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation
import Alamofire

class RegistrationNetworkManager {
    typealias Completion = (_ status: Int, _ error: String)->()
    typealias PhoneRequestCompletion = (_ status: Int, _ error: String, _ skip: Bool)->()
    typealias SubscriptionsCompletion = (_ status: Int, _ subscriptions: [AuvestaSubscription]?, _ error: String)->()
    typealias PartnershipTermsCompletion = (_ status: Int, _ terms: [PartnershipTerms]?, _ error: String)->()
    typealias UspDetailsCompletion = (_ status: Int, _ terms: [PartnershipUsp]?, _ error: String)->()
    typealias CustomerTermsCompletion = (_ status: Int, _ terms: CustomerTerms?, _ error: String)->()
    typealias CountriesDetailsCompletion = (_ status: Int, _ countries: [Country]?, _ error: String)->()
    typealias ArtIdDetailsCompletion = (_ status: Int, _ countries: [CompanyArtID]?, _ error: String)->()
    typealias IpDetailsCompletion = (_ status: Int, _ ipDetails: IpDetails?, _ error: String)->()
    
    private static let headers = NetworkService.headers
    
    // MARK: Customer Registration
    
    static func checkSponsorId(sponsorID: String, completion: @escaping Completion) {
        NetworkService.request(.checkSponsorID(sponsorID)) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func phoneNumberRequest(body: [String: String], completion: @escaping PhoneRequestCompletion) {
        NetworkService.request(.phoneRequest, body: body) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let json = value as? [String: Any],
                    let skip = json["skip"] as? Bool else {
                        completion(status, "", false)
                        return
                }
                completion(status, "", skip)
            default:
                completion(status, error, false)
            }
        }
    }
    
    static func phoneNumberVerify(body: [String: String], completion: @escaping Completion) {
        NetworkService.request(.phoneVerify, body: body) { (status, error, _) in
            completion(status,error)
        }
    }
    
    static func uploadUserInfo(sponsorID: String, body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.registerUser(sponsorID), body: body) { (status, error, value) in
            switch status {
            case 200:
                guard let value = value else { return }
                AuvestaUser.shared.updateToken(value: value)
                completion(status, error)
            default:
                completion(status, error)
            }
        }
    }
    
    static func emailConfirmCheck(completion: @escaping Completion) {
        NetworkService.request(.emailCheck) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func resendEmail(completion: @escaping Completion) {
        NetworkService.request(.emailResend) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func getSubscriptions(completion: @escaping SubscriptionsCompletion) {
        NetworkService.request(.getSubscriptions) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonArray = value as? [String: Any],
                    let subscriptionsJson = jsonArray["subscriptions"] else { return }
                let subscriptions = AuvestaSubscription.getArray(from: subscriptionsJson)
                completion(status, subscriptions, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func selectSubscription(id: String, completion: @escaping Completion) {
        NetworkService.request(.selectSubscription(id)) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func getCustomerTerms(completion: @escaping CustomerTermsCompletion) {
          NetworkService.request(.getCustomerTerms) { (status, error, value) in
              switch status {
              case 200:
                  guard let value = value else { return }
                  let terms = CustomerTerms(json: value)
                  completion(status, terms, error)
              default:
                  completion(status, nil, error)
              }
          }
      }
    
    static func acceptCustomerTerms(completion: @escaping Completion) {
        NetworkService.request(.acceptCustomerTerms) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func uploadBankDetails(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.uploadBankDetails, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func uploadUserPassportImage(imageData: Data, completion: @escaping Completion) {
        upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "file", fileName: "passportImage.jpg", mimeType: "image/jpeg")
        }, to: NetworkService.url(.uploadPassport), headers: headers) { (encodingCompletion) in
            switch encodingCompletion {
            case .success(request: let uploadRequest, streamingFromDisk: _, streamFileURL: _):
                uploadRequest
                    .validate()
                    .responseJSON(completionHandler: { (responseJson) in
                        guard let statusCode = responseJson.response?.statusCode else { return }
                        
                        switch statusCode {
                        case 200:
                            completion(statusCode, "")
                        default:
                            guard let errorString = NetworkService.captureErrorFromData(data: responseJson.data) else {
                                completion(statusCode, "Something went wrong, please try again later.")
                                return
                            }
                            completion(statusCode, "\(errorString)")
                        }
                    })
            case .failure(let error):
                print(error.localizedDescription, "------> ERROR in encoding")
            }
        }
    }
    
    // MARK: Partner registration
    
    static func getPartnerRegistrationDetails(completion: @escaping Completion) {
        NetworkService.request(.getPartnershipDetails) { (status, error, value) in
            switch status {
            case 200:
                guard let value = value else { return }
                AuvestaUser.shared.updatePartnerRegistrationInfo(value: value)
                NetworkService.downloadImage(url: AuvestaUser.shared.passportImageLink, completion: { (image) in
                    AuvestaUser.shared.passportPhoto = image
                    completion(status, error)
                })
            default:
                completion(status, error)
            }
        }
    }
    
    static func getPartnerTerms(completion: @escaping PartnershipTermsCompletion) {
        NetworkService.request(.getPartnershipDetails) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let json = value as? [String: Any],
                    let jsonTerms = json["terms"] else { return }
                let partnershipTerms = PartnershipTerms.getArray(from: jsonTerms)
                completion(status, partnershipTerms, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func getUspDetails(completion: @escaping UspDetailsCompletion) {
        NetworkService.request(.getUspDetails) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonArray = value as? [String: Any],
                    let translations = jsonArray["translations"] else { return }
                let usp = PartnershipUsp.getArray(from: translations)
                completion(status, usp, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func acceptPartnerTerms(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.acceptPartnershipTerms, body: body) { (status, error, _) in
            switch status {
            case 200:
                AuvestaUser.shared.isPartner = true
                completion(status, error)
            default:
                completion(status, error)
            }
        }
    }
    
    // MARK: Additional requests
    
    static func getCountriesDetails(completion: @escaping CountriesDetailsCompletion) {
        NetworkService.request(.getCountriesDetails) { (status, error, value) in
            switch status {
            case 200:
                guard let jsonArray = value as? [Any] else { return }
                let countries = Country.getArray(from: jsonArray)
                completion(status, countries, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func getCompanyArtIdData(completion: @escaping ArtIdDetailsCompletion) {
        NetworkService.request(.getArtIdDetails) { (status, error, value) in
            switch status {
            case 200:
                guard let jsonArray = value as? [Any] else { return }
                let companyArtIDs = CompanyArtID.getArray(from: jsonArray)
                completion(status, companyArtIDs, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    static func getIpInfo(completion: @escaping IpDetailsCompletion) {
        NetworkService.request(.getIpDetails, auth: false) { (status, error, value) in
            switch status {
            case 200:
                guard let value = value else { return }
                let ipDetails = IpDetails(value: value)
                completion(status, ipDetails, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    
}

