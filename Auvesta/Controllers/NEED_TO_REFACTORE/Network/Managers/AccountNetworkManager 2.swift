//
//  AccountNetworkManager.swift
//  Auvesta
//
//  Created by meowsawyer on 18/11/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import UIKit
import Alamofire

class AccountNetworkManager {
    typealias Completion = (_ status: Int, _ error: String)->()
    
    private static let headers = NetworkService.headers
    
    static func changeUsername(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changeUsername, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changePsn(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changePsn, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changePassword(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changePassword, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changeNewsletter(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changeNewsletter, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func changeUserImage(image: UIImage, completion: @escaping Completion) {
        let resizedImage = image.resizeImage(CGSize(width: 600, height: 600))
        guard let imageData = resizedImage.jpegData(compressionQuality: 0) else { return }
        
        upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "file", fileName: "userImage.jpg", mimeType: "image/jpeg")
        }, to: NetworkService.url(.changeUserImage), headers: headers) { (encodingCompletion) in
            
            switch encodingCompletion {
            case .success(request: let uploadRequest, streamingFromDisk: _, streamFileURL: _):
                uploadRequest.responseJSON(completionHandler: { (responseJson) in
                    
                    guard let status = responseJson.response?.statusCode else { return }
                    switch status {
                    case 200:
                        completion(status, "")
                    default:
                        guard let error = NetworkService.captureErrorFromData(data: responseJson.data) else {
                            completion(status, "Something went wrong, please try again later.")
                            return
                        }
                        completion(status, error)
                    }
                })
            case .failure(let error):
                print(error.localizedDescription, "-----------> ERROR in encoding")
            }
        }
    }
    
    
}
