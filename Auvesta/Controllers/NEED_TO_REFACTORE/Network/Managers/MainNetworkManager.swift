import UIKit
import Alamofire

class MainNetworkManager {
    typealias Completion = (_ status: Int, _ error: String)->()
    typealias ReferralCompletion = (_ status: Int, _ partner: AuvestaPartner?, _ error: String)->()
    typealias DashboardCompletion = (_ status: Int, _ portfolios: [AuvestaPortfolio]?, _ error: String)->()
    typealias PoolsCompletion = (_ status: Int, _ pools: [AuvestaPool]?, _ incentives: [AuvestaIncentive]?, _ error: String)->()

    // MARK: Network
    
    static func getUserInfo(completion: @escaping Completion) {
        NetworkService.request(.getUserInfo) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonArray = value as? [String:Any],
                    let user = jsonArray["user"] else { return }
                AuvestaUser.shared.updateUserProfile(value: jsonArray)
                AuvestaUser.shared.updateUserProfileDetails(value: user)
                completion(status, error)
            default:
                completion(status, error)
            }
        }
    }
    
    static func getNetworkData(completion: @escaping Completion) {
        NetworkService.request(.getNetwork) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonArray = value as? [String:Any],
                    let contracts = jsonArray["contracts"],
                    let directProfiles = jsonArray["directProfiles"] else { return }
                AuvestaUser.shared.updateUserNetworkInfo(value: jsonArray)
                AuvestaUser.shared.partners = AuvestaPartner.getArray(from: directProfiles)
                AuvestaUser.shared.contracts = AuvestaContract.getArray(from: contracts)
                completion(status, error)
            default:
                completion(status, error)
            }
        }
    }
    
    static func getReferralLinkText(completion: @escaping (_ status: Int, _ text: String, _ error: String)->()) {
        NetworkService.request(.getNetwork) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonArray = value as? [String:Any],
                    let referralText = jsonArray["refText"] as? String else { return }
                completion(status, referralText ,error)
            default:
                completion(status, "", error)
            }
        }
    }
    
    static func getReferralInfo(referralID: String, completion: @escaping ReferralCompletion) {
        NetworkService.request(.getReferralInfo(referralID)) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonArray = value as? [String:Any],
                    let directProfiles = jsonArray["directProfiles"] else { return }
                let partner = AuvestaPartner(jsonArray: jsonArray)
                partner.partners = AuvestaPartner.getArray(from: directProfiles)
                completion(status, partner, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    // MARK: Pools
    
    static func getPools(completion: @escaping PoolsCompletion) {
        NetworkService.request(.getPools) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonArray = value as? [String:Any],
                    let poolsArray = jsonArray["pools"],
                    let incentivesArray = jsonArray["incentives"] else { return }
                let pools = AuvestaPool.getArray(from: poolsArray)
                let incentives = AuvestaIncentive.getArray(from: incentivesArray)
                completion(status, pools, incentives, error)
            default:
                completion(status, nil, nil, error)
            }
        }
    }
    
    // MARK: Dashboard
    
    static func getDashboard(completion: @escaping DashboardCompletion) {
        NetworkService.request(.getDashboard) { (status, error, value) in
            switch status {
            case 200:
                guard let jsonArray = value  else { return }
                let portfolios = AuvestaPortfolio.getArray(from: jsonArray)
                completion(status, portfolios, error)
            default:
                completion(status, nil, error)
            }
        }
    }
    
    
}

