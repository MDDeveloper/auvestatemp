//
//  LoginNetworkManager.swift
//  Auvesta
//
//  Created by meowsawyer on 14/11/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation

class LoginNetworkManager {
    typealias Completion = (_ status: Int, _ error: String)->()
    
    // MARK: Login
    
    static func login(body: [String: String], completion: @escaping Completion) {
        NetworkService.request(.login, body: body) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonObject = value as? [String:Any],
                    let userProfile = jsonObject["userProfile"] as? [String:Any],
                    let user = userProfile["user"] else { return }
                AuvestaUser.shared.updateToken(value: jsonObject)
                AuvestaUser.shared.updateUserProfile(value: userProfile)
                AuvestaUser.shared.updateUserProfileDetails(value: user)
                completion(status, error)
            case 409:
                completion(status, "Enter PartnerID error")
            default:
                completion(status, error)
            }
        }
    }
    
    static func loginWithPartnerID(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.login, body: body) { (status, error, value) in
            switch status {
            case 200:
                guard
                    let jsonObject = value as? [String:Any],
                    let userProfile = jsonObject["userProfile"] as? [String:Any],
                    let user = userProfile["user"] else { return }
                AuvestaUser.shared.updateToken(value: jsonObject)
                AuvestaUser.shared.updateUserProfile(value: userProfile)
                AuvestaUser.shared.updateUserProfileDetails(value: user)
                completion(status, error)
            default:
                completion(status, error)
            }
        }
    }
    
    // MARK: Forgot Pasword Flow
    
    static func getOneTimeCode(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.getOneTimePassword, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
    static func getOneTimeToken(_ oneTimeCode: String, completion: @escaping Completion) {
        NetworkService.request(.getOneTimeToken(oneTimeCode)) { (status, error, value) in
            switch status {
            case 200:
                guard let jsonArray = value as? [String:Any] else { return }
                AuvestaUser.shared.updateToken(value: jsonArray)
                completion(status, error)
            default:
                completion(status, error)
            }
        }
    }
    
    static func changeForgottenPassword(body: [String: Any], completion: @escaping Completion) {
        NetworkService.request(.changeForgottenPassword, body: body) { (status, error, _) in
            completion(status, error)
        }
    }
    
}
