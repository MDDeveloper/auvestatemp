//
//  ApiRouter.swift
//  Auvesta
//
//  Created by meowsawyer on 14/11/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRouter {
    
    // MARK: Helpers
    case getIpDetails
    
    // MARK: Login
    case login
    case getOneTimePassword
    case getOneTimeToken(_ oneTimeCode: String)
    case changeForgottenPassword
    
    // MARK: Registration
    case checkSponsorID(_ id: String)
    case phoneRequest
    case phoneVerify
    case registerUser(_ id: String)
    case emailCheck
    case emailResend
    case getSubscriptions
    case selectSubscription(_ id: String)
    case uploadBankDetails
    case uploadPassport
    case getPartnershipDetails
    case getPartnershipTerms
    case acceptPartnershipTerms
    case getUspDetails
    case getCustomerTerms
    case acceptCustomerTerms
    case getCountriesDetails
    case getArtIdDetails
    
    // MARK: Account
    case changeUsername
    case changePsn
    case changePassword
    case changeNewsletter
    case changeUserImage
    
    // MARK: Main
    case getUserInfo
    case getReferralInfo(_ id: String)
    case getDashboard
    case getPools
    case getNetwork
    
    var scheme: String {
        switch self {
        case .getIpDetails:
            return "http"
        default:
            return "https"
        }
    }
    
    var host: String {
        switch self {
        case .getIpDetails:
            return "ip-api.com"
        default:
            #if DEBUG
            return "app.auvesta.com"
//            return "auvesta.cpi.ninja"
            #else
            //            return "auvesta.cpi.ninja"
            return "app.auvesta.com"
            #endif
        }
    }
    
    var path: String {
        let api = "/api/v1/"
        
        switch self {
        case .getIpDetails:
            return "/json"
        case .login:
            return api + "login"
        case .getOneTimePassword:
            return api + "forgot_pass"
        case .getOneTimeToken(let code):
            return api + "forgot_auth/" + code
        case .changeForgottenPassword:
            return api + "change_forgotten"
        case .checkSponsorID(let id):
            return api + "check/" + id
        case .phoneRequest:
            return api + "phone/request"
        case .phoneVerify:
            return api + "phone/verify"
        case .registerUser(let id):
            return api + "register/" + id
        case .emailCheck:
            return api + "email_confirm_check"
        case .emailResend:
            return api + "repeat_email_confirm"
        case .getSubscriptions:
            return api + "subscription"
        case .selectSubscription(let id):
            return api + "subscribe/" + id
        case .acceptCustomerTerms:
            return api + "accept_terms/0"
        case .uploadBankDetails:
            return api + "iban"
        case .uploadPassport:
            return api + "add_passport"
        case .getPartnershipDetails:
            return api + "partnership"
        case .getPartnershipTerms:
            return api + "partnership"
        case .getUspDetails:
            return api + "usp"
        case .getCustomerTerms:
            return api + "terms"
        case .acceptPartnershipTerms:
            return api + "partnership_confirm"
        case .getCountriesDetails:
            return api + "countries"
        case .getArtIdDetails:
            return api + "company_art_list"
        case .changeUsername:
            return api + "change_name"
        case .changePsn:
            return api + "change_psn"
        case .changePassword:
            return api + "change_password"
        case .changeNewsletter:
            return api + "mailing_settings"
        case .changeUserImage:
            return api + "add_photo"
        case .getUserInfo:
            return api + "whoami"
        case .getReferralInfo(let id):
            return api + "network/" + id
        case .getDashboard:
            return api + "get_dashboard"
        case .getPools:
            return api + "get_pools"
        case .getNetwork:
            return api + "network"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getIpDetails:
            return .get
        case .login:
            return .post
        case .getOneTimePassword:
            return .post
        case .getOneTimeToken(_):
            return .get
        case .changeForgottenPassword:
            return .post
        case .checkSponsorID(_):
            return .get
        case .phoneRequest:
            return .post
        case .phoneVerify:
            return .post
        case .registerUser(_):
            return .post
        case .emailCheck:
            return .head
        case .emailResend:
            return .get
        case .getSubscriptions:
            return .get
        case .selectSubscription(_):
            return .post
        case .acceptCustomerTerms:
            return .post
        case .uploadBankDetails:
            return .post
        case .uploadPassport:
            return .post
        case .getPartnershipDetails:
            return .get
        case .getPartnershipTerms:
            return .get
        case .getUspDetails:
            return .get
        case .getCustomerTerms:
            return .get
        case .acceptPartnershipTerms:
            return .post
        case .getCountriesDetails:
            return .get
        case .getArtIdDetails:
            return .get
        case .changeUsername:
            return .post
        case .changePsn:
            return .post
        case .changePassword:
            return .post
        case .changeNewsletter:
            return .post
        case .changeUserImage:
            return .post
        case .getUserInfo:
            return .get
        case .getReferralInfo(_):
            return .get
        case .getDashboard:
            return .get
        case .getPools:
            return .get
        case .getNetwork:
            return .get
        }
    }
}
