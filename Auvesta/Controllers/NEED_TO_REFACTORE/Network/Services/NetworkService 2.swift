//
//  NetworkService.swift
//  Auvesta
//
//  Created by meowsawyer on 14/11/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation
import Alamofire

class NetworkService {
    typealias Completion = (_ status: Int, _ error: String, _ value: Any?)->()
    
    static var headers: [String : String] {
        return ["Authorization":"\(AuvestaUser.shared.authorizationType) \(AuvestaUser.shared.token)"]
    }
    
    class func url(_ router: ApiRouter) -> URL {
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path
        guard let url = components.url else { fatalError() }
        return url
    }
    
    class func request(_ router: ApiRouter, body: [String: Any]? = nil, auth: Bool = true, completion: @escaping Completion) {
        Alamofire.request(url(router), method: router.method, parameters: body, encoding: JSONEncoding.default, headers: auth ? headers : nil)
            .validate()
            .responseJSON { (responseJson) in
                guard let statusCode = responseJson.response?.statusCode else { return }
                switch statusCode {
                case 200:
                    completion(statusCode, "", responseJson.value)
                default:
                    guard let errorString = captureErrorFromData(data: responseJson.data) else {
                        completion(statusCode, "Something went wrong, please try again later.", nil)
                        return
                    }
                    completion(statusCode, "\(errorString)", nil)
                }
        }
    }
    
    class func downloadImage(url: String, completion: @escaping (_ image: UIImage?)->()) {
        guard let url = URL(string: url) else {
            completion(UIImage())
            return
        }
        Alamofire.request(url).response { (response) in
            guard
                let data = response.data,
                let image = UIImage(data: data) else {
                    completion(UIImage())
                    return
            }
            completion(image)
        }
    }
    
    private class func convertJsonToDictionary(jsonString: String) -> [String:Any]? {
        if let data = jsonString.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func captureErrorFromData(data: Data?) -> String? {
        guard
            let data = data,
            let jsonData = String.init(data: data, encoding: .utf8),
            let jsonDict = convertJsonToDictionary(jsonString: jsonData),
            let errorString = jsonDict["error"] as? String else { return nil }
        return errorString
    }
    
    
}
