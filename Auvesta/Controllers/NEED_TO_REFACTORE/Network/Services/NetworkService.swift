import Foundation
import Alamofire

class NetworkService {
    typealias Completion = (_ status: Int, _ error: String, _ value: Any?)->()
    
    static var headers: HTTPHeaders {
        return ["Authorization":"\(AuvestaUser.shared.authorizationType) \(AuvestaUser.shared.token)"]
    }
    
    class func url(_ router: ApiRouter) -> URL {
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path
        guard let url = components.url else { fatalError() }
        return url
    }
    
    // MARK: - Added debug print with goal of better logging networking requests
    
    class func request(_ router: ApiRouter, body: [String: Any]? = nil, auth: Bool = true, completion: @escaping Completion) {
        AF.request(url(router), method: router.method, parameters: body, encoding: JSONEncoding.default, headers: auth ? headers : nil)
            .validate()
            .responseJSON { (responseJson) in
                debugPrint("LOG: REQUEST \(responseJson.request?.url) STATUS CODE: \(responseJson.response?.statusCode)")
                guard let statusCode = responseJson.response?.statusCode else { return }
                switch statusCode {
                case 200:
                    completion(statusCode, "", responseJson.value)
//                case 401:
//                    refreshToken(router, body: body, auth: auth, completion: completion)
//                case 403:
//                    refreshToken(router, body: body, auth: auth, completion: completion)
                default:
                    guard let errorString = captureErrorFromData(data: responseJson.data) else {
                        completion(statusCode, "somethingWentWrong".localized(), nil)
                        return
                    }
                    completion(statusCode, "\(errorString)", nil)
                }
        }
    }
    
    class func downloadImage(url: String, completion: @escaping (_ image: UIImage?)->()) {
        guard let url = URL(string: url) else {
            completion(UIImage())
            return
        }
        AF.request(url).response { (response) in
            guard
                let data = response.data,
                let image = UIImage(data: data) else {
                    completion(UIImage())
                    return
            }
            completion(image)
        }
    }
    
    private class func convertJsonToDictionary(jsonString: String) -> [String:Any]? {
        if let data = jsonString.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    //MARK: - Refresh token temporary
    
    private class func refreshToken(_ router: ApiRouter, body: [String: Any]? = nil, auth: Bool = true, completion: @escaping Completion) {
        
        guard let authDictionary = AuthManager.shared.getLoginCredentials() as? [String: String] else { return }
        let password = authDictionary["password"]
        let email = authDictionary["email"]
        let body = ["email": email, "password": password] as Parameters
        
         self.request(.login, body: body) { (status, error, value) in
                   switch status {
                   case 200:
                    guard let jsonObject = value as? [String:Any] else { return }
                    AuvestaUser.shared.updateToken(value: jsonObject)
                    request(router, body: body, auth: auth, completion: completion)
                   default:
                       fatalError("Didn't update token")
                   }
               }
    }
    
    class func captureErrorFromData(data: Data?) -> String? {
        guard
            let data = data,
            let jsonData = String.init(data: data, encoding: .utf8),
            let jsonDict = convertJsonToDictionary(jsonString: jsonData),
            let errorString = jsonDict["error"] as? String else { return nil }
        return errorString
    }
    
}
