import UIKit
import Firebase
import UserNotifications
import Stripe

/*
----- Backend -----
 
 1. Complete logics of email code confirmation ( code, that we receive from email should be match with code we send at step "email confirm"
 1.1 I suggest for development configuration send any 6-digit number
 1.2 for production configuration code should match else 404 ( ??? as they said before )
 2. Check registration status at step "Subscription plan" instead "Download passport"
 
 ----- Summary -----
 
 1. Assets for new stepper
 2. Localized strings for email confirmation flow
 
 ----- Local -----
 
 1. Continue refactore project ( create separate storyboards , fix controllers classes )
 2. Refactore project structure
 3. Delete old comments
 4. Try to find out some documentation
 5. Try to understand how it should work? ( Same as android app )
 6. Deal with warnings / Migrate to swift 5
 */

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    weak var screen : UIView? = nil
    var window: UIWindow?
//    let gcmMessageIDKey = "gcm.message_id"
    private var notificationManager = NotificationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        notificationManager.setDelegates()
        notificationManager.requestAuthorization()
        notificationManager.setNotificationsCategories()
        application.registerForRemoteNotifications()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = RootViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        notificationManager.didRecievedSilentNotification(application, didReceiveRemoteNotification: userInfo)
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//
//
//        print(userInfo)
    }
    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//        // TODO: If necessary send token to application server.
//        // Note: This callback is fired at each app startup and whenever a new token is generated.
//    }
//
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("Received data message: \(remoteMessage.appData)")
//        print("--------------MESSAGE RECIVE___________")
////        let body = "remote message"
////        let title = "remote MEssage"
////        CoreDataManager.shared.createNotification(of: AUNotification.self, body: body, title: title)
//
//    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

  

    func applicationWillEnterForeground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
    }

  

}

//extension AppDelegate: UNUserNotificationCenterDelegate {
//    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
//        print("OPEN SETIINGS______----------")
//
//    }
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print("RECIEVED!!!!!!")
//        // когда получен ответ на нотификацию
//        let body = response.notification.request.content.body
//        let title = response.notification.request.content.title
//        CoreDataManager.shared.createNotification(of: AUNotification.self, body: body, title: title)
//    }
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        //перед тем как отобразит в foregrounde
//        print("WILL PRESENT")
//        let body = notification.request.content.body
//        let title = notification.request.content.title
//        CoreDataManager.shared.createNotification(of: AUNotification.self, body: body, title: title)
//        completionHandler([.alert, .sound])
//    }
//}
