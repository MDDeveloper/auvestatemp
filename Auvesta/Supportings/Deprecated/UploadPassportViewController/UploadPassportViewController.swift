import UIKit

protocol UploadPassportViewControllerDelegate: AnyObject {
    func didTapUploadPassportButton(vc: UploadPassportViewController)
    func didTapNextButton(vc: UploadPassportViewController)
    func didTapBackButton(vc: UploadPassportViewController)
}

class UploadPassportViewController: UIViewController {
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var uploadPassportImageView: UIImageView!
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var uploadPassportLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var controller: UploadPassportController?
    private var delegate: UploadPassportViewControllerDelegate?
    
    var passportImage: UIImage?
    var imageUrl: NSURL?
    var isUploadPassport: Bool = false
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setController(viewController: self)
        updateUI()
    }
    
    // MARK: - Methods
    
    private func setController(viewController: UploadPassportViewController) {
        controller = UploadPassportController(vc: viewController)
        viewController.delegate = controller
    }
    
    func checkCompleteStatus(isUploadPassport: Bool) {
        if isUploadPassport {
            nextButton.isEnabled = true
            nextButton.backgroundColor = #colorLiteral(red: 0.9001484513, green: 0.7037043571, blue: 0.1851784289, alpha: 1)
            nextButton.setTitleColor( #colorLiteral(red: 0.5299201012, green: 0.4084127843, blue: 0.1909401417, alpha: 1), for: .normal )
        } else {
            nextButton.isEnabled = false
            nextButton.backgroundColor = #colorLiteral(red: 0.6666092277, green: 0.6667047739, blue: 0.6665791273, alpha: 1)
            nextButton.setTitleColor( .white, for: .normal )
        }
    }
    
    func showImagePickerAlert() {
        let picker = UIImagePickerController()
        picker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        let controller = UIAlertController(title: "selectSource".localized(), message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "cancelTitle".localized(), style: .cancel, handler: nil)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let camera = UIAlertAction(title: "cameraSource".localized(), style: .default) { (action) in
                picker.sourceType = .camera
                self.present(picker, animated: true, completion: nil)
            }
            controller.addAction(camera)
        }
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let library = UIAlertAction(title: "photoLibrary".localized(), style: .default) { (action) in
                picker.sourceType = .photoLibrary
                self.present(picker, animated: true, completion: nil)
            }
            controller.addAction(library)
        }
        
        controller.addAction(cancel)
        controller.popoverPresentationController?.sourceView = self.view
        
        present(controller, animated: true, completion: nil)
    }
    
    private func updateUI() {
        modalTransitionStyle = .crossDissolve
        uploadPassportImageView.layer.cornerRadius = uploadPassportImageView.frame.size.height / 2
        nextButton.layer.cornerRadius = 5
        nextButton.isEnabled = false
        headerTitleLabel.text = "uploadPassportVcHeader".localized()
        uploadPassportLabel.text = "passportUpload".localized()
        nextButton.setTitle("customerRegistrationFinishButton".localized(), for: .normal)
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapUploadPassportButton(_ sender: UIButton) {
        delegate?.didTapUploadPassportButton(vc: self)
    }
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self)
    }
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        delegate?.didTapNextButton(vc: self)
    }
}

extension UploadPassportViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        
        passportImage = image
        nextButton.startActivityIndicator()
        
        if passportImage != nil {
            guard let imageData = image.jpegData(compressionQuality: 0) else { return }
            
            // last step
            RegistrationNetworkManager.uploadUserPassportImage(imageData: imageData) { (status, error) in
                self.nextButton.stopActivityIndicator()
                
                switch status {
                case 200:
                    self.uploadPassportImageView.image = UIImage(named: "passportUploadSuccess")
                    self.uploadPassportLabel.text = "passportUploadSuccess".localized()
                    self.isUploadPassport = true
                    self.checkCompleteStatus(isUploadPassport: self.isUploadPassport)
                default:
                    self.uploadPassportImageView.image = UIImage(named: "registration Passport")
                    self.uploadPassportLabel.text = "passportUploadError".localized()
                    self.isUploadPassport = false
                    self.checkCompleteStatus(isUploadPassport: self.isUploadPassport)
                    
                    let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                    self.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
}

extension UploadPassportViewController: UINavigationControllerDelegate {
    
}
