import UIKit

class UploadPassportController {
    private var viewController: UploadPassportViewController?
    
    init(vc: UploadPassportViewController?) {
        self.viewController = vc
    }
}

extension UploadPassportController: UploadPassportViewControllerDelegate {
    func didTapBackButton(vc: UploadPassportViewController) {
       let cancelAction = UIAlertAction(title: "noTitle".localized(), style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "yesTitle".localized(), style: .default) { (action) in
            vc.navigationController?.popToRootViewController(animated: true)
        }
        vc.addAlert(title: "exitTitle".localized(), message: "exitMessage".localized(), actions: [cancelAction, okAction])
    }
    
    func didTapNextButton(vc: UploadPassportViewController) {
        let loginVC = vc.navigationController?.viewControllers[0] as? LoginViewController
        loginVC?.performSegue(withIdentifier: "loginSegue", sender: nil)
//        let okAction = UIAlertAction(title: "OK".localized(), style: .default) { (action) in
//            let loginVC = vc.navigationController?.viewControllers[0] as? LoginViewController
//            loginVC?.performSegue(withIdentifier: "loginSegue", sender: nil)
//        }
//        vc.addAlert(title: "congratulationsTitle".localized(), message: "registrationSuccess".localized(), actions: [okAction])
    }
    
    func didTapUploadPassportButton(vc: UploadPassportViewController) {
        vc.showImagePickerAlert()
    }
}
