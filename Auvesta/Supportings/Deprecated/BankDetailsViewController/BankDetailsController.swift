import UIKit

class BankDetailsController: NSObject {
    fileprivate enum PaymentCycle: Int, CaseIterable {
        case monthly = 0
        case quarterly = 1
        case biannually = 2
        case yearly = 3
        
        var stringValue: String {
            switch self {
            case .monthly: return "monthly".localized()
            case .quarterly: return "quarterly".localized()
            case .biannually: return "biannually".localized()
            case .yearly: return "yearly".localized()
            }
        }
        
        var backendValue: String {
            switch self {
            case .monthly: return "M"
            case .quarterly: return "V"
            case .biannually: return "H"
            case .yearly: return "J"
            }
        }
    }
    
    private var viewController: BankDetailsViewController?
    private var countriesVC: SearchTableViewController?
    fileprivate var body: [String: Any] = [:]
    fileprivate var selectedPaymentCycle = PaymentCycle.monthly
    fileprivate var dateFormatter: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yyyy"
        return df
    }
    
    fileprivate var minBeginDate: Date {
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        components.day = 1
        return calendar.date(byAdding: components, to: currentDate)!
    }
    
    fileprivate func showDatePickerAlert(vc: BankDetailsViewController, completion: @escaping (Date)->()) {
        let alert = UIAlertController(title: "", message: "\n\n\n\n\n\n", preferredStyle: .actionSheet)
        alert.isModalInPopover = true
        
        let pickerFrame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width - 16, height: 140)
        let picker = UIDatePicker(frame: pickerFrame)
        picker.datePickerMode = .date
        
        picker.minimumDate = minBeginDate
        alert.view.addSubview(picker)
        
        let cancelAction = UIAlertAction(title: "cancelTitle".localized(), style: .destructive, handler: nil)
        let selectAction = UIAlertAction(title: "selectTitle".localized(), style: .default) { (action) in
            completion(picker.date)
        }
        
        alert.addAction(selectAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true)
    }
    
    fileprivate func showPickerAlert(vc: BankDetailsViewController) {
        let alert = UIAlertController(title: "", message: "\n\n\n\n\n\n", preferredStyle: .actionSheet)
        alert.isModalInPopover = true
        
        
        let pickerFrame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width - 16, height: 140)
        let picker = UIPickerView(frame: pickerFrame)
        picker.delegate = self
        picker.dataSource = self
        
        alert.view.addSubview(picker)
        
        let cancelAction = UIAlertAction(title: "cancelTitle".localized(), style: .destructive, handler: nil)
        let selectAction = UIAlertAction(title: "selectTitle".localized(), style: .default) { (void) in
            self.viewController?.paymentCycleLabel.text = self.selectedPaymentCycle.stringValue
        }
        
        alert.addAction(selectAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true)
    }
    
    init(viewController: BankDetailsViewController) {
        self.viewController = viewController
    }
}

extension BankDetailsController: BankDetailsViewControllerDelegate {
    func didTapChangeSubscriptionButton(vc: BankDetailsViewController) {
        // FIXME: - Probably changed ( storyboard reference )
        let viewController = vc.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.onViewDidLoad = {
            viewController.delegate = self
        }
        vc.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func didTapPaymentCycleButton(vc: BankDetailsViewController) {
        showPickerAlert(vc: vc)
    }
    
    func didTapBeginSubsButton(vc: BankDetailsViewController) {
        showDatePickerAlert(vc: vc) { (date) in
            DispatchQueue.main.async {
                vc.beginSubsLabel.text = self.dateFormatter.string(from: date)
            }
        }
    }
    
    func didTapSelectCountryButton(vc: BankDetailsViewController) {
        let countriesVc = vc.storyboard?.instantiateViewController(withIdentifier: String(describing: SearchTableViewController.self)) as! SearchTableViewController
        countriesVC = countriesVc
        countriesVc.onViewDidLoad = {
            countriesVc.titleLabel.text = "bankCountryHeaderTitle".localized()
            countriesVc.tableView.delegate = self
        }
        vc.present(countriesVc, animated: true)
    }
    
    func didTapBackButton(vc: BankDetailsViewController) {
        let cancelAction = UIAlertAction(title: "noTitle".localized(), style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "yesTitle".localized(), style: .default) { (action) in
            vc.navigationController?.popToRootViewController(animated: true)
        }
        vc.addAlert(title: "exitTitle".localized(), message: "exitMessage".localized(), actions: [cancelAction, okAction])
    }
    
    func didTapNextButton(vc: BankDetailsViewController) {
        guard
            let bic = vc.bankBicTextField.text,
            let bankName = vc.bankNameTextField.text,
            let bankCountry = vc.selectedCountry?.is3,
            let subscriptionRate = vc.monthlyRateTextField.text,
            //            let subscriptionPrepayment = vc.prepaymentTextField.text,
            let subscriptionBegin = vc.beginSubsLabel.text,
            let gold = vc.goldAllocationTextField.text,
            let silver = vc.silverAllocationTextField.text,
            let platinum = vc.platinumAllocationTextField.text,
            let palladium = vc.palladiumAllocationTextField.text else { return }
        
        //        AuvestaUser.shared.iban = IBAN
        //        AuvestaUser.shared.bankBic = vc.bankBicTextField.text ?? ""
        //        AuvestaUser.shared.bankName = vc.bankNameTextField.text ?? ""
        //        AuvestaUser.shared.bankCountry = vc.selectedCountry
        //        AuvestaUser.shared.subscriptionRate = Double(vc.monthlyRateTextField.text!) ?? 0
        //        AuvestaUser.shared.subscriptionPrepayment = Double(vc.prepaymentTextField.text!) ?? 0
        //        AuvestaUser.shared.subscriptionBegin = vc.beginSubsLabel.text ?? ""
        //        AuvestaUser.shared.subscriptionPaymentCycle = selectedPaymentCycle.backendValue
        //        AuvestaUser.shared.allocationGold = Int(vc.goldAllocationTextField.text!) ?? 0
        //        AuvestaUser.shared.allocationSilver = Int(vc.silverAllocationTextField.text!) ?? 0
        //        AuvestaUser.shared.allocationPlatinum = Int(vc.platinumAllocationTextField.text!) ?? 0
        //        AuvestaUser.shared.allocationPalladium = Int(vc.palladiumAllocationTextField.text!) ?? 0
        
        body = ["iban": vc.ibanTextFieldValueWithoutMask,
                "bankName": bankName,
                "bankCountry": bankCountry,
                "bankBic": bic,
                "rate": Int(subscriptionRate)!,
                "prepayment": 0,
                "begin": subscriptionBegin,
                "paymentCycle": selectedPaymentCycle.backendValue,
                "allocation_gold": Int(gold)!,
                "allocation_silver": Int(silver)!,
                "allocation_platinum": Int(platinum)!,
                "allocation_palladium": Int(palladium)!]
        
        vc.showTermsVC()
    }
}

extension BankDetailsController: BankDetailsViewControllerDataSource {
    func updatePrefilledData(vc: BankDetailsViewController) {
        let user = AuvestaUser.shared
        
        vc.agioLabel.text = user.subscriptionAgio
        vc.selectedBankCountryLabel.text = "selectTitle".localized()
        vc.subscriptionNameLabel.text = user.subscriptionType
        vc.monthlyRateTextField.text = String(Int(user.subscriptionRate))
        vc.silverAllocationTextField.text = "100"
        vc.beginSubsLabel.text = dateFormatter.string(from: minBeginDate)
        vc.paymentCycleLabel.text = selectedPaymentCycle.stringValue
        vc.changeAllocationStatus()
    }
}

extension BankDetailsController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country = countriesVC!.dataSource?.getCurrentCountry(vc: countriesVC!, indexPath: indexPath)
        viewController?.selectedCountry = country
        viewController?.selectedBankCountryLabel.text = country?.countryName
        countriesVC?.dismiss(animated: true, completion: nil)
    }
}

extension BankDetailsController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return PaymentCycle.init(rawValue: row)?.stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPaymentCycle = PaymentCycle(rawValue: row)!
    }
}

extension BankDetailsController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PaymentCycle.allCases.count
    }
}

extension BankDetailsController: CustomerTermsViewControllerDelegate {
    func didTapNewsletterButton(vc: CustomerTermsViewController, sender: UIButton) {
        let body: [String: Any] = ["news": sender.isSelected, "newsletterID": 0]
        
        AccountNetworkManager.changeNewsletter(body: body) { (status, error) in
            switch status {
            case 200:
                sender.isSelected.toggle()
            default:
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
    
    func didTapAgreeButton(vc: CustomerTermsViewController) {
        RegistrationNetworkManager.uploadBankDetails(body: body) { (status, error) in
            let bankVC = vc.viewController as! BankDetailsViewController
            bankVC.nextButton.stopActivityIndicator()
            
            switch status {
            case 200:
                RegistrationNetworkManager.acceptCustomerTerms { (status, error) in
                    switch status {
                    case 200:
                        vc.dismiss(animated: true) {
                            bankVC.performSegue(withIdentifier: "nextSegue", sender: nil)
                        }
                    default:
                        vc.dismiss(animated: true) {
                            bankVC.addAlert(title: "somethingWrongTitle".localized(), message: error)
                        }
                    }
                }
            default:
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error)
            }
        }
    }
    
    func didTapDisagreeButton(vc: CustomerTermsViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
}
//
//extension BankDetailsController: TermsViewControllerDelegate {
//    func didTapAgreeButton(vc: TermsViewController) {
//
//    }
//
//    func didTapDisagreeButton(vc: TermsViewController) {
//    }
//
//    func didTapNewsletterButton(vc: TermsViewController, sender: UIButton) {
//
//    }
//}

extension BankDetailsController: SubscriptionViewControllerDelegate {
    func didTapTermsButton(vc: SubscriptionViewController) {
        guard let id = vc.selectedSubscriptionID else { return }
        RegistrationNetworkManager.selectSubscription(id: "\(id)") { (status, error) in
            switch status {
            case 200:
                self.viewController?.agioLabel.text = AuvestaUser.shared.subscriptionAgio
                self.viewController?.subscriptionNameLabel.text = AuvestaUser.shared.subscriptionType
                self.viewController?.monthlyRateTextField.text = "\(AuvestaUser.shared.subscriptionRate)"
                vc.navigationController?.popViewController(animated: true)
            default:
                let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                vc.addAlert(title: "somethingWrongTitle".localized(), message: error, actions: [okAction])
            }
        }
    }
    
    func didTapBackButton(vc: SubscriptionViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
}
