import UIKit
import InputMask

protocol BankDetailsViewControllerDelegate: AnyObject {
    func didTapBackButton(vc: BankDetailsViewController)
    func didTapNextButton(vc: BankDetailsViewController)
    func didTapSelectCountryButton(vc: BankDetailsViewController)
    func didTapPaymentCycleButton(vc: BankDetailsViewController)
    func didTapBeginSubsButton(vc: BankDetailsViewController)
    func didTapChangeSubscriptionButton(vc: BankDetailsViewController)
}

protocol BankDetailsViewControllerDataSource: AnyObject {
    func updatePrefilledData(vc: BankDetailsViewController)
}

class BankDetailsViewController: UIViewController {
    enum ValidationResult {
        case emptyFields
        case incorrectPrepayment
        case incorrectMonthlyRate(_ minRate: Double)
        case incorrectAllocation
        case valid
        
        var errorMessage: String {
            switch self {
            case .emptyFields: return "emptyFieldsMessage".localized()
            case .incorrectPrepayment: return "incorrectPrepayment".localized()
            case .incorrectMonthlyRate(let minRate):
                return "incorrectMonthlyRatePartOne".localized() + "\(minRate)" + "incorrectMonthlyRatePartTwo".localized()
            case .incorrectAllocation: return "incorrectAllocation".localized()
            case .valid: return ""
            }
        }
    }
    
    // MARK: - @IBOutlet & Variables
    
    @IBOutlet private weak var changeSubscriptionButton: UIButton!
    @IBOutlet weak var subscriptionNameLabel: UILabel!
    @IBOutlet private weak var listener: MaskedTextFieldDelegate!
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    @IBOutlet private weak var bankDetailsScrollView: UIScrollView!
    
    @IBOutlet private weak var headerTitleLabel: UILabel!
    @IBOutlet private weak var allocationDescriptionLabel: UILabel!
    @IBOutlet private weak var ibanDescriptionLabel: UILabel!
    @IBOutlet private weak var bankCountryStringLabel: UILabel!
    @IBOutlet private weak var beginSubscriptionStringLabel: UILabel!
    @IBOutlet private weak var paymentCycleStringLabel: UILabel!
//    @IBOutlet private weak var prepaymentStringLabel: UILabel!
    @IBOutlet private weak var goldAllocationStringLabel: UILabel!
    @IBOutlet private weak var silverAllocationStringLabel: UILabel!
    @IBOutlet private weak var platinumAllocationStringLabel: UILabel!
    @IBOutlet private weak var palladiumAllocationStringLabel: UILabel!
    
    @IBOutlet private weak var ibanCustomView: UIView!
    @IBOutlet private weak var bankBicBackgroundView: UIView!
    @IBOutlet private weak var bankNameBackgroundView: UIView!
    @IBOutlet private weak var bankCountryCustomView: UIView!
    @IBOutlet private weak var monthlyRateBackgroundView: UIView!
    @IBOutlet private weak var beginSubsBackgroundView: UIView!
    @IBOutlet private weak var paymentCycleBackgroundView: UIView!
    @IBOutlet private weak var agioBackgroundView: UIView!
//    @IBOutlet private weak var prepaymentBackgroundView: UIView!
    @IBOutlet private weak var goldAllocationBackgroundView: UIView!
    @IBOutlet private weak var silverAllocationBackgroundView: UIView!
    @IBOutlet private weak var platinumAllocationBackgroundView: UIView!
    @IBOutlet private weak var palladiumAllocationBackgroundView: UIView!
    
//    @IBOutlet weak var prepaymentTextField: UITextField!
    @IBOutlet weak var monthlyRateTextField: UITextField!
    @IBOutlet weak var bankBicTextField: UITextField!
    @IBOutlet weak var bankNameTextField: UITextField!
    @IBOutlet private weak var ibanTextField: UITextField!
    @IBOutlet weak var goldAllocationTextField: UITextField!
    @IBOutlet weak var silverAllocationTextField: UITextField!
    @IBOutlet weak var platinumAllocationTextField: UITextField!
    @IBOutlet weak var palladiumAllocationTextField: UITextField!
    
//    @IBOutlet weak var prepaymentLabel: UILabel!
    @IBOutlet weak var agioLabel: UILabel!
    @IBOutlet weak var paymentCycleLabel: UILabel!
    @IBOutlet weak var beginSubsLabel: UILabel!
    @IBOutlet weak var monthlyRateLabel: UILabel!
    @IBOutlet weak var selectedBankCountryLabel: UILabel!
    @IBOutlet weak var allocationSumLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    let minIbanLength = 16
    let maxIbanLength = 34
    var agio = ""
    var monthlyRate = ""
    var minMonthlyRate = 0.0
    var ibanTextFieldValueWithoutMask: String = ""
    var selectedCountry: Country?
    
    private var delegate: BankDetailsViewControllerDelegate?
    private var dataSource: BankDetailsViewControllerDataSource?
    private var controller: BankDetailsController?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateListenerSettings(listener: listener)
        setController(viewController: self)
        dataSource?.updatePrefilledData(vc: self)
        allocationTextFieldDidChange()
        setupUI()
        localize()
        setupToolbars()
        
        #if DEBUG
        debug()
        #endif
    }
    
    // MARK: - Methods
    
    private func debug() {
        ibanTextFieldValueWithoutMask = "AL90208110080000001039531801"
        bankBicTextField.text = "GENODE61LAH"
        bankNameTextField.text = "Bank"
    }
    
    private func allocationTextFieldDidChange() {
        silverAllocationTextField.addTarget(self, action: #selector(changeAllocationStatus), for: UIControl.Event.editingChanged)
        goldAllocationTextField.addTarget(self, action: #selector(changeAllocationStatus), for: UIControl.Event.editingChanged)
        platinumAllocationTextField.addTarget(self, action: #selector(changeAllocationStatus), for: UIControl.Event.editingChanged)
        palladiumAllocationTextField.addTarget(self, action: #selector(changeAllocationStatus), for: UIControl.Event.editingChanged)
    }
    
    private func updateListenerSettings(listener: MaskedTextFieldDelegate) {
        listener.primaryMaskFormat = "[AA][00] [____] [0000] [0000] [0000] [0000] [0000] [0000] [00]"
        listener.autocomplete = true
        listener.onMaskedTextChangedCallback = { textField, value, complete in
            self.ibanTextFieldValueWithoutMask = value
        }
    }
    
    private func setController(viewController: BankDetailsViewController) {
        controller = BankDetailsController(viewController: viewController)
        viewController.delegate = controller
        viewController.dataSource = controller
    }
    
    @objc
    func changeAllocationStatus() {
        guard
            let goldAllocation = Int(goldAllocationTextField.text!),
            let silverAllocation = Int(silverAllocationTextField.text!),
            let platinumAllocation = Int(platinumAllocationTextField.text!),
            let palladiumAllocation = Int(palladiumAllocationTextField.text!) else { return }
        let allocationSum = goldAllocation + silverAllocation + platinumAllocation + palladiumAllocation
        allocationSumLabel.text = "Allocation: \(allocationSum)%"
        
        switch allocationSum {
        case 100:
            allocationSumLabel.textColor = .green
        default:
            allocationSumLabel.textColor = .red
        }
    }
    
    private func setupUI() {
        modalTransitionStyle = .crossDissolve
        nextButton.layer.cornerRadius = 5
        
        setupView(view: ibanCustomView)
        setupView(view: bankCountryCustomView)
        setupView(view: bankBicBackgroundView)
        setupView(view: bankNameBackgroundView)
        setupView(view: monthlyRateBackgroundView)
        setupView(view: beginSubsBackgroundView)
        setupView(view: paymentCycleBackgroundView)
        setupView(view: agioBackgroundView)
//        setupView(view: prepaymentBackgroundView)
        setupView(view: goldAllocationBackgroundView)
        setupView(view: silverAllocationBackgroundView)
        setupView(view: platinumAllocationBackgroundView)
        setupView(view: palladiumAllocationBackgroundView)
    }
    
    private func localize() {
        headerTitleLabel.text = "bankDetailsVcHeader".localized()
        nextButton.setTitle("nextButton".localized(), for: .normal)
        ibanDescriptionLabel.text = "ibanDescriptionTitle".localized()
        allocationDescriptionLabel.text = "allocationDescriptionTitle".localized()
        ibanTextField.placeholder = "ibanPlaceholder".localized()
        bankBicTextField.placeholder = "bankBicPlaceholder".localized()
        bankNameTextField.placeholder = "bankNamePlaceholder".localized()
        bankCountryStringLabel.text = "bankCountryPlaceholder".localized()
        monthlyRateLabel.text = "monthlyRatePlaceholder".localized()
        beginSubscriptionStringLabel.text = "beginSubscriptionPlaceholder".localized()
        paymentCycleStringLabel.text = "paymentCyclePlaceholder".localized()
        goldAllocationStringLabel.text = "goldPlaceholder".localized()
        silverAllocationStringLabel.text = "silverPlaceholder".localized()
        platinumAllocationStringLabel.text = "platinumPlaceholder".localized()
        palladiumAllocationStringLabel.text = "palladiumPlaceholder".localized()
//        prepaymentStringLabel.text = "prepaymentPlaceholder".localized()
    }
    
    private func validate() -> ValidationResult {
        guard
            !ibanTextField.text!.isEmpty,
            !bankBicTextField.text!.isEmpty,
            !bankNameTextField.text!.isEmpty,
            !selectedBankCountryLabel.text!.isEmpty,
            selectedBankCountryLabel.text! != "selectTitle".localized(),
            !monthlyRateTextField.text!.isEmpty,
            !beginSubsLabel.text!.isEmpty,
            !paymentCycleLabel.text!.isEmpty,
//            !prepaymentTextField.text!.isEmpty,
            !goldAllocationTextField.text!.isEmpty,
            !silverAllocationTextField.text!.isEmpty,
            !platinumAllocationTextField.text!.isEmpty,
            !palladiumAllocationTextField.text!.isEmpty else { return .emptyFields }
//        guard
//            let prepaymentValue = Double(prepaymentTextField.text!),
//            prepaymentValue >= 0 && prepaymentValue <= 100000 else { return .incorrectPrepayment }
        guard
            let monthlyRate = Double(monthlyRateTextField.text!),
            monthlyRate >= minMonthlyRate && monthlyRate <= 100000 else { return .incorrectMonthlyRate(minMonthlyRate) }
        guard
            let goldAllocation = Int(goldAllocationTextField.text!),
            let silverAllocation = Int(silverAllocationTextField.text!),
            let platinumAllocation = Int(platinumAllocationTextField.text!),
            let palladiumAllocation = Int(palladiumAllocationTextField.text!) else { return .incorrectAllocation }
        let allocationSum = goldAllocation + silverAllocation + platinumAllocation + palladiumAllocation
        guard allocationSum == 100 else { return .incorrectAllocation }
        
        return .valid
    }
    
    private func setupToolbars() {
        addInputAccessoryForTextFields(textFields: [ibanTextField,
                                                    bankBicTextField,
                                                    bankNameTextField,
                                                    monthlyRateTextField,
//                                                    prepaymentTextField,
                                                    goldAllocationTextField,
                                                    silverAllocationTextField,
                                                    platinumAllocationTextField,
                                                    palladiumAllocationTextField],
                                       previousNextable: true)
    }
    
    private func setupView(view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 0.2
        view.layer.borderColor = #colorLiteral(red: 0.7598875761, green: 0.7695845366, blue: 0.7692692876, alpha: 1)
    }
    
    func showTermsVC() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: CustomerTermsViewController.self)) as! CustomerTermsViewController
        viewController.viewController = self
        viewController.delegate = controller
        
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.view.endEditing(true)
    }
    
    // MARK: - @IBAction
    
    @IBAction func didTapChangeSubscriptionButton(_ sender: UIButton) {
        delegate?.didTapChangeSubscriptionButton(vc: self)
    }
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        delegate?.didTapBackButton(vc: self)
    }
    
    @IBAction func didTapSelectBankCountryButton(_ sender: UIButton) {
        delegate?.didTapSelectCountryButton(vc: self)
    }
    
    @IBAction func didTapPaymentCycleButton(_ sender: UIButton) {
        delegate?.didTapPaymentCycleButton(vc: self)
    }
    
    @IBAction func didTapBeginSubsButton(_ sender: UIButton) {
        delegate?.didTapBeginSubsButton(vc: self)
    }
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        let validationResult = validate()
        
        switch validationResult {
        case .valid:
            delegate?.didTapNextButton(vc: self)
        default:
            let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
            addAlert(title: "somethingWrongTitle".localized(), message: validationResult.errorMessage, actions: [okAction])
        }
    }
}

extension BankDetailsViewController: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        ibanTextFieldValueWithoutMask = value
    }
}

extension BankDetailsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let centerPoint = textField.center
        let convertedPoint = self.view.convert(centerPoint, from: textField.superview)
        let shouldChangeOffset = convertedPoint.y > self.view.frame.height / 2
        if shouldChangeOffset {
            bankDetailsScrollView.contentOffset.y += abs(convertedPoint.y - self.view.frame.height / 2)
        }
        return true
    }
}
