import Foundation

class ScanIdCardController {
    private var viewController: ScanIdCardViewController
    
    init(vc: ScanIdCardViewController) {
        self.viewController = vc
    }
}

extension ScanIdCardController: ScanIdCardViewControllerDelegate {
    func didTapCancelButton(vc: ScanIdCardViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
    func didTapNextButton(vc: ScanIdCardViewController) {
        
    }
    
    func didTapUploadButton(vc: ScanIdCardViewController) {
        
    }
    
    func didTapAddButton(vc: ScanIdCardViewController) {
        print("TAPPED")
    }
}
