import UIKit

protocol ScanIdCardViewControllerDelegate {
    func didTapCancelButton(vc: ScanIdCardViewController)
    func didTapNextButton(vc: ScanIdCardViewController)
    func didTapUploadButton(vc: ScanIdCardViewController)
    func didTapAddButton(vc: ScanIdCardViewController)
}

class ScanIdCardViewController: UIViewController {

    @IBOutlet weak var scanImageView: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var scanImageTitleLabel: UILabel!
    @IBOutlet weak var nextButton: ButtonWithActivityIndicator!
    @IBOutlet weak var cardIdTextField: UITextField!
    @IBOutlet weak var cardIdTitleLabel: UILabel!
    @IBOutlet weak var cardIdBackgroundView: UIView!
    
    private var delegate: ScanIdCardViewControllerDelegate?
    private var controller: ScanIdCardController?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setController()
        localize()
        setupUI()
    }
    
    private func setController() {
        let controller = ScanIdCardController(vc: self)
        delegate = controller
    }
    
    private func setupUI() {
        nextButton.layer.cornerRadius = 5
        scanImageView.layer.cornerRadius = scanImageView.layer.frame.size.height / 2
        
        cardIdBackgroundView.layer.cornerRadius = 5
        cardIdBackgroundView.layer.borderWidth = 0.2
        cardIdBackgroundView.layer.borderColor = #colorLiteral(red: 0.7598875761, green: 0.7695845366, blue: 0.7692692876, alpha: 1)
    }
    
    private func localize() {
        headerTitleLabel.text = "scanIdHeader".localized()
        scanImageTitleLabel.text = "scanImageTitle".localized()
        cardIdTitleLabel.text = "scanIdTitleLabel".localized()
        nextButton.setTitle("nextButton".localized(), for: .normal)
    }
    
    @IBAction func didTapUploadPhotoButton(_ sender: UIButton) {
        delegate?.didTapUploadButton(vc: self)
    }
    
    @IBAction func didTapAddButton(_ sender: UIButton) {
        delegate?.didTapAddButton(vc: self)
    }
    
    @IBAction func didTapNextButton(_ sender: ButtonWithActivityIndicator) {
        delegate?.didTapNextButton(vc: self)
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        delegate?.didTapCancelButton(vc: self)
    }
}
