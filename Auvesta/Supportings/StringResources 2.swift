//
//  StringResources.swift
//  Auvesta
//
//  Created by meowsawyer on 03/12/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation

class StringResources {
    static let shared = StringResources()
    
    var partnerNewsletterText = ""
    
    private init () {}
}
