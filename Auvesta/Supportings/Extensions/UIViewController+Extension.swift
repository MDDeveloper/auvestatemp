import UIKit

extension UIViewController {
    func addAlert(title: String? = nil, message: String? = nil, actions: [UIAlertAction]? = nil) {
        var actionsArray = [UIAlertAction]()
        
        if let newActions = actions {
            actionsArray = newActions
        } else {
            let okAction = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
            actionsArray.append(okAction)
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actionsArray {
            alert.addAction(action)
        }
        present(alert, animated: true)
    }
    
    func addInputAccessoryForTextFields(textFields: [UITextField], dismissable: Bool = true, previousNextable: Bool = false) {
        for (index, textField) in textFields.enumerated() {
            let toolbar: UIToolbar = UIToolbar()
            toolbar.sizeToFit()
            
            var items = [UIBarButtonItem]()
            
            if previousNextable {
                let previousButton = UIBarButtonItem(image: UIImage(named: "up_arrow"), style: .plain, target: nil, action: nil)
                previousButton.width = 30
                if textField == textFields.first {
                    previousButton.isEnabled = false
                } else {
                    previousButton.target = textFields[index - 1]
                    previousButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                let nextButton = UIBarButtonItem(image: UIImage(named: "down_arrow"), style: .plain, target: nil, action: nil)
                nextButton.width = 30
                if textField == textFields.last {
                    nextButton.isEnabled = false
                } else {
                    nextButton.target = textFields[index + 1]
                    nextButton.action = #selector(UITextField.becomeFirstResponder)
                }
                items.append(contentsOf: [previousButton, nextButton])
            }
            
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing))
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            items.append(contentsOf: [spacer, doneButton])
            
            
            toolbar.setItems(items, animated: false)
            textField.inputAccessoryView = toolbar
        }
    }
    
    
}
