import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: "Localizable", comment: "")
        
    }
    
    func isValidEmail() -> Bool {
        let mask = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let checkedString = NSPredicate(format: "SELF MATCHES %@", mask)
        
        return checkedString.evaluate(with: self) && self.count <= 100
    }
    
    func isValidPassword(name: String, surname: String, date: String) -> Bool {
        let mask = "^(?=.*[A-Za-z])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&])(?=.*\\d)[A-Za-z\\d!@#$%^&]{8,}$"
        let checkedString = NSPredicate(format: "SELF MATCHES %@", mask)
        guard checkedString.evaluate(with: self) else { return false }
        let lowercasedSelf = self.lowercased()
        guard !lowercasedSelf.contains(name.lowercased()) else { return false }
        guard !lowercasedSelf.contains(surname.lowercased()) else { return false }
        let dateWithoutDots = date.replacingOccurrences(of: ".", with: "")
        guard !self.contains(dateWithoutDots) else { return false }
        return true
    }
    
    func isValidPhoneNumber() -> Bool {
        let mask = "\\+?[1-9]\\d{1,14}"
        let checkedString = NSPredicate(format: "SELF MATCHES %@", mask)
        
        return checkedString.evaluate(with: self)
    }
    
    func isValidCompanyName() -> Bool {
        return self.count >= 2 && self.count <= 36
    }
    
    func isValidFirstName() -> Bool {
        return self.count >= 2 && self.count <= 100
    }
    
    func isValidLastName() -> Bool {
        return self.count >= 2 && self.count <= 100
    }
    
    func isValidStreet() -> Bool {
        return self.count >= 2 && self.count <= 60
    }
    
    func isValidZipCode() -> Bool {
        return self.count >= 2 && self.count <= 10
    }
    
    func isValidCity() -> Bool {
        return self.count >= 2 && self.count <= 50
    }
    
}
