import Foundation
import CoreData

final class CoreDataManager {
    
    static let shared = CoreDataManager()

//MARK: - Container

lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "CoreDataModel")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        print(storeDescription)
        if let error = error as NSError? {
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    })
    return container
}()

//MARK: - Context

lazy private var context = persistentContainer.viewContext

//MARK: - Saving and Deleting

func save () {
    if self.context.hasChanges {
        do {
            try self.context.save()
            print("SUCCESSFULL SAVING CORE DATA")
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
}

func delete(_ object: NSManagedObject) {
    DispatchQueue.global().async {
        self.context.delete(object)
        print("SUCCESSFULL DELETING CORE DATA: \(Date())")
    }
}

//MARK: - Fetching from CoreDataModel

func fetchNotifications() -> [AUNotification] {
    let request: NSFetchRequest<AUNotification> = AUNotification.fetchRequest()
    var results: [Any] = []
    do {
        results = try context.fetch(request)
        print("SUCCESS FETCHING CORE DATA")
    } catch {
        let nserror = error as NSError
        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
    }
    guard let teachers = results as? [AUNotification] else { return [] }
    return teachers
}

//MARK: - Creating an object of required type

    func createNotification(of type: NSManagedObject.Type, body: String, title: String) -> NSManagedObject? {
    guard let object = type.init(context: context) as? AUNotification else { print("DIDNT CREATE"); return nil }
    object.body = body
    object.title = title
    save()
    print("CORE DATA OBJECT CREATED")
    return object
 }
}
