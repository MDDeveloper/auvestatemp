import Foundation
import CoreData


extension AUNotification {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AUNotification> {
        return NSFetchRequest<AUNotification>(entityName: "AUNotification")
    }

    @NSManaged public var body: String?
    @NSManaged public var title: String?

}
