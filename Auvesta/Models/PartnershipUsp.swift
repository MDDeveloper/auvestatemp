import Foundation

class PartnershipUsp {
    enum UspType: String {
        case title = "TITLE"
        case header = "LISTHEADER"
        case item = "LISTITEM"
        
        var intValue: Int {
            switch self {
            case .title: return 0
            case .header: return 1
            case .item: return 2
            }
        }
    }
    
    var type: UspType = .title
    var text: String = ""
    var imageLink: String = ""
    
    static func getArray(from jsonArray: Any) -> [PartnershipUsp] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [PartnershipUsp]() }
        return jsonArray.compactMap{ PartnershipUsp(value: $0) }
    }
    
    init(value: [String: Any]) {
        guard let type = value["translationType"] as? String else { return }
        guard let text = value["translationText"] as? String else { return }
        let link = value["imageLink"] as? String
        
        self.type = UspType(rawValue: type)!
        self.text = text
        self.imageLink = link ?? ""
    }
}

extension PartnershipUsp: Comparable {
    static func < (lhs: PartnershipUsp, rhs: PartnershipUsp) -> Bool {
        return lhs.type.intValue < rhs.type.intValue
    }
    
    static func == (lhs: PartnershipUsp, rhs: PartnershipUsp) -> Bool {
        return lhs.type.intValue == rhs.type.intValue
    }
}
