import UIKit

class AuvestaUser {
    static let shared = AuvestaUser()
    private var privateProperties: PrivateProperties = PrivateProperties()
    
    // MARK: Authorization
    
    var token: String {
        get { return privateProperties.token }
        set { privateProperties.token = newValue }
    }
    
    var authorizationType: String {
        get { return privateProperties.authorizationType }
        set { privateProperties.authorizationType = newValue }
    }
    
    var stage: UserStatusStage {
        get { return UserStatusStage(rawValue: privateProperties.stage)! }
        set {  privateProperties.stage = newValue.rawValue }
    }
    
    // MARK: User Info
    
    var firstName: String {
        get { return privateProperties.firstName }
        set { privateProperties.firstName = newValue }
    }
    
    var lastName: String {
        get { return privateProperties.lastName }
        set { privateProperties.lastName = newValue }
    }
    
    var username: String {
        get { return privateProperties.username }
        set { privateProperties.username = newValue }
    }
    
    var psn: String {
        get { return privateProperties.psn }
        set { privateProperties.psn = newValue }
    }
    
    var telephone: String {
        get { return privateProperties.telephone }
        set { privateProperties.telephone = newValue }
    }
    
    var mobilePhone: String {
        get { return privateProperties.mobilePhone }
        set { privateProperties.mobilePhone = newValue }
    }
    
    var birthdayDate: String {
        get { return privateProperties.birthdayDate }
        set { privateProperties.birthdayDate = newValue }
    }
    
    var genderType: String {
        get { return privateProperties.genderType }
        set { privateProperties.genderType = newValue }
    }
    
    var nationality: String {
        get { return privateProperties.nationality }
        set { privateProperties.nationality = newValue }
    }
    
    var deviceCultureCode: String {
        get { return privateProperties.deviceCultureCode }
        set { privateProperties.deviceCultureCode = newValue }
    }
    
    // MARK: Company Info
    
    var companyArtID: Int? {
        get { return privateProperties.companyArtID }
        set { privateProperties.companyArtID = newValue }
    }
    
    var companyName: String {
        get { return privateProperties.companyName }
        set { privateProperties.companyName = newValue }
    }
    
    var city: String {
        get { return privateProperties.city }
        set { privateProperties.city = newValue }
    }
    
    var street: String {
        get { return privateProperties.street }
        set { privateProperties.street = newValue }
    }
    
    var zipCode: String {
        get { return privateProperties.zipCode }
        set { privateProperties.zipCode = newValue }
    }
    
    // MARK: Partner info
    
    var parentSponsorID: String {
        get { return privateProperties.parentSponsorID }
        set { privateProperties.parentSponsorID = newValue }
    }
    
    var sponsorID: String {
        get { return privateProperties.sponsorID }
        set { privateProperties.sponsorID = newValue }
    }
    
    var externalPartnerId: String {
        get { return privateProperties.externalPartnerId }
        set { privateProperties.externalPartnerId = newValue }
    }
    
    var externalDepotId: String {
        get { return privateProperties.externalDepotId }
        set { privateProperties.externalDepotId = newValue }
    }
    
    var isPartner: Bool {
        get { return privateProperties.isPartner }
        set { privateProperties.isPartner = newValue }
    }
    
    // MARK: Credentials
    
    var email: String {
        get { return privateProperties.email }
        set { privateProperties.email = newValue }
    }
    
    var password: String {
        get { return privateProperties.password }
        set { privateProperties.password = newValue }
    }
    
    // MARK: Bank Info
    
    var bankCountry: Country? {
        get { return privateProperties.bankCountry }
        set { privateProperties.bankCountry = newValue }
    }
    
    var bankBic: String {
        get { return privateProperties.bankBic }
        set { privateProperties.bankBic = newValue }
    }
    
    var bankName: String {
        get { return privateProperties.bankName }
        set { privateProperties.bankName = newValue }
    }
    
    var iban: String {
        get { return privateProperties.iban }
        set { privateProperties.iban = newValue }
    }
    
    // MARK: Subscription Info
    
    var subscriptionAgio: String {
        get { return privateProperties.subscriptionAgio }
        set { privateProperties.subscriptionAgio = newValue }
    }
    
    var subscriptionType: String {
        get { return privateProperties.subscriptionType }
        set { privateProperties.subscriptionType = newValue }
    }
    
    var subscriptionTypeId: Int {
        get { return privateProperties.subscriptionTypeId }
        set { privateProperties.subscriptionTypeId = newValue }
    }
    
    var subscriptionRate: Double {
        get { return privateProperties.subscriptionRate }
        set { privateProperties.subscriptionRate = newValue }
    }
    
    var subscriptionPrepayment: Double {
        get { return privateProperties.subscriptionPrepayment }
        set { privateProperties.subscriptionPrepayment = newValue }
    }
    
    var subscriptionBegin: String {
        get { return privateProperties.subscriptionBegin }
        set { privateProperties.subscriptionBegin = newValue }
    }
    
    var subscriptionPaymentCycle: String {
        get { return privateProperties.subscriptionPaymentCycle }
        set { privateProperties.subscriptionPaymentCycle = newValue }
    }
    
    // MARK: Allocation
    
    var allocationGold: Int {
        get { return privateProperties.allocationGold }
        set { privateProperties.allocationGold = newValue }
    }
    
    var allocationSilver: Int {
        get { return privateProperties.allocationSilver }
        set { privateProperties.allocationSilver = newValue }
    }
    
    var allocationPlatinum: Int {
        get { return privateProperties.allocationPlatinum }
        set { privateProperties.allocationPlatinum = newValue }
    }
    
    var allocationPalladium: Int {
        get { return privateProperties.allocationPalladium }
        set { privateProperties.allocationPalladium = newValue }
    }
    
    // MARK: Images
    
    var userPhoto: UIImage? {
        get { return privateProperties.userPhoto }
        set { privateProperties.userPhoto = newValue }
    }
    
    var userImageLink: String {
        get { return privateProperties.userImageLink }
        set { privateProperties.userImageLink = newValue }
    }
    
    var passportPhoto: UIImage? {
        get { return privateProperties.passportPhoto }
        set { privateProperties.passportPhoto = newValue }
    }
    
    var passportImageLink: String {
        get { return privateProperties.passportImageLink }
        set { privateProperties.passportImageLink = newValue }
    }
    
    // MARK: Mailing
    
    var partnerNewsletterIsHidden: Bool {
        get { return privateProperties.partnerNewsletterIsHidden }
        set { privateProperties.partnerNewsletterIsHidden = newValue }
    }
    
    var depotNewsletter: Bool {
        get { return privateProperties.depotNewsletter }
        set { privateProperties.depotNewsletter = newValue }
    }
    
    var partnerNewsletter: Bool {
        get { return privateProperties.partnerNewsletter }
        set { privateProperties.partnerNewsletter = newValue }
    }
    
    // MARK: Terms
    
    var customerTerms: Bool {
        get { return privateProperties.customerTerms }
        set { privateProperties.customerTerms = newValue }
    }
    
    var partnerTerms: Bool {
        get { return privateProperties.partnerTerms }
        set { privateProperties.partnerTerms = newValue }
    }
    
    // MARK: Network Info
    
    var contracts: [AuvestaContract] {
        get { return privateProperties.contracts }
        set { privateProperties.contracts = newValue }
    }

    var partners: [AuvestaPartner] {
        get { return privateProperties.partners }
        set { privateProperties.partners = newValue }
    }
    
    var commission: Double {
        get { return privateProperties.commission }
        set { privateProperties.commission = newValue }
    }
    
    var totalContractsCount: Int {
        get { return privateProperties.totalContractsCount }
        set { privateProperties.totalContractsCount = newValue }
    }
    
    var pendingContractsCount: Int {
        get { return privateProperties.pendingContractsCount }
        set { privateProperties.pendingContractsCount = newValue }
    }
    
    var partnersCount: Int {
        get { return privateProperties.partnersCount }
        set { privateProperties.partnersCount = newValue }
    }
    
    var totalSubPartnersCount: Int {
        get { return privateProperties.totalSubPartnersCount }
        set { privateProperties.totalSubPartnersCount = newValue }
    }
    
    // MARK: Methods
    
    func resetPrivateProperties() {
        privateProperties = PrivateProperties()
    }
    
    // MARK: Methods For Initialization
    
    func updateUserNetworkInfo(value: Any) {
        guard let jsonObject = value as? [String:Any] else { return }
            let totalSubPartnersCount = jsonObject["count"] as? Int
            let partnersCount = jsonObject["directCount"] as? Int
            let pendingContractsCount = jsonObject["pendingContractsCount"] as? Int
            let totalContractsCount = jsonObject["totalContractsCount"] as? Int
            let submission = jsonObject["submission"] as? Double
        
        self.totalSubPartnersCount = totalSubPartnersCount ?? 0
        self.partnersCount = partnersCount ?? 0
        self.pendingContractsCount = pendingContractsCount ?? 0
        self.totalContractsCount = totalContractsCount ?? 0
        self.commission = submission ?? 0
    }
    
    func updateUserProfile(value: Any) {
        guard let jsonObject = value as? [String:Any] else { return }
        //            let parentSponsorID = jsonObject["parentSponsorID"] as? String,
        //            let passportImageLink = jsonObject["passportPhoto"] as? String,
        let isPartner = jsonObject["isPartner"] as? Bool
        let userImageLink = jsonObject["profilePhoto"] as? String
        let externalPartnerId = jsonObject["externalPartnerId"] as? Int
        let externalDepotId = jsonObject["externalDepotId"] as? String
        let psn = jsonObject["psn"] as? String
        let gender = jsonObject["gender"] as? String
        let companyName = jsonObject["companyName"] as? String
        
        //        self.parentSponsorID = parentSponsorID
        //        self.passportImageLink = passportImageLink
        self.companyName = companyName ?? ""
        self.genderType = gender ?? ""
        self.psn = psn ?? ""
//        self.isPartner = false
        self.isPartner = isPartner ?? false
        self.userImageLink = userImageLink ?? ""
        self.externalDepotId = externalDepotId ?? ""
        self.externalPartnerId = String(externalPartnerId ?? 0)
        
        guard
            let userSubscription = jsonObject["userSubscription"] as? [String:Any],
            let subscription = userSubscription["subscription"] as? [String:Any] else { return }
        let subscriptionName = subscription["name"] as? String
        let subscriptionAgio = subscription["agio"] as? Int
        let subscriptionRate = subscription["invest"] as? Int
        
        self.subscriptionAgio = String(subscriptionAgio ?? 0)
        self.subscriptionType = subscriptionName ?? ""
        self.subscriptionRate = Double(subscriptionRate ?? 0)
    }
    
    func updateUserProfileDetails(value: Any) {
        guard let jsonObject = value as? [String:Any] else { return }
        let username = jsonObject["fullName"] as? String
        let email = jsonObject["email"] as? String
        let firstName = jsonObject["firstName"] as? String
        let lastName = jsonObject["lastName"] as? String
        let userSponsorID = jsonObject["referrerUid"] as? String
        
        
        self.username = username ?? ""
        self.email = email ?? ""
        self.firstName = firstName ?? ""
        self.lastName = lastName ?? ""
        self.sponsorID = userSponsorID ?? ""
        
        guard let mailing = jsonObject["mailing"] as? [String:Any] else { return }
        let customer = mailing["customer"] as? Bool
        
        self.depotNewsletter = customer ?? false
        
        guard let partner = mailing["partner"] as? Bool else {
            self.partnerNewsletterIsHidden = true
            return
        }
        self.partnerNewsletter = partner
        self.partnerNewsletterIsHidden = false
    }
    
    func updatePartnerRegistrationInfo(value: Any) {
        guard let jsonObject = value as? [String:Any] else { return }
        let firstName = jsonObject["firstName"] as? String
        let lastName = jsonObject["lastName"] as? String
        let email = jsonObject["email"] as? String
        let passportLink = jsonObject["passportLink"] as? String
        let telephone = jsonObject["telephone"] as? String
        let mobile = jsonObject["mobile"] as? String
        let birthday = jsonObject["birthday"] as? String
        let city = jsonObject["city"] as? String
        let street = jsonObject["street"] as? String
        let zip = jsonObject["zip"] as? String
        let nationality = jsonObject["nationality"] as? String
        let gender = jsonObject["gender"] as? String
        let companyName = jsonObject["companyName"] as? String
        let partnerNewsletterText = jsonObject["newsletterText"] as? String
        
        self.firstName = firstName ?? ""
        self.lastName = lastName ?? ""
        self.email = email ?? ""
        self.passportImageLink = passportLink ?? ""
        self.telephone = telephone ?? ""
        self.mobilePhone = mobile ?? ""
        self.birthdayDate = birthday ?? ""
        self.city = city ?? ""
        self.street = street ?? ""
        self.zipCode = zip ?? ""
        self.nationality = nationality ?? ""
        self.genderType = gender ?? ""
        self.companyName = companyName ?? ""
        StringResources.shared.partnerNewsletterText = partnerNewsletterText ?? "newsletter".localized()
    }
    
    func updateToken(value: Any) {
        guard
            let jsonObject = value as? [String:Any],
            let token = jsonObject["token"] as? String,
            let authorizationType = jsonObject["authorizationType"] as? String,
            let userProfile = jsonObject["userProfile"] as? [String:Any],
            let user = userProfile["user"] as? [String:Any],
            let onboarding = user["onboarding"] as? [String:Any],
            let stage = onboarding["stage"] as? Int else { return }
        
        self.authorizationType = authorizationType
        self.token = token
        AuthManager.shared.updateAccountToken(token)
        self.stage = UserStatusStage(rawValue: stage)!
    }
    
    func updateOneTimeToken(value: Any) {
        guard
        let jsonObject = value as? [String:Any],
        let token = jsonObject["token"] as? String,
            let authorizationType = jsonObject["authorizationType"] as? String else { return }
        
        self.authorizationType = authorizationType
        self.token = token
    }
}
