import Foundation

struct AuvestaSubscription {
    var id: Int?
    var name: String?
    var agio: String?
    var spread: String?
    var description: String?
    var investAmountDouble: Double?
    var investAmount: String?
    var backgroundImageName: String?

    static func getArray(from jsonArray: Any) -> [AuvestaSubscription] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [AuvestaSubscription]() }
        return jsonArray.compactMap{ AuvestaSubscription(jsonArray: $0) }
    }
    
    init?(jsonArray: [String:Any]) {
        let name = jsonArray["name"] as? String
        let agioAmount = jsonArray["agio"] as? Double
        let investAmount = jsonArray["invest"] as? Double
        let backgroundColor = jsonArray["color"] as? String
        let description = jsonArray["description"] as? String
        let spreadLevel = jsonArray["spreadLevel"] as? String
        let spreadPercent = jsonArray["spreadPercent"] as? Double
        let subscriptionId = jsonArray["subscriptionId"] as? Int
        let currency = jsonArray["currency"] as? Int
        
        self.id = subscriptionId
        self.name = name
        self.description = description ?? ""
        self.agio = "\(String(format: "%.2f", agioAmount ?? 0.00))\(AuvestaCurrency(rawValue: currency ?? -1)!.stringIcon)"
        self.spread = "\(spreadLevel ?? "") Level Spreads (+\(spreadPercent ?? 0)%)"
        self.investAmountDouble = investAmount ?? 0
        self.investAmount = "\(String(format: "%.2f", investAmount ?? 0.00))\(AuvestaCurrency(rawValue: currency ?? -1)!.stringIcon)"
        self.backgroundImageName = "\(backgroundColor ?? "yellow") subscription background"
    }
}
