import Foundation

class StripeCheck {
    var showStripe: String = ""
    
    init(value: Any) {
        guard let json = value as? [String: Any] else { return }
        guard let showStripe = json["showStripe"] as? String else { return }
        self.showStripe = showStripe
    }
}
