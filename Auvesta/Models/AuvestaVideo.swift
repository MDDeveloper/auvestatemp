import Foundation

class AuvestaVideo {
    var id: String = ""
    var url: String = ""
    var title: String = ""
    
    init(value: Any) {
        guard let json = value as? [String: Any] else { return }
        guard let id = json["id"] as? String else { return }
        self.id = id
        guard let url = json["url"] as? String else { return }
        self.url = url
        guard let title = json["title"] as? String else { return }
        self.title = title
    }
}
