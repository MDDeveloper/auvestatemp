import Foundation

enum AuvestaCurrency: Int, CaseIterable {
    case notDetected = -1
    case euro = 0
    case btc = 1
    
    case gram = 3
    case pieces = 4
    
    var stringIcon: String {
        switch self {
        case .notDetected: return ""
        case .euro: return "€"
        case .btc: return "BTC"
        case .gram: return "g"
        case .pieces: return "pieces"
        }
    }
    
    static func getPriceValue(_ value: inout String) {
        for currency in AuvestaCurrency.allCases {
            guard value.hasSuffix(currency.stringIcon) else { continue }
            value.removeLast(currency.stringIcon.count)
        }
    }
}
