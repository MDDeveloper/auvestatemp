//
//  AuvestaTerm.swift
//  Auvesta
//
//  Created by meowsawyer on 19/09/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation

class CustomerTerms {
    var tacLink: String = ""
    var distanceSellingLink: String = ""
    var revocationLink: String = ""
    var privacyLink: String = ""
    var buttonText: String = ""
    
    var checkboxOneText: String = ""
    var tacText: String = ""
    var distanceSellingText: String = ""
    var revocationText: String = ""
    var checkboxTwoText: String = ""
    var privacyText: String = ""
    var checkboxThreeText: String = ""
    var checkboxFourText: String = ""
    var checkboxFourDescriptionText: String = ""
    
    init?(json: Any) {
        guard let jsonArray = json as? [String:Any] else { return }
        guard let texts = jsonArray["texts"] as? [String:Any] else { return }
        guard let pdfs = jsonArray["pdfs"] as? [String:Any] else { return }
        let text1 = texts["TEXT_1"] as? String
        let text2 = texts["TEXT_2"] as? String
        let text3 = texts["TEXT_3"] as? String
        let text4 = texts["TEXT_4"] as? String
        let text5 = texts["TEXT_5"] as? String
        let text6 = texts["TEXT_6"] as? String
        let text7 = texts["TEXT_7"] as? String
        let text8 = texts["TEXT_8"] as? String
        let text9 = texts["TEXT_9"] as? String
        let tac = pdfs["TAC"] as? String
        let distanceSelling = pdfs["DISTANCE_SELLING"] as? String
        let revocation = pdfs["REVOCATION"] as? String
        let privacy = pdfs["PRIVACY"] as? String
        let buttonText = texts["BUTTON"] as? String
        
        self.checkboxOneText = text1 ?? ""
        self.tacText = text2 ?? ""
        self.distanceSellingText = text3 ?? ""
        self.revocationText = text4 ?? ""
        self.checkboxTwoText = text5 ?? ""
        self.privacyText = text6 ?? ""
        self.checkboxThreeText = text7 ?? ""
        self.checkboxFourText = text8 ?? ""
        self.checkboxFourDescriptionText = text9 ?? ""
        self.tacLink = tac ?? ""
        self.distanceSellingLink = distanceSelling ?? ""
        self.revocationLink = revocation ?? ""
        self.privacyLink = privacy ?? ""
        self.buttonText = buttonText ?? "agreeButton".localized()
    }
}
