//
//  IpDetails.swift
//  Auvesta
//
//  Created by meowsawyer on 21/11/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation

class IpDetails {
    var countryCode: String = ""
    
    init(value: Any) {
        guard let json = value as? [String: Any] else { return }
        let countryCode = json["countryCode"] as? String
        self.countryCode = countryCode ?? ""
    }
}
