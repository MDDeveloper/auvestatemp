//
//  UserStatusStage.swift
//  Auvesta
//
//  Created by meowsawyer on 26/11/2019.
//  Copyright © 2019 Auvesta. All rights reserved.
//

import Foundation

enum UserStatusStage: Int {
    case unknownState = -1
    case notConfirmedEmail = 0
    case emailConfirmed = 1
    case subscriptionSelected = 2
    case bankDetailsUploaded = 3
    case customerTermsAccepted = 4
    case customer = 5
}
