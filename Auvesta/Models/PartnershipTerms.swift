import Foundation

class PartnershipTerms {
    enum TermsType: String {
        case link = "link"
        case line = "line"
        case header = "header"
        
        var intValue: Int {
            switch self {
            case .header: return 0
            case .line: return 1
            case .link: return 2
            }
        }
    }
    
    var type: TermsType = .line
    var text: String = ""
    var link: String = ""
    
    static func getArray(from jsonArray: Any) -> [PartnershipTerms] {
        guard let jsonArray = jsonArray as? Array<[String:Any]> else { return [PartnershipTerms]() }
        return jsonArray.compactMap{ PartnershipTerms(value: $0) }
    }
    
    init(value: [String: Any]) {
        guard let type = value["type"] as? String else { return }
        guard let text = value["text"] as? String else { return }
        let link = value["url"] as? String
        
        self.type = TermsType(rawValue: type)!
        self.text = text
        self.link = link ?? ""
    }
}

extension PartnershipTerms: Comparable {
    static func < (lhs: PartnershipTerms, rhs: PartnershipTerms) -> Bool {
        return lhs.type.intValue < rhs.type.intValue
    }
    
    static func == (lhs: PartnershipTerms, rhs: PartnershipTerms) -> Bool {
        return lhs.type.intValue == rhs.type.intValue
    }
}
