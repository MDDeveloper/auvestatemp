import Foundation

class IpDetails {
    var countryCode: String = ""
    
    init(value: Any) {
        guard let json = value as? [String: Any] else { return }
        let countryCode = json["countryCode"] as? String
        self.countryCode = countryCode ?? ""
    }
}
