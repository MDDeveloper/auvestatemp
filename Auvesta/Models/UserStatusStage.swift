import Foundation

enum UserStatusStage: Int {
    case unknownState = -1  // default
    case notConfirmedEmail = 0
    case emailConfirmed = 1 // step missed with current logics ( we use emailVerify api request instead of emailConfirmCheck )
    case subscriptionSelected = 2 // latest step
    case bankDetailsUploaded = 3 // should never execute with new logics
    case customerTermsAccepted = 4
    case customer = 5
}
