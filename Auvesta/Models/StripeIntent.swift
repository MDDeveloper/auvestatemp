import Foundation

@objc class StripeIntent: NSObject {
    var paymentIntentID: String = " "
    var clientSecret: String = " "
    var stripeCustomerID: String = " "
    var amount: Int = 0
    var amountInCurrency: Int = 0
    var currency: String = " "
    var amount_agio: Int = 0
    var amount_metal: Int = 0
    
    
    init(value: Any) {
        guard let json = value as? [String: Any] else { return }
        guard let paymentIntentID = json["paymentIntentID"] as? String else { return }
        self.paymentIntentID = paymentIntentID
        guard let clientSecret = json["clientSecret"] as? String else { return }
        self.clientSecret = clientSecret
        guard let stripeCustomerID = json["stripeCustomerID"] as? String else { return }
        self.stripeCustomerID = stripeCustomerID
        guard let amount = json["amount"] as? Int else { return }
        self.amount = amount
        guard let amountInCurrency = json["amountInCurrency"] as? Int else { return }
        self.amountInCurrency = amountInCurrency
        guard let currency = json["currency"] as? String else { return }
        self.currency = currency
        guard let amount_agio = json["amount_agio"] as? Int else { return }
        self.amount_agio = amount_agio
        guard let amount_metal = json["amount_metal"] as? Int else { return }
        self.amount_metal = amount_metal
    }
}
