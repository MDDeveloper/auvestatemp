import Foundation

class StripeKey {
    var clientPublic: String = " "
    
    init(value: Any) {
        guard let json = value as? [String: Any] else { return }
        guard let clientPublic = json["clientPublic"] as? String else { return }
        self.clientPublic = clientPublic
    }
}
