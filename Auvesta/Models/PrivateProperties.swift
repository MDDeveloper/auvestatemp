import UIKit

class PrivateProperties {
    
    // MARK: Authorization
    
    var token: String = ""
    var authorizationType: String = "Bearer"
    var stage = -1
    
    // MARK: User Info
    
    var firstName: String = ""
    var lastName: String = ""
    var username: String = ""
    var psn: String = ""
    var telephone: String = ""
    var mobilePhone: String = ""
    var birthdayDate: String = ""
    var genderType: String = ""
    var nationality: String = ""
    var deviceCultureCode: String = ""
   
    // MARK: Company Info

    var companyArtID: Int? = nil
    var companyName: String = ""
    var city: String = ""
    var street: String = ""
    var zipCode: String = ""
    
    // MARK: Partner info

    var parentSponsorID: String = ""
    var sponsorID: String = ""
    var externalPartnerId: String = ""
    var externalDepotId: String = ""
    var isPartner: Bool = false
    
    // MARK: Credentials

    var email: String = ""
    var password: String = ""
    
    // MARK: Bank Info

    var bankCountry: Country?
    var bankBic: String = ""
    var bankName: String = ""
    var iban: String = ""
    
    // MARK: Subscription Info

    var subscriptionAgio: String = ""
    var subscriptionType: String = "Not selected"
    var subscriptionTypeId: Int = 0
    var subscriptionRate: Double = 0
    var subscriptionPrepayment: Double = 0
    var subscriptionBegin: String = ""
    var subscriptionPaymentCycle: String = ""
    
    // MARK: Allocation

    var allocationGold: Int = 0
    var allocationSilver: Int = 0
    var allocationPlatinum: Int = 0
    var allocationPalladium: Int = 0
    
    // MARK: Images

    var userPhoto = UIImage(named: "new user icon")
    var userImageLink: String = ""
    var passportPhoto: UIImage?
    var passportImageLink: String = ""
    
    // MARK: Mailing

    var partnerNewsletterIsHidden: Bool = true
    var depotNewsletter: Bool = false
    var partnerNewsletter: Bool = false
    
    // MARK: Terms

    var customerTerms: Bool = false
    var partnerTerms: Bool = false
    
    // MARK: Network Info

    var contracts = [AuvestaContract]()
    var partners = [AuvestaPartner]()
    var commission: Double = 0
    var totalContractsCount: Int = 0
    var pendingContractsCount: Int = 0
    var partnersCount: Int = 0
    var totalSubPartnersCount: Int = 0
}
