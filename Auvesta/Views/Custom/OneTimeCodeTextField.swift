import UIKit

class OneTimeCodeTextField: UITextField {
    
    var didEnterLastDigit: ((String) -> Void)?
    
    private var isConfigured: Bool = false
    
    var codeLabels = [UILabel]()
    
    var codeLabelsCount: Int {
        return codeLabels.count
    }
    
    private lazy var tapRecognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer()
        recognizer.addTarget(self, action: #selector(becomeFirstResponder))
        return recognizer
    }()
    
    func configure(with slotCount: Int = 6) {
        guard isConfigured == false else { return }
        isConfigured.toggle()
        
        configureTextField()
        
        let labelsStackView = createLabelsStackView(with: slotCount)
        addSubview(labelsStackView)
        
        addGestureRecognizer(tapRecognizer)
        
        NSLayoutConstraint.activate([
            labelsStackView.topAnchor.constraint(equalTo: topAnchor),
            labelsStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            labelsStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            labelsStackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        for label in codeLabels {
            NSLayoutConstraint.activate([
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor),
                label.leadingAnchor.constraint(equalTo: label.superview!.leadingAnchor),
                label.trailingAnchor.constraint(equalTo: label.superview!.trailingAnchor),
                label.bottomAnchor.constraint(equalTo: label.superview!.bottomAnchor)
            ])
        }
    }
    
    private func configureTextField() {
        tintColor = .clear
        textColor = .clear
        keyboardType = .numberPad
        textContentType = .oneTimeCode
        borderStyle = .none
        addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    }
    
    private func createLabelsStackView(with count: Int) -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        
        for _ in 1 ... count {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.cornerRadius = 5
            view.layer.borderWidth = 0.2
            view.layer.borderColor = #colorLiteral(red: 0.439996779, green: 0.4511739612, blue: 0.4894440174, alpha: 1)
            view.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.9843137255, blue: 0.9843137255, alpha: 1)
            
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textAlignment = .center
            label.font = .systemFont(ofSize: 40)
            label.textColor = #colorLiteral(red: 0.440081656, green: 0.4471049905, blue: 0.4896175265, alpha: 1)
            label.isUserInteractionEnabled = true
            label.text = "_"

            view.addSubview(label)
            stackView.addArrangedSubview(view)
            
            codeLabels.append(label)
        }
        return stackView
    }
    
    @objc
    func textDidChange() {
        guard let text = self.text, text.count <= codeLabels.count else { return }
        
        for i in 0 ..< codeLabels.count {
            let currentLabel = codeLabels[i]
            
            if i < text.count {
                let index = text.index(text.startIndex, offsetBy: i)
                currentLabel.text = String(text[index])
            } else {
                currentLabel.text = "_"
            }
        }
        
        if text.count == codeLabels.count {
            didEnterLastDigit?(text)
        }
    }
    // Some delegate functionality configured in parent ViewController class!
}
