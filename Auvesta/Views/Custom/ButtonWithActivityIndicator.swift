import UIKit

class ButtonWithActivityIndicator: UIButton {
    private let activityIndicator = UIActivityIndicatorView()
    private var buttonTitleLabelText = ""
    private var buttonImage = UIImage()
//    private let customLabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        updateActivityIndicator(indicator: activityIndicator, view: self)
    }
    
//    func setCustomLabel(text: String, fontColor: UIColor, fontSize: CGFloat) {
//        self.addSubview(customLabel)
//        [
//            customLabel.topAnchor.constraint(equalTo: self.topAnchor),
//            customLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor),
//            customLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
//            customLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor)
//            ].forEach({ $0.isActive = true })
//
//        self.setTitle("", for: .normal)
//        customLabel.textColor = fontColor
//        customLabel.font = .systemFont(ofSize: fontSize)
//        customLabel.text = text
//    }
    
    func startActivityIndicator() {
        buttonImage = imageView?.image ?? UIImage()
        setImage(nil, for: .normal)
        buttonTitleLabelText = titleLabel!.text ?? ""
        setTitle("", for: .normal)
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    func stopActivityIndicator() {
        activityIndicator.stopAnimating()
        setTitle(buttonTitleLabelText, for: .normal)
        setImage(buttonImage, for: .normal)
    }
    
    private func updateActivityIndicator(indicator: UIActivityIndicatorView, view: UIView) {
        indicator.hidesWhenStopped = true
        indicator.isHidden = true
        indicator.color = #colorLiteral(red: 0.5529384017, green: 0.4286764264, blue: 0.1908931732, alpha: 1)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
    }
}
