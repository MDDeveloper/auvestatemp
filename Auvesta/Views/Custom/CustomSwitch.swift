import UIKit
@IBDesignable
final class CustomSwitch: UIControl {
    @IBInspectable public var onTintColor = #colorLiteral(red: 0.989192903, green: 0.8983053565, blue: 0.6874808669, alpha: 1) {
        didSet {
            self.setupUI()
        }
    }
    
    @IBInspectable public var offTintColor = #colorLiteral(red: 0.8065007807, green: 0.8037052799, blue: 0.7953187773, alpha: 1) {
        didSet {
            self.setupUI()
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat = 0.5 {
        didSet {
            self.layoutSubviews()
        }
    }

    @IBInspectable public var thumbTintColor = UIColor.white {
        didSet {
            self.thumbView.backgroundColor = self.thumbTintColor
        }
    }
    
    @IBInspectable public var thumbBorderColor = #colorLiteral(red: 0.8164352775, green: 0.6262487769, blue: 0.1418944895, alpha: 1) {
        didSet {
            self.thumbView.backgroundColor = self.thumbTintColor
        }
    }
    
    @IBInspectable public var thumbCornerRadius: CGFloat = 0.5 {
        didSet {
            self.layoutSubviews()
        }
    }
    
   @IBInspectable public var thumbSize = CGSize.zero {
        didSet {
            self.layoutSubviews()
        }
    }
    
    @IBInspectable public var padding: CGFloat = 0 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    @IBInspectable public var isOn = false {
        didSet {
            self.setupUI()
        }
    }
    
    public var animationDuration: Double = 0.5
    
    fileprivate var thumbView = UIView(frame: CGRect.zero)
    fileprivate var onPoint = CGPoint.zero
    fileprivate var offPoint = CGPoint.zero
    fileprivate var isAnimating = false
    
    private func clear() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    func setupUI() {
        self.clear()
        self.clipsToBounds = false
        self.thumbView.backgroundColor = self.thumbTintColor
        self.thumbView.isUserInteractionEnabled = false
        self.addSubview(self.thumbView)
        
        self.thumbView.layer.shadowColor = UIColor.black.cgColor
        self.thumbView.layer.shadowRadius = 1.5
        self.thumbView.layer.shadowOpacity = 0.4
        self.thumbView.layer.shadowOffset = CGSize(width: 0.75, height: 2)
        
        self.thumbView.layer.borderWidth = 3
        self.thumbView.layer.borderColor = self.thumbBorderColor.cgColor
    }
    
    private func animate() {
        self.isOn = !self.isOn
        self.isAnimating = true
        
        UIView.animate(withDuration: self.animationDuration, delay: 0, usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0.5, options: [.curveEaseOut, .beginFromCurrentState],
                       animations: {
                        self.thumbView.frame.origin.x = self.isOn ? self.onPoint.x : self.offPoint.x
                        self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
        }, completion: { _ in
            self.isAnimating = false
            self.sendActions(for: UIControl.Event.valueChanged)
        })
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        setupUI()
        if !self.isAnimating {
            self.layer.cornerRadius = self.bounds.size.height * self.cornerRadius
            self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
    
            // thumb management
            let thumbSize = self.thumbSize != CGSize.zero ? self.thumbSize : CGSize(width: self.bounds.size.height * 1.3 , height: self.bounds.height * 1.3 )
            let yPosition = (self.bounds.size.height - thumbSize.height) / 2
            
            self.onPoint = CGPoint(x: self.bounds.size.width - thumbSize.width - self.padding, y: yPosition)
            self.offPoint = CGPoint(x: self.padding, y: yPosition)
            
            self.thumbView.frame = CGRect(origin: self.isOn ? self.onPoint : self.offPoint, size: thumbSize)
            self.thumbView.layer.cornerRadius = thumbSize.height * self.thumbCornerRadius
        }
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        
        self.animate()
        return true
    }
}
