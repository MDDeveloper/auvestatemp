import UIKit

class SubscriptionTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var viewForMoneyLabel: UIView!
    @IBOutlet weak var selectionView: UIView!
    
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet private weak var agioLabel: UILabel!
    @IBOutlet private weak var spreadsLabel: UILabel!
    @IBOutlet private weak var moneyAmountLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var paymentPeriodLabel: UILabel!
    
    var subscription: AuvestaSubscription? {
        didSet {
            self.updateData()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupImageView(image: backgroundImageView)
        setupViewForMoneyLabel(view: viewForMoneyLabel)
    }
    
    private func updateData() {
        paymentPeriodLabel.text = "paymentPeriodTitle".localized()
        if let subscription = subscription {
            levelLabel.text = subscription.name
            agioLabel.text = "Setup fee \(subscription.agio!)"
            descriptionLabel.text = "\(subscription.description ?? "")"
            moneyAmountLabel.text = "\(subscription.investAmount!)"
            backgroundImageView.image = UIImage(named: subscription.backgroundImageName!)
        } else {
            levelLabel.text = nil
            agioLabel.text = nil
            spreadsLabel.text = nil
            moneyAmountLabel.text = nil
            backgroundImageView.image = nil
        }
    }
    
    private func setupViewForMoneyLabel(view: UIView) {
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    private func setupImageView(image: UIImageView) {
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
    }
}
